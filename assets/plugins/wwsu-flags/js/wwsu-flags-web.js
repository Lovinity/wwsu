"use strict";
// This class manages reporting / flagging songs and broadcasts.

// REQUIRES these WWSUmodules: WWSUMeta, WWSUevents, WWSUsongs, WWSUutil, WWSUanimations, noReq
// REQUIRES: Alpaca
class WWSUflagsweb extends WWSUevents {
	/**
	 * Construct the class
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 */
	constructor(manager, options) {
		super();

		this.manager = manager;

		this.endpoints = {
			add: "/flags/add",
		};

		this.flagModal = new WWSUmodal(`Flag a broadcast / track`, null, ``, true, {
			overlayClose: false,
			zindex: 1110,
		});
	}

	/**
	 * Add a flag to the WWSU API.
	 *
	 * @param {object} data Data to be passed to the API.
	 * @param {?function} cb Callback executed after the call to the API is complete. True if successful, false otherwise.
	 */
	add(data, cb) {
		try {
			this.manager.get("noReq").request(
				{
					dom: `#modal-${this.flagModal.id}`,
					method: "post",
					url: this.endpoints.add,
					data: data,
				},
				(response) => {
					if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
					if (response !== "OK") {
						$(document).Toasts("create", {
							class: "bg-warning",
							title: "Error flagging",
							body: "There was an error flagging this track or broadcast. Either you already flagged this in the last hour, the track played / the broadcast ended over 30 minutes ago, the track was already disabled, or some other error occurred. If you believe this is an error, please contact the engineer at wwsu4@wright.edu.",
							delay: 30000,
						});
						if (typeof cb === "function") cb(false);
					} else {
						$(document).Toasts("create", {
							class: "bg-success",
							title: "Flagged",
							autohide: true,
							delay: 15000,
							body: `You successfully flagged that track / broadcast. Directors will be notified.`,
						});
						if (typeof cb === "function") cb(true);
					}
				}
			);
		} catch (e) {
			if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
			$(document).Toasts("create", {
				class: "bg-danger",
				title: "Error flagging",
				body: "There was an error flagging this track or broadcast. Please contact the engineer at wwsu4@wright.edu.",
				autohide: true,
				delay: 15000,
				icon: "fas fa-skull-crossbones fa-lg",
			});
			console.error(e);
			if (typeof cb === "function") cb(false);
		}
	}

	/**
	 * Load a form for reporting something.
	 *
	 * @param {object} initialData Initial data to include in the form, such as trackID, attendanceID, and meta.
	 */
	flagForm(initialData) {
		// Create form
		this.flagModal.title = `Flag / Report ${initialData.meta}`;
		this.flagModal.body = ``;
		this.flagModal.iziModal("open");
		$(this.flagModal.body).alpaca({
			schema: {
				type: "object",
				properties: {
					trackID: {
						type: "number",
					},
					attendanceID: {
						type: "number",
					},
					meta: {
						title: "You are reporting",
						type: "string",
						required: true,
						readonly: true,
					},
					reason: {
						title: "Reason for reporting",
						type: "string",
						required: true,
					},
				},
			},
			options: {
				fields: {
					trackID: {
						type: "hidden",
					},
					attendanceID: {
						type: "hidden",
					},
					reason: {
						type: "textarea",
						cols: 40,
						rows: 5,
						helper:
							"Please briefly explain why you are reporting this track / broadcast and what is wrong with it. Repeat false reports can lead to getting banned from the WWSU website. Please do NOT report a track / broadcast simply because you do not like it; only report if there is a technical problem or they contain content inappropriate for radio.",
					},
				},

				form: {
					buttons: {
						submit: {
							title: `Report`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();
								this.add(value, (success) => {
									if (success) {
										this.flagModal.iziModal("close");
									}
								});
							},
						},
					},
				},
			},
			data: initialData,
		});
	}
}
