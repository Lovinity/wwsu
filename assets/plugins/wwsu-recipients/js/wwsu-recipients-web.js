"use strict";

// This class is a recipients helper for web-based recipients, such as listeners.

// REQUIRES these WWSUmodules: noReq (WWSUreq), WWSUevents
class WWSUrecipientsweb extends WWSUevents {
  /**
   * The class constructor.
   *
   * @param {WWSUmodules} manager The modules class which initiated this module
   * @param {object} options Options to be passed to this module
   */
  constructor(manager, options) {
    super();

    this.manager = manager;

    this.endpoints = {
      addDisplay: "/recipients/add-display",
      addWeb: "/recipients/add-web",
      editWeb: "/recipients/edit-web",
    };

    this.recipient = {};
  }

  /**
   * Add this host as a display recipient to the WWSU API (register as online).
   *
   * @param {string} host Name of the host being registered
   * @param {function} cb Callback; recipient data as first parameter, boolean true = success, false = no success as second parameter
   */
  addRecipientDisplay(host, cb) {
    this.manager.get("noReq").request(
      {
        method: "post",
        url: this.endpoints.addDisplay,
        data: { host: host },
      },
      (response2) => {
        try {
          this.recipient = response2;
          this.emitEvent("recipientChanged", [this.recipient]);
          if (response2.label) {
            cb(response2, true);
          } else {
            cb(response2, false);
          }
        } catch (e) {
          cb({}, false);
          console.error(e);
        }
      }
    );
  }

  /**
   * Add this host as a web recipient to the WWSU API (register as online).
   *
   * @param {?string} device OneSignal App ID if applicable (for notifications)
   * @param {function} cb Callback; recipient data as first parameter, boolean true = success, false = no success as second parameter
   */
  addRecipientWeb(device, cb) {
    this.manager.get("noReq").request(
      {
        method: "post",
        url: this.endpoints.addWeb,
        data: { device: device },
      },
      (response2) => {
        try {
          this.recipient = response2;
          this.emitEvent("recipientChanged", [this.recipient]);
          if (response2.label) {
            cb(response2, true);
          } else {
            cb(response2, false);
          }
        } catch (e) {
          this.recipient = {};
          cb({}, false);
          console.error(e);
        }
      }
    );
  }

  /**
   * Edit the nickname for a web recipient.
   *
   * @param {object} data The data to send to the API.
   * @param {string} data.label The new nickname for this recipient if changing nickname
   * @param {string} data.device The new device Id from subscriptions if changing device
   * @param {?function} cb Callback.
   */
  editRecipientWeb(data, cb) {
    this.manager.get("noReq").request(
      {
        method: "post",
        url: this.endpoints.editWeb,
        data: data,
      },
      (response2) => {
        if (data.label) {
          this.recipient.label = `Web (${data.label})`;
        }
        if (data.device) {
          this.recipient.device = data.device;
        }
        this.emitEvent("recipientChanged", [this.recipient]);
        if (typeof cb === "function") cb();
      }
    );
  }
}
