"use strict";
// Manager for the blogs system on the website.
// Does not use WWSUdb; would take too much memory for large blog databases.

// REQUIRES these WWSUmodules: WWSUevents, WWSUnavigation, WWSUMeta, WWSUdb

class WWSUblogsweb extends WWSUevents {
  /**
   * The class constructor.
   *
   * @param {WWSUmodules} manager The modules class which initiated this module
   * @param {object} options Options to be passed to this module
   */
  constructor(manager, options = {}) {
    super();

    this.manager = manager;
    this.options = options;

    // DOM
    this.search = {
      button: undefined,
      box: undefined,
    };
    this.blogContainer;
    this.blogList = undefined;
    this.moreButton = undefined;

    this.lastID = 0;
    this.category = undefined;

    this.cache = new WWSUdb("WWSUblogsweb");
    this.cache
      .db()
      .get()
      .forEach((post) => {
        // Clear expired posts from cache or posts which were made over a month ago
        if (
          moment(post.starts)
            .add(1, "months")
            .isBefore(moment(this.manager.get("WWSUMeta").meta.time)) ||
          moment(post.expires).isSameOrAfter(
            moment(this.manager.get("WWSUMeta").meta.time)
          )
        )
          this.cache.query({ remove: post.ID });
      });

    this.endpoints = {
      add: "/blogs/add-web",
      getOne: "/blogs/get-one-web",
      get: "/blogs/get-web",
    };

    // Custom socket
    this.manager.socket.on("blogs-web", (data) => {
      for (let action in data) {
        if (!Object.prototype.hasOwnProperty.call(data, action)) continue;

        // TODO

        switch (action) {
          case "insert":
            break;
          case "update":
            break;
          case "remove":
            break;
        }
      }
    });

    // Navigation event emitter for replacing the search box with previous search queries
    this.manager
      .get("WWSUNavigation")
      .on("popState", "WWSUblogsweb", (data) => {
        if (!data.WWSUblogsweb || !data.WWSUblogsweb.search) return;
        $(this.search.box).val(data.WWSUblogsweb.search);
      });
  }

  /**
   * Initialize web blogs system. Should be called immediately after loading the module.
   *
   * @param {string} blogContainer DOM query string of the entire container used for blog post browsing.
   * @param {string} blogList DOM query string where blog cards are placed.
   * @param {string} moreButton DOM query string of the "Load More Blog Posts" button.
   * @param {string} searchButton DOM query string of the search button.
   * @param {string} searchBox DOM query string of the input box for search terms.
   */
  init(blogContainer, blogList, moreButton, searchButton, searchBox) {
    if (this.search.button) $(this.search.button).off("click");
    if (this.moreButton) $(this.moreButton).off("click");

    // Load DOMs into memory
    this.blogContainer = blogContainer;
    this.blogList = blogList;
    this.moreButton = moreButton;
    this.search.button = searchButton;
    this.search.box = searchBox;

    // Add click handler for blog searching
    $(this.search.button).on("click", () => {
      this.lastID = 0;
      $(this.blogList).html("");
      this.getBlogs({
        limit: 9,
        search: $(this.search.box).val(),
        category: this.category,
      });
    });

    // More button click
    $(this.moreButton).on("click", () => {
      this.getBlogs({
        limit: 9,
        search: $(this.search.box).val(),
        category: this.category,
      });
    });

    // Add pushState data to WWSUNavigation
    this.manager.get("WWSUNavigation").pushStateFns.set("WWSUblogsweb", () => {
      return {
        WWSUblogsweb: {
          search: $(this.search.box).val(), // We should remember search queries from past history
        },
      };
    });
  }

  /**
   * This should be called whenever we want to view/refresh blogs of a category.
   *
   * @param {boolean} reload Whether or not to actually reload blog posts from the API. Otherwise, will load cached posts.
   * @param {string} category The category of blog posts to retrieve; do not define to retrieve all.
   */
  list(reload, category) {
    this.category = category;
    this.lastID = 0;
    $(this.blogList).html("");

    // Only check the API for blogs if reload is true AND we are connected to websocket. Otherwise, use cache / localStorage.
    if (reload && this.manager.socket.isConnected()) {
      // Check the API for blog posts and load them.
      this.getBlogs({
        limit: 9,
        search: $(this.search.box).val(),
        category: this.category,
      });
    } else {
      // Display blog posts from the cache
      this.cache
        .db()
        .get()
        .forEach((post) => {
          // Skip blogs not in this category if a category was defined
          if (category && post.categories.indexOf(category) === -1) return;

          // Skip by search term if it does not match
          let search = $(this.search.box).val();
          if (
            !post.title.includes(search) &&
            !post.summary.includes(search) &&
            !post.contents.includes(search) &&
            (!post.tags || post.tags.indexOf(search) === -1)
          )
            return;

          // Mark the last ID when we fetch more posts.
          if (post.ID > this.lastID) this.lastID = post.ID;

          // Populate the cards
          $(this.blogList).append(this.summaryCard(post));
        });
      // Make sure button to load more blog posts is active; do even if cache is empty as there may have been a new post made that was not yet fetched.
      $(this.moreButton).prop("disabled", false);
      $(this.moreButton).removeClass("btn-outline-danger");
      $(this.moreButton).addClass("btn-secondary");
      $(this.moreButton).html("Load More Blog Posts");
    }
  }

  /**
   * Get blog posts from the WWSU API and populate them on the page.
   *
   * @param {object} data Data to pass to the API.
   * @param {function} cb Callback executed with the response passed as a parameter.
   */
  getBlogs(data, cb) {
    if (typeof data.lastID === "undefined") data.lastID = this.lastID;

    try {
      this.manager.get("noReq").request(
        {
          dom: this.blogContainer,
          method: "post",
          url: this.endpoints.get,
          data: data,
        },
        (response) => {
          if (response.constructor !== Array) {
            if (this.manager.has("WWSUehhh"))
              this.manager.get("WWSUehhh").play();
            $(document).Toasts("create", {
              class: "bg-danger",
              title: "Error fetching blog posts",
              body: "There was an error fetching blog posts. Please report this to the engineer.",
              autohide: true,
              delay: 10000,
              icon: "fas fa-skull-crossbones fa-lg",
            });
            if (typeof cb === "function") cb(false);
          } else {
            if (response.length > 0) {
              this.lastID = 0;
              response.forEach((post) => {
                // Mark the last ID when we fetch more posts.
                if (post.ID > this.lastID) this.lastID = post.ID;

                // Populate the cards
                $(this.blogList).append(this.summaryCard(post));

                // Add to cache if the post was less than a month ago, else remove it from cache if it exists
                if (
                  moment(post.starts)
                    .add(1, "months")
                    .isAfter(moment(this.manager.get("WWSUMeta").meta.time)) &&
                  moment(post.expires).isBefore(
                    moment(this.manager.get("WWSUMeta").meta.time)
                  )
                ) {
                  let record = this.cache.find({ ID: post.ID }, true);
                  if (!record) {
                    this.cache.query({ insert: post });
                  } else {
                    this.cache.query({ update: post });
                  }
                } else {
                  this.cache.query({ remove: post.ID });
                }
              });

              // Re-add click handlers for the cards via WWSUnavigation
              this.manager.get("WWSUNavigation").addItem(
                ".nav-blog",
                "#section-blog",
                (e) =>
                  e
                    ? `${$(e)
                        .find(".card-title")
                        .first()
                        .html()} - WWSU 106.9 FM`
                    : `Blog Post ${$(e).data("id")} - WWSU 106.9 FM`,
                (e) =>
                  `/blog${
                    e ? `/${$(e).data("id")}/${$(e).data("monikor")}` : ``
                  }`,
                false,
                (e, popState) => {
                  this.loadBlogPost($(e).data("id"));
                }
              );

              // Make sure button to load more blog posts is active
              $(this.moreButton).prop("disabled", false);
              $(this.moreButton).removeClass("btn-outline-danger");
              $(this.moreButton).addClass("btn-secondary");
              $(this.moreButton).html("Load More Blog Posts");
            } else {
              // No more blog posts; disable button to load more
              $(this.moreButton).prop("disabled", true);
              $(this.moreButton).addClass("btn-outline-danger");
              $(this.moreButton).removeClass("btn-secondary");
              $(this.moreButton).html("No More Blog Posts");
            }
            if (typeof cb === "function") cb(response);
          }
        }
      );
    } catch (e) {
      if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
      $(document).Toasts("create", {
        class: "bg-danger",
        title: "Error fetching blog posts",
        body: "There was an error fetching blog posts. Please report this to the engineer.",
        autohide: true,
        delay: 10000,
        icon: "fas fa-skull-crossbones fa-lg",
      });
      console.error(e);
      if (typeof cb === "function") cb(false);
    }
  }

  /**
   * Load a single blog post entry.
   *
   * @param {number} id ID of the post to load / show
   * @param {function} cb Optional callback with response data as parameter
   */
  loadBlogPost(id, cb) {
    try {
      this.manager.get("noReq").request(
        {
          dom: `#section-blog`,
          method: "post",
          url: this.endpoints.getOne,
          data: { ID: id },
        },
        (response) => {
          if (!response || !response.ID) {
            if (this.manager.has("WWSUehhh"))
              this.manager.get("WWSUehhh").play();
            $(document).Toasts("create", {
              class: "bg-danger",
              title: "Error fetching blog post",
              body: "There was an error fetching the blog post. Please report this to the engineer.",
              autohide: true,
              delay: 10000,
              icon: "fas fa-skull-crossbones fa-lg",
            });
            if (typeof cb === "function") cb(false);
          } else {
            if (typeof cb === "function") cb(response);

            this.manager.get("WWSUNavigation").addItem(
              ".nav-blogs-subcategory",
              "#section-blogs",
              (e) =>
                e
                  ? `${
                      $(e).data("category") || "All"
                    } Blog Posts - WWSU 106.9 FM`
                  : `All Blog Posts - WWSU 106.9 FM`,
              (e) =>
                `/blogs${
                  e && $(e).data("category") ? `/${$(e).data("category")}` : ``
                }`,
              false,
              (e, popState) => {
                this.manager
                  .get("WWSUNavigation")
                  .processMenu(
                    `#nav-blogs${
                      e && $(e).data("category")
                        ? `-${$(e).data("category")}`
                        : ``
                    }`
                  );
              }
            );

            // Author constant function

            const updateAuthorInfo = (author) => {
              // Author information
              if (author) {
                $("#section-blog-author-card").removeClass("d-none");
                $("#section-blog-author").html(
                  author.realName || author.name || "Unknown Author"
                );
                $("#section-blog-author-card-realName").html(
                  author.realName || `Unknown Author Name`
                );
                $("#section-blog-author-card-name").html(
                  author.realName || `Unknown Author Nickname`
                );
                $("#section-blog-author-card-profile").html(
                  author.profile || `No profile provided`
                );
                if (author.avatar) {
                  $("#section-blog-author-avatar").removeClass("d-none");
                  $("#section-blog-author-avatar").attr(
                    "src",
                    `https://server.wwsu1069.org/uploads/get/${author.avatar}`
                  );
                  $("#section-blog-author-card-avatar").removeClass("d-none");
                  $("#section-blog-author-card-avatar").attr(
                    "src",
                    `https://server.wwsu1069.org/uploads/get/${author.avatar}`
                  );
                } else {
                  $("#section-blog-author-avatar").addClass("d-none");
                  $("#section-blog-author-card-avatar").addClass("d-none");
                }
              } else {
                $("#section-blog-author-card").addClass("d-none");
                $("#section-blog-author-avatar").addClass("d-none");
                $("#section-blog-author").html("Unknown Author");
              }
            };

            // Get post author
            let author;
            if (response.author) {
              author = this.manager
                .get("WWSUdjs")
                .find({ ID: response.author }, true);

              // No author found? Maybe WWSUdjs has not loaded yet. Add a one-off event handler.
              if (!author) {
                this.manager
                  .get("WWSUdjs")
                  .once("change", "WWSUblogsweb", (db) => {
                    author = this.manager
                      .get("WWSUdjs")
                      .find({ ID: response.author }, true);
                    updateAuthorInfo(author);
                  });
              }
            }

            updateAuthorInfo(author);

            // Post title
            $("#section-blog-title").html(response.title);

            // Publication date
            $("#section-blog-publication").html(
              `Published ${moment(response.starts || response.createdAt).format(
                "MMM D, YYYY, h:mm A"
              )}`
            );

            // Tags
            let tagsHtml = ``;
            if (response.tags && response.tags.length) {
              tagsHtml = `Tags: ${response.tags
                .map(
                  (tag) =>
                    `<span class="p-1 m-1 badge badge-secondary nav-blogs-tag" style="cursor: pointer;" tabindex="-1" role="link" data-search="${tag}" aria-label="${tag} tag">${tag}</span>`
                )
                .join("")}`;
            }
            $("#section-blog-tags").html(tagsHtml);

            // Tags click event via navigation
            this.manager
              .get("WWSUNavigation")
              .addItem(
                ".nav-blogs-tag",
                "#section-blogs",
                `Blog Posts - WWSU 106.9 FM`,
                `/blogs`,
                false,
                (e, popState) => {
                  $(this.search.box).val($(e).data("search"));
                  this.lastID = 0;
                  $(this.blogList).html("");
                  this.getBlogs({
                    limit: 9,
                    search: $(this.search.box).val(),
                  });
                }
              );

            // Featured image
            if (response.featuredImage) {
              $("#section-blog-featuredImage").removeClass("d-none");
              $("#section-blog-featuredImage").attr(
                "src",
                `https://server.wwsu1069.org/uploads/get/${response.featuredImage}`
              );
            } else {
              $("#section-blog-featuredImage").addClass("d-none");
            }

            // Post contents
            $("#section-blog-contents").html(response.contents);
          }
        }
      );
    } catch (e) {
      if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
      $(document).Toasts("create", {
        class: "bg-danger",
        title: "Error fetching blog post",
        body: "There was an error fetching the blog post. Please report this to the engineer.",
        autohide: true,
        delay: 10000,
        icon: "fas fa-skull-crossbones fa-lg",
      });
      console.error(e);
      if (typeof cb === "function") cb(false);
    }
  }

  /**
   * Generate HTML for a small blog summary card.
   * Remember to re-bind nav-blog.
   *
   * @param {object} blog The blog record.
   * @returns {string} HTML for the summary
   */
  summaryCard(blog) {
    return `
    <div class="col-md-12 col-lg-6 col-xl-4">
    <div
      class="card mb-2"
      style="
        background: linear-gradient(
            rgba(0, 0, 0, 0.75),
            rgba(0, 0, 0, 0.75)
          )${
            blog.featuredImage
              ? `, url('https://server.wwsu1069.org/uploads/get/${blog.featuredImage}') center center`
              : ``
          };
        min-height: 275px;
      "
    >
      <a
        class="
          card-img-overlay
          d-flex
          flex-column
          justify-content-end
          nav-blog
        "
        data-id="${blog.ID}"
        data-monikor="${this.toMonikor(blog.title)}"
        href="#"
      >
        <h2
          class="
            card-title
            text-white
            font-weight-bold
            h5
          "
        >
          ${blog.title}
        </h2>
        <p class="card-text text-white pb-2 pt-1">
          ${blog.summary}
        </p>
        <div class="text-white">Published ${moment(
          blog.starts || blog.createdAt
        ).format("MMM D, YYYY, h:mm A")}</div>
      </a>
    </div>
    </div>`;
  }

  /**
   * Convert a blog title to a monikor
   *
   * @param {string} title Title of the blog
   * @returns {string} URL monikor
   */
  toMonikor(title) {
    return title
      .toLowerCase() // Make everything lower case
      .replace(/\s/g, "-") // Replace all whitespace with a hyphen
      .replace("&", "and") // Replace ampersands with the word and
      .replace("_", "-") // Replace underscores with hyphens
      .replace(/[^0-9a-z\-]/gi, ""); // Remove all remaining non-alphanumeric characters unless it's a hyphen
  }
}
