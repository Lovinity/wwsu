"use strict";

/* global WWSUdb */

// This class manages requesting tracks on the WWSU website
// Event emitter also supports 'trackRequested' with the request object as a parameter

// REQUIRES these WWSUmodules: WWSUMeta, WWSUsongs, WWSUutil, WWSUanimations, noReq (WWSUreq)
class WWSUrequestsweb extends WWSUevents {
  /**
   * Create the announcements class.
   *
   * @param {WWSUmodules} manager The modules class which initiated this module
   * @param {object} options Options to be passed to this module
   */
  constructor(manager, options) {
    super(); // Create the events

    this.manager = manager;

    this.endpoints = {
      place: "/requests/place",
    };
    this.data = {
      get: {},
    };

    this.tableRows;
    this.searchField;
    this.searchGenre;
    this.searchButton;

    this.skip = 0;
  }

  /**
   * Initialize the table for browsing tracks to request and requesting them.
   *
   * @param {string} tableRows DOM query string where rows for the table will be placed.
   * @param {string} searchField DOM query string of the input box where listeners can search by artist or title
   * @param {string} searchGenre DOM query string of the select box that will be populated with genres to select / filter by.
   * @param {string} searchButton DOM query string of the button to click to execute the filter/search.
   * @param {string} loadMoreButton DOM query string of the button to click to load more tracks.
   */
  initTable(tableRows, searchField, searchGenre, searchButton, loadMoreButton) {
    this.tableRows = tableRows;
    this.searchField = searchField;
    this.searchGenre = searchGenre;
    this.searchButton = searchButton;
    this.loadMoreButton = loadMoreButton;

    // Create table
    this.manager.get("WWSUanimations").add("requests-init-track-table", () => {
      this.clearTable();
    });

    // Add click handlers for buttons
    $(this.searchButton).click((e) => {
      $(this.searchButton).block({
        message: `...`,
        css: { border: "3px solid #a00" },
        timeout: 15000,
        onBlock: () => {
          this.clearTable();
          this.populateTable();
        },
      });
    });

    $(this.loadMoreButton).click((e) => {
      $(this.loadMoreButton).block({
        message: `...`,
        css: { border: "3px solid #a00" },
        timeout: 15000,
        onBlock: () => {
          this.populateTable();
        },
      });
    });
  }

  /**
   * Place a track request via the WWSU API.
   *
   * @param {string} dom The DOM query string to block while processing.
   * @param {object} data The data to pass to the API.
   * @param {?function} cb Callback function with true if success, false if not.
   */
  place(dom, data, cb) {
    try {
      this.manager
        .get("noReq")
        .request(
          { dom: dom, method: "post", url: this.endpoints.place, data },
          (response) => {
            if (!response.requested) {
              $(document).Toasts("create", {
                class: "bg-danger",
                title: "Error placing request",
                body:
                  response.message ||
                  `There was an error placing the request. Maybe this track cannot be requested right now?`,
                autohide: true,
                delay: 10000,
                icon: "fas fa-skull-crossbones fa-lg",
              });
              if (typeof cb === "function") {
                cb(false);
              }
            } else {
              $(document).Toasts("create", {
                class: "bg-success",
                title: "Request placed!",
                autohide: true,
                delay: 15000,
                body:
                  response.message ||
                  `Your request was placed! Requests play after breaks in automation. When a show is broadcasting, it is up to DJ/host discretion.`,
              });
              if (typeof cb === "function") {
                cb(true);
              }
            }
          }
        );
    } catch (e) {
      $(document).Toasts("create", {
        class: "bg-danger",
        title: "Error placing request",
        body: "There was an error placing the request. Please report this to the engineer at wwsu4@wright.edu.",
        autohide: true,
        delay: 10000,
        icon: "fas fa-skull-crossbones fa-lg",
      });
      if (typeof cb === "function") {
        cb(false);
      }
      console.error(e);
    }
  }

  /**
   * Clear the track table if it exists.
   */
  clearTable() {
    this.manager.get("WWSUanimations").add("tracks-clear-table", () => {
      if (this.tableRows) {
        $(this.tableRows).html(``);
        this.skip = 0;
      }
    });
  }

  /**
   * Update the track requests table if it exists. Also update track request notification badge and icon.
   */
  populateTable() {
    this.manager.get("WWSUanimations").add("tracks-populate-table", () => {
      if (this.tableRows) {
        let query = {
          search: $(this.searchField).val(),
          skip: this.skip,
          limit: 50,
          ignoreDisabled: true,
          ignoreNonMusic: true,
        };
        let selectedOption = $(this.searchGenre)
          .children("option:selected")
          .val();
        if (selectedOption !== "0") {
          query.genre = parseInt(selectedOption);
        }
        this.manager.get("WWSUsongs").get(query, (tracks) => {
          if (tracks.constructor === Array && tracks.length > 0) {
            let html = ``;
            this.skip += tracks.length;
            tracks.forEach((track) => {
              html += `<tr>
                <td>${track.artist} - ${track.title}</td>
                <td><button class="btn btn-sm bg-blue btn-track-view" data-id="${track.ID}" title="View more info / Request Track"><i class="fas fa-eye p-1"></i>View / Request</button></td>
              </tr>`;
            });
            $(this.tableRows).append(html);

            // Action button click events
            $(".btn-track-view").unbind("click");

            $(".btn-track-view").click((e) => {
              this.manager
                .get("WWSUsongs")
                .showTrackInfo(
                  parseInt($(e.currentTarget).data("id")),
                  false,
                  true
                );
            });
          } else {
            $(document).Toasts("create", {
              class: "bg-warning",
              autohide: true,
              delay: 5000,
              title: "No more tracks",
              body: "There are no more tracks to load for your search.",
            });
          }
          $(this.searchButton).unblock();
          $(this.loadMoreButton).unblock();
        });
      } else {
        $(this.searchButton).unblock();
        $(this.loadMoreButton).unblock();
      }
    });
  }
}
