// This is the service worker with the combined offline experience (Offline page + Offline copy of pages)

const CACHE = "wwsu-cache";
const QUEUE_NAME = "wwsu-queue";

importScripts(
  "https://storage.googleapis.com/workbox-cdn/releases/6.2.0/workbox-sw.js"
);

const offlineFallbackPage = "/offline.html";
const logo = "app/icon.png";

const HTML_CACHE = "wwsu-html";
const JS_CACHE = "wwsu-javascript";
const STYLE_CACHE = "wwsu-stylesheets";
const IMAGE_CACHE = "wwsu-images";
const FONT_CACHE = "wwsu-fonts";

// Background sync
const bgSyncPlugin = new workbox.backgroundSync.BackgroundSyncPlugin(
  QUEUE_NAME,
  {
    maxRetentionTime: 60, // Retry for max of 1 hour
  }
);

self.addEventListener("message", (event) => {
  if (event.data && event.data.type === "SKIP_WAITING") {
    self.skipWaiting();
  }
});

// pre-cache offline assets
workbox.precaching.precacheAndRoute([
  { url: offlineFallbackPage, revision: "1" },
  { url: logo, revision: null },
]);

workbox.routing.setCatchHandler(async ({ event }) => {
  // Return the precached offline page if a document is being requested
  if (event.request.destination === "document") {
    return workbox.precaching.matchPrecache("/offline.html");
  }

  return Response.error();
});

workbox.routing.registerRoute(
  ({ event }) => event.request.destination === "document",
  new workbox.strategies.CacheFirst({
    cacheName: HTML_CACHE,
    plugins: [
      new workbox.expiration.ExpirationPlugin({
        maxAgeSeconds: 24 * 60 * 60, // 1 day
        purgeOnQuotaError: true,
      }),
      bgSyncPlugin,
    ],
  })
);

workbox.routing.registerRoute(
  ({ event }) => event.request.destination === "script",
  new workbox.strategies.CacheFirst({
    cacheName: JS_CACHE,
    plugins: [
      new workbox.expiration.ExpirationPlugin({
        maxAgeSeconds: 7 * 24 * 60 * 60, // 1 week
        purgeOnQuotaError: true,
      }),
      bgSyncPlugin,
    ],
  })
);

workbox.routing.registerRoute(
  ({ event }) => event.request.destination === "style",
  new workbox.strategies.CacheFirst({
    cacheName: STYLE_CACHE,
    plugins: [
      new workbox.expiration.ExpirationPlugin({
        maxAgeSeconds: 7 * 24 * 60 * 60, // 1 week
        purgeOnQuotaError: true,
      }),
      bgSyncPlugin,
    ],
  })
);

workbox.routing.registerRoute(
  ({ event }) => event.request.destination === "image",
  new workbox.strategies.CacheFirst({
    cacheName: IMAGE_CACHE,
    plugins: [
      new workbox.expiration.ExpirationPlugin({
        maxAgeSeconds: 7 * 24 * 60 * 60, // 1 week
        purgeOnQuotaError: true,
      }),
      bgSyncPlugin,
    ],
  })
);

workbox.routing.registerRoute(
  ({ event }) => event.request.destination === "font",
  new workbox.strategies.CacheFirst({
    cacheName: FONT_CACHE,
    plugins: [
      new workbox.expiration.ExpirationPlugin({
        maxAgeSeconds: 7 * 24 * 60 * 60, // 1 week
        purgeOnQuotaError: true,
      }),
      bgSyncPlugin,
    ],
  })
);

workbox.routing.registerRoute(
  new RegExp("/*"),
  new workbox.strategies.CacheFirst({
    cacheName: CACHE,
    plugins: [
      new workbox.expiration.ExpirationPlugin({
        maxAgeSeconds: 7 * 24 * 60 * 60, // 1 week
        purgeOnQuotaError: true,
      }),
      bgSyncPlugin,
    ],
  })
);
