/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also do this by creating a hook.
 *
 * For more information on bootstrapping your app, check out:
 * https://sailsjs.com/config/bootstrap
 */
const cron = require("node-cron");

// Discord should be accessible application-wide
global["Discord"] = require("discord.js");

module.exports.bootstrap = async function (done) {
  sails.log.verbose(`BOOTSTRAP: started`);

  // Instantiate a Discord client globally; we cannot do this in the initializer because it won't be global.
  global["DiscordClient"] = sails.config.custom.discord.clientOptions
    ? new Discord.Client(sails.config.custom.discord.clientOptions)
    : undefined;

  // Run initialize functions for sails.models models
  // NOTE! They should be async and should NOT depend on each other (order is not guaranteed).
  // NOTE! Initialize functions should return a string if anything should be logged after it is done.
  if (sails.models) {
    for (let model in sails.models) {
      if (
        !Object.prototype.hasOwnProperty.call(sails.models, model) ||
        typeof sails.models[model].initialize !== "function"
      )
        continue;
      sails.log.verbose(`BOOTSTRAP: initializing model ${model}`);
      let results = await sails.models[model].initialize();
      if (results) {
        sails.log.verbose(`BOOTSTRAP Model Initialize (${model}): ${results}`);
      } else {
        sails.log.verbose(`BOOTSTRAP Model Initialize (${model}): DONE`);
      }
    }
  }

  // Run bootstrap initializers under sails.helpers.bootstrap.initialize
  // NOTE! The helpers should return a string if they should log something when done.
  if (
    sails.helpers &&
    sails.helpers.bootstrap &&
    sails.helpers.bootstrap.initializers
  ) {
    for (let initializer in sails.helpers.bootstrap.initializers) {
      if (
        !Object.prototype.hasOwnProperty.call(
          sails.helpers.bootstrap.initializers,
          initializer
        )
      )
        continue;
      sails.log.verbose(`BOOTSTRAP: Running initializer ${initializer}`);
      let results = await sails.helpers.bootstrap.initializers[initializer]();
      if (results) {
        sails.log.verbose(`BOOTSTRAP initializer (${initializer}): ${results}`);
      } else {
        sails.log.verbose(`BOOTSTRAP initializer (${initializer}): DONE`);
      }
    }
  }

  // Schedule cron jobs defined in sails.helpers.bootstrap.cron
  // NOTE! These should have a schedule property in the export containing the cronTab string!
  if (
    sails.helpers &&
    sails.helpers.bootstrap &&
    sails.helpers.bootstrap.cron
  ) {
    for (let cronTask in sails.helpers.bootstrap.cron) {
      if (
        !Object.prototype.hasOwnProperty.call(
          sails.helpers.bootstrap.cron,
          cronTask
        )
      )
        continue;
      sails.log.verbose(`BOOTSTRAP: Scheduling cron ${cronTask}`);

      let info = sails.helpers.bootstrap.cron[cronTask].toJSON();
      cron.schedule(info.schedule, async () => {
        if (sails._exiting) return; // Do not run any crons if we are shutting down

        sails.log.verbose(`BOOTSTRAP cron: Running ${cronTask}`);

        try {
          let results = await sails.helpers.bootstrap.cron[cronTask]();
          if (results) {
            sails.log.verbose(`BOOTSTRAP cron ${cronTask}: ${results}`);
          } else {
            sails.log.verbose(`BOOTSTRAP cron ${cronTask}: DONE`);
          }
        } catch (e) {
          sails.log.error(e);
        }
      });
    }
  }

  // Log that the server was rebooted
  sails.log.verbose(`BOOTSTRAP: logging reboot`);

  await sails.models.logs
    .create({
      attendanceID: null,
      logtype: "reboot",
      loglevel: "info",
      logsubtype: "automation",
      logIcon: `fas fa-exclamation-triangle`,
      title: `The Node server was started.`,
      event: `If this reboot was not intentional, please check the server and error logs.`,
    })
    .fetch()
    .tolerate((err) => {
      // Don't throw errors, but log them
      sails.log.error(err);
    });

  sails.log.verbose(`BOOTSTRAP: Done.`);

  return done();
};
