/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
  //  ╦ ╦╔═╗╔╗ ╔═╗╔═╗╔═╗╔═╗╔═╗
  //  ║║║║╣ ╠╩╗╠═╝╠═╣║ ╦║╣ ╚═╗
  //  ╚╩╝╚═╝╚═╝╩  ╩ ╩╚═╝╚═╝╚═╝

  /***************************************************************************
   *                                                                          *
   * Make the view located at `views/homepage.ejs` your home page.            *
   *                                                                          *
   * (Alternatively, remove this and add an `index.html` file in your         *
   * `assets` directory)                                                      *
   *                                                                          *
   ***************************************************************************/

  /***************************************************************************
   *                                                                          *
   * More custom routes here...                                               *
   * (See https://sailsjs.com/config/routes for examples.)                    *
   *                                                                          *
   * If a request to a URL doesn't match any of the routes in this file, it   *
   * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
   * not match any of those, it is matched against static assets.             *
   *                                                                          *
   ***************************************************************************/

  /*
      UPLOADS GETTER
  */

  "GET /uploads/get/:id": function (req, res) {
    sails.models.uploads
      .findOne({ ID: req.param("id") })
      .then((record) => {
        res.sendFile(record.path);
      })
      .catch((e) => {
        res.sendStatus(404);
      });
  },

  /*
      CATEGORY BLOGS
  */

  "GET /blogs/:category?": { action: "blogs/index" },

  /*
      INDIVIDUAL BLOGS
  */

  "GET /blog/:id/:monikor?": { action: "blog/index" },

  /*
      DIRECTORS PANEL
  */

  "GET /directors/panel/director/:director": function (req, res) {
    return res.view("directors/director", {
      layout: "directors/layout",
      director: req.param("director"),
    });
  },

  /*
      REDIRECTS
  */

  "GET /recordings":
    "https://raidermailwright-my.sharepoint.com/:f:/g/personal/wwsu4_wright_edu/EtXkb0rxp8RLgDIkUF7qg-QB-MQ1vxguf-qUrx0dUPq5eg",
  "GET /video/embed": "https://wwsuvideo.pdstig.me",
};
