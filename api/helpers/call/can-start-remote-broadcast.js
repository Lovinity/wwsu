module.exports = {
  friendlyName: "call.canStartRemoteBroadcast",

  description: "Check if a provided host can start a remote broadcast",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The host ID to check",
    },
    type: {
      type: "string",
      isIn: ["remote", "sports"],
      description:
        "Remote for standard remote broadcasts, sports for remote sports broadcasts. Blank for either.",
    },
  },

  fn: async function (inputs) {
    // sails.js modifies query strings; dumb decision TBH. We need to clone inputs to avoid bugs.
    let criteria = _.cloneDeep({
      ID: inputs.ID,
      type: inputs.type,
    });

    let host = await sails.models.hosts.findOne({
      ID: criteria.ID,
      authorized: true,
    });

    // Failsafe; deny if host does not exist or is not authorized
    if (!host) return false;

    // If no belongsTo is set, it is assumed this host is a WWSU computer. Allow remote broadcasting.
    if (!host.belongsTo) return true;

    if (host.belongsTo) {
      // Locate the member the host belongs to. If not found or inactive, this host cannot broadcast remotely.
      let dj = await sails.models.djs.findOne({
        ID: host.belongsTo,
        active: true,
      });
      if (!dj) return false;

      // Member has permission to start remote broadcasts from their own computer. Allow it.
      if (criteria.type) {
        if (dj.permissions.indexOf(criteria.type) !== -1) return true;
      } else {
        if (
          dj.permissions.indexOf("sports") !== -1 ||
          dj.permissions.indexOf("remote") !== -1
        )
          return true;
      }
    }

    // By this point, remote broadcasting is not allowed
    return false;
  },
};
