module.exports = {
  friendlyName: "helpers.break.checkClockwheel",

  description: "Check for, and queue when necessary, clockwheel breaks.",

  inputs: {
    triggeredByBootstrap: {
      type: "boolean",
      defaultsTo: false,
      description:
        "If true, we will use queue duration checks when factoring whether or not to queue a break. Also, we will check for and queue liners and track requests when applicable.",
    },
  },

  fn: async function (inputs) {
    // Do not run this process if triggered by bootstrap and either:
    // a. We are changing states
    // b. We suspect the current queue length is not accurate right now
    // c. We are not in automation or a prerecord
    // d. We cannot get the currently playing track (or it has no Duration or Elapsed specified)
    if (
      inputs.triggeredByBootstrap &&
      (sails.models.meta.memory.changingState !== null ||
        sails.models.status.errorCheck.trueZero > 0 ||
        (["automation_on", "automation_genre", "automation_playlist"].indexOf(
          sails.models.meta.memory.state
        ) === -1 &&
          !sails.models.meta.memory.state.startsWith("prerecord_")) ||
        typeof sails.models.meta.automation[0] === "undefined" ||
        typeof sails.models.meta.automation[0].Duration === "undefined" ||
        typeof sails.models.meta.automation[0].Elapsed === "undefined")
    )
      return;

    let trackDuration = parseFloat(
      sails.models.meta.automation[1]
        ? sails.models.meta.automation[1].Duration
        : 0
    );
    if (typeof trackDuration !== "number") trackDuration = 0;

    // Workaround for RadioDJ 2.0.3.4 bug; tracks with unknown duration sometimes return a massively large duration that triggers invalid dates
    try {
      let justToThrowAnErrorIfNecessary = moment()
        .add(trackDuration, "seconds")
        .toISOString(true);
    } catch (e) {
      trackDuration = 0;
    }

    let trackLength = sails.models.meta.automation[0]
      ? parseFloat(sails.models.meta.automation[0].Duration) -
        parseFloat(sails.models.meta.automation[0].Elapsed)
      : 0;
    if (typeof trackLength !== "number") trackLength = 0;

    // Workaround for RadioDJ 2.0.3.4 bug; tracks with unknown duration sometimes return a massively large duration that triggers invalid dates
    try {
      let justToThrowAnErrorIfNecessary = moment()
        .add(trackLength, "seconds")
        .toISOString(true);
    } catch (e) {
      trackLength = 0;
    }

    // Bail if the currently playing track has breakCheck minutes or more left (resolves a discrepancy)
    if (
      inputs.triggeredByBootstrap &&
      trackLength >= 60 * sails.config.custom.breakCheck
    )
      return;

    // If triggered by bootstrap, determine when the current track in RadioDJ will finish. Otherwise, set this to now.
    let endTime = inputs.triggeredByBootstrap
      ? moment().add(trackLength, "seconds")
      : moment();

    // Sometimes track end time will be before current time (negative) if there is no known duration. Reset to now if this is the case.
    if (moment().isAfter(moment(endTime))) endTime = moment();

    // Determine the top of the current hour
    let topOfHour = moment().startOf("hour");

    // Determines which clockwheel break (minute) we will be focused on
    let key = 0;

    // Determines the scheduled time for this break
    let breakTime;

    // Always consider a break if not triggered by bootstrap
    let doBreak = !inputs.triggeredByBootstrap;

    // Use top of hour break if previous ID was over 10 minutes ago and current time is between :55 and :05
    if (
      moment(sails.models.status.errorCheck.prevID)
        .add(10, "minutes")
        .isBefore(moment()) &&
      (moment().minute() >= 55 || moment().minute() < 5)
    ) {
      key = 0;
      breakTime =
        moment().minute() < 5
          ? moment(topOfHour)
          : moment(topOfHour).add(1, "hours");
      doBreak = true;
    } else {
      // Determine which clockwheel break we should focus on based on which one the current time is closest

      let clockwheels = [];

      // Go through each clockwheel break and determine the difference in minutes between it and the current time
      for (let i in sails.config.custom.breaks) {
        if (
          !Object.prototype.hasOwnProperty.call(
            sails.config.custom.breaks,
            i
          ) ||
          i === 0
        )
          continue;

        // Determine if we should focus on breakTime for the current hour or breakTime for the next hour.
        // If prevBreak (+ half of breakCheck) minute is before this clockwheel, focus on current hour, else next hour.
        let _breakTime =
          moment(sails.models.status.errorCheck.prevBreak).minute() +
            sails.config.custom.breakCheck / 2 <
          i
            ? moment(topOfHour).add(i, "minutes")
            : moment(topOfHour).add(1, "hours").add(i, "minutes");

        clockwheels.push({
          key: i,
          breakTime: _breakTime,
          diff: Math.abs(moment(endTime).diff(_breakTime)), // Diff should use absolute value; we're using + or - distance.
        });
      }

      // Sort by diff from smallest to largest, and choose the one with the smallest diff.
      clockwheels.sort((a, b) => a.diff - b.diff);
      key = clockwheels[0].key;
      breakTime = clockwheels[0].breakTime;
    }

    sails.log.debug("KEY is " + key);

    // Now, determine if we should queue the break.

    // If the current time is before scheduled break, but the currently playing track will finish after scheduled break (unless not triggered by bootstrap), consider queuing the break.
    if (
      inputs.triggeredByBootstrap &&
      moment().isBefore(moment(breakTime)) &&
      moment(endTime).isSameOrAfter(moment(breakTime))
    ) {
      doBreak = true;
    }

    // If the currently playing track will not end after the scheduled break,
    // but the following track will end further after the scheduled break than the current track would,
    // queue the break early.
    if (
      inputs.triggeredByBootstrap &&
      typeof sails.models.meta.automation[1] !== "undefined" &&
      typeof sails.models.meta.automation[1].Duration !== "undefined"
    ) {
      let distancebefore;
      let distanceafter;
      let endtime2;

      distancebefore = moment(breakTime).diff(moment(endTime));
      endtime2 = moment(endTime).add(trackDuration, "seconds");
      distanceafter = endtime2.diff(breakTime);
      if (
        moment(endtime2).isAfter(moment(breakTime)) &&
        distanceafter > distancebefore
      ) {
        doBreak = true;
      }
    }

    // Do not queue if this clockwheel break already ran recently (judged by breakCheck)
    if (
      key !== 0 &&
      moment(sails.models.status.errorCheck.prevBreak)
        .add(sails.config.custom.breakCheck, "minutes")
        .isSameOrAfter(breakTime)
    ) {
      doBreak = false;
    }

    // Do the break if we are supposed to
    if (doBreak) {
      // Reset the break clock
      sails.models.status.errorCheck.prevBreak = breakTime;

      // Reset the liner clock as well so liners do not play too close to breaks
      sails.models.status.errorCheck.prevLiner = moment();

      // enforce station ID for top of the hour breaks
      if (key === 0 || key === "0") {
        sails.models.status.errorCheck.prevID = breakTime;
        await sails.helpers.error.count("stationID");
      }

      // Remove liners in the queue. Do not do the playlist re-queue method as there may be a big prerecord or playlist in the queue.
      try {
        await sails.helpers.songs.remove(
          false,
          sails.config.custom.subcats.liners,
          true,
          true
        );
      } catch (srerror) {
        sails.log.error(srerror);
      }

      // Queue a return if in a prerecord
      if (sails.models.meta.memory.state.startsWith("prerecord_")) {
        if (
          typeof sails.config.custom.showcats[
            sails.models.meta.memory.show.split(" - ")[1]
          ] !== "undefined"
        ) {
          await sails.helpers.songs.queue(
            [
              sails.config.custom.showcats[
                sails.models.meta.memory.show.split(" - ")[1]
              ]["Show Returns"],
            ],
            "Top",
            1
          );
        } else {
          await sails.helpers.songs.queue(
            [sails.config.custom.showcats["Default"]["Show Returns"]],
            "Top",
            1
          );
        }
      }

      // Execute the break array
      try {
        await sails.helpers.break.executeArray(
          sails.config.custom.breaks[key],
          `Clockwheel ${key}`
        );
      } catch (baerror) {
        sails.log.error(baerror);
      }

      // If not doing a break, check to see if it's time to do a liner
    } else {
      // Don't do a liner if it was too soon.
      if (
        sails.models.status.errorCheck.prevLiner === null ||
        moment().diff(
          moment(sails.models.status.errorCheck.prevLiner),
          "minutes"
        ) > sails.config.custom.linerTime
      ) {
        // Only do liners when in automation and triggered by bootstrap
        if (
          sails.models.meta.memory.state.startsWith("automation_") &&
          inputs.triggeredByBootstrap
        ) {
          // If there is at least 1 track in the queue, and both the current track and the next track are not noMeta tracks, queue a liner
          if (
            sails.models.meta.automation.length > 1 &&
            parseInt(sails.models.meta.automation[0].ID) !== 0 &&
            sails.config.custom.subcats.noMeta.indexOf(
              parseInt(sails.models.meta.automation[0].IDSubcat)
            ) === -1 &&
            sails.config.custom.subcats.noMeta.indexOf(
              parseInt(sails.models.meta.automation[1].IDSubcat)
            ) === -1
          ) {
            sails.models.status.errorCheck.prevLiner = moment();
            await sails.helpers.songs.queue(
              sails.config.custom.subcats.liners,
              "Top",
              1
            );
            await sails.helpers.break.addUnderwritings(true);
          }
        }
      }
    }

    return;
  },
};
