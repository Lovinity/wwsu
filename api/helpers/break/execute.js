module.exports = {
  friendlyName: "break.execute",

  description: "Execute a configured break object.",

  inputs: {
    task: {
      type: "string",
      required: true,
      description: "Name of the break task to execute.",
    },
    event: {
      type: "string",
      defaultsTo: ``,
      description: "For log tasks, the event to log.",
    },
    category: {
      type: "string",
      description:
        "For queue tasks, the configured config.custom.categories to queue tracks from.",
    },
    quantity: {
      type: "number",
      defaultsTo: 1,
      description:
        "For tasks involving queuing of tracks or requests, number of tracks to queue.",
    },
    rules: {
      type: "string",
      isIn: ["noRules", "lenientRules", "strictRules"],
      defaultsTo: "noRules",
      description:
        "For track queuing, noRules = do not consider rotation rules, lenientRules = consider rotation rules until/unless there are no more tracks that can be queued, and then queue randomly, strictRules = consider rotation rules and stop queuing when no more tracks can be queued.",
    },
    doWhen: {
      type: "json",
      defaultsTo: [],
      custom: (value) => {
        if (value.constructor !== Array) return false; // Must be an array

        let invalid = value.find(
          (item) =>
            typeof item !== "string" ||
            [
              "show",
              "remote",
              "sports",
              "prerecord",
              "genre",
              "playlist",
              "default",
            ].indexOf(item) === -1
        );
        return !invalid;
      },
      description:
        "Only execute this task during the specified types of broadcasts (empty array = all of them, the default). Accepted values: show, remote, sports, prerecord, genre, playlist, default (Default automation rotation).",
    },
  },

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs, exits) {
    sails.log.debug("Helper requests.get called.");

    let status = `FAILED; Invalid task ${inputs.task}`;

    // Skip if doWhen is not satisfied
    if (
      ((sails.models.meta.memory.state.startsWith("live_") &&
        inputs.doWhen.indexOf("show") === -1) ||
        (sails.models.meta.memory.state.startsWith("remote_") &&
          inputs.doWhen.indexOf("remote") === -1) ||
        ((sails.models.meta.memory.state.startsWith("sports_") ||
          sails.models.meta.memory.state.startsWith("sportsremote_")) &&
          inputs.doWhen.indexOf("sports") === -1) ||
        (sails.models.meta.memory.state.startsWith("prerecord_") &&
          inputs.doWhen.indexOf("prerecord") === -1) ||
        (sails.models.meta.memory.state === "automation_genre" &&
          inputs.doWhen.indexOf("genre") === -1) ||
        (sails.models.meta.memory.state === "automation_playlist" &&
          inputs.doWhen.indexOf("playlist") === -1) ||
        ((sails.models.meta.memory.state === "automation_on" ||
          sails.models.meta.memory.state === "automation_break") &&
          inputs.doWhen.indexOf("default") === -1)) &&
      inputs.doWhen.length > 0
    )
      return exits.success(
        `SKIPPED; doWhen criteria not satisfied (${inputs.doWhen.join(", ")})`
      );

    try {
      switch (inputs.task) {
        // Log an entry
        case "log":
          try {
            await sails.models.logs
              .create({
                attendanceID: sails.models.meta.memory.attendanceID,
                logtype: "break",
                loglevel: "info",
                logsubtype: "automation",
                logIcon: `fas fa-coffee`,
                title: inputs.event,
                event: ``,
              })
              .fetch();
            return exits.success("SUCCESS");
          } catch (error) {
            sails.log.error(error);
            return exits.success(`FAILED; ${error.message}`);
          }
        // Add requested tracks
        case "queueRequests":
          try {
            let queuedSomething = await sails.helpers.requests.queue(
              inputs.quantity,
              true,
              true
            );
            if (queuedSomething) {
              return exits.success("SUCCESS; At least one request was queued.");
            } else if (await sails.models.requests.count({ played: 0 })) {
              return exits.success(
                "FAILED; one or more requests were pending, but none were queued (maybe rotation rules prevented it?)"
              );
            } else {
              return exits.success("SKIPPED; no requests to queue.");
            }
          } catch (error) {
            sails.log.error(error);
            return exits.success(`FAILED; ${error.message}`);
          }
        // Queue tracks from a configured categories.category
        case "queue":
          try {
            let queued = await sails.helpers.songs.queue(
              sails.config.custom.subcats[inputs.category],
              "Top",
              inputs.quantity,
              inputs.rules,
              null
            );
            if (queued.success) {
              return exits.success("SUCCESS");
            } else {
              return exits.success(
                `FAILED; ${queued.message || `Unknown error`}`
              );
            }
          } catch (error) {
            sails.log.error(error);
            return exits.success(`FAILED; ${error.message}`);
          }
        // Re-queue any underwritings etc that were removed due to duplicate track checking
        case "queueDuplicates":
          try {
            let tracks = await sails.helpers.songs.queuePending();
            return exits.success(
              `DONE; Attempted to queue ${tracks} pending tracks.`
            );
          } catch (error) {
            sails.log.error(error);
            return exits.success(`FAILED; ${error.message}`);
          }
        // Queue underwritings scheduled to air
        case "queueUnderwritings":
          try {
            let queued = await sails.helpers.break.addUnderwritings(
              false,
              inputs.quantity
            );
            if (queued.success) {
              return exits.success(
                `SUCCESS; Queued ${queued.queued} underwritings.`
              );
            } else {
              return exits.success(`FAILED`);
            }
          } catch (error) {
            sails.log.error(error);
            return exits.success(`FAILED; ${error.message}`);
          }
        case "queueAlerts":
          try {
            let alertsActive = await sails.models.eas.count();
            if (alertsActive > 0) {
              let queued1 = await sails.helpers.songs.queue(
                sails.config.custom.subcats["easAlertUpdateStream"],
                "Top",
                1,
                "noRules",
                null
              );
              if (queued1.success) {
                return exits.success(`SUCCESS; ${alertsActive} active alerts.`);
              } else {
                return exits.success(
                  `FAILED; did not queue easAlertUpdateStream track.`
                );
              }
            } else {
              return exits.success(`SKIPPED; no active alerts;`);
            }
          } catch (error) {
            sails.log.error(error);
            return exits.success(`FAILED; ${error.message}`);
          }
      }

      return exits.success(`FAILED; invalid task.`);
    } catch (e) {
      return exits.error(e);
    }
  },
};
