module.exports = {
  friendlyName: "helpers.canStartRemoteBroadcast",

  description:
    "Check if a host is allowed to start a remote broadcast. Does not check schedule! Returns false if not.",

  inputs: {
    host: {
      type: "number",
      required: true,
      description: "The ID of the host to check.",
    },
    broadcastType: {
      type: "string",
      required: true,
      isIn: ["sports", "remote"],
      description: "The type of remote broadcast we are attempting to start.",
    },
  },

  exits: {},

  fn: async function (inputs) {

    // sails.js modifies query strings; dumb decision TBH. We need to clone inputs to avoid bugs.
    let criteria = _.cloneDeep({
      host: inputs.host,
      broadcastType: inputs.broadcastType
    });

    let host = await sails.models.hosts.findOne({
      ID: criteria.host,
      authorized: true,
    });
    if (!host) return false;

    // If no belongsTo is set, it is assumed this host is a WWSU computer. Allow remote broadcasting.
    if (!host.belongsTo) return true;

    if (host.belongsTo) {
      // Locate the member the host belongs to. If not found, this host cannot broadcast remotely.
      let dj = await sails.models.djs.findOne({
        ID: host.belongsTo,
        active: true,
      });
      if (!dj) return false;

      // Member has permission to start remote broadcasts from their own computer. Allow it.
      if (dj.permissions.indexOf(criteria.broadcastType) !== -1) return true;
    }

    // By this point, remote broadcasting is not allowed
    return false;
  },
};
