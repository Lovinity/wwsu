const os = require("os");

module.exports = {
  friendlyName: "helpers.bootstrap.cron.checkServerHealth",

  description:
    "Check for server CPU and memory use and update status accordingly. Also determine if http requests should be throttled because of poor server performance.",

  schedule: "*/15 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Server health
    let load = os.loadavg();
    let mem = os.freemem();

    if (
      load[0] >= sails.config.custom.status.server.load1.critical ||
      load[1] >= sails.config.custom.status.server.load5.critical ||
      load[2] >= sails.config.custom.status.server.load15.critical ||
      mem <= sails.config.custom.status.server.memory.critical
    ) {
      // Critical status? Reject requests.
      sails.models.status.errorCheck.throttleAPI = 2;

      await sails.helpers.status.change.with({
        name: `server`,
        label: `Server`,
        status: 1,
        data: `Server resource use is dangerously high!!! <strong>Web and API requests are being rejected at this time.</strong> CPU: 1-min ${load[0]}, 5-min: ${load[1]}, 15-min: ${load[2]}. Free memory: ${mem}`,
      });
    } else if (
      load[0] >= sails.config.custom.status.server.load1.error ||
      load[1] >= sails.config.custom.status.server.load5.error ||
      load[2] >= sails.config.custom.status.server.load15.error ||
      mem <= sails.config.custom.status.server.memory.error
    ) {
      // Very high use? Throttle requests.
      sails.models.status.errorCheck.throttleAPI = 1;

      await sails.helpers.status.change.with({
        name: `server`,
        label: `Server`,
        status: 2,
        data: `Server resource use is very high! <strong>Web and API requests are being throttled at this time.</strong> CPU: 1-min ${load[0]}, 5-min: ${load[1]}, 15-min: ${load[2]}. Free memory: ${mem}`,
      });
    } else if (
      load[0] >= sails.config.custom.status.server.load1.warn ||
      load[1] >= sails.config.custom.status.server.load5.warn ||
      load[2] >= sails.config.custom.status.server.load15.warn ||
      mem <= sails.config.custom.status.server.memory.warn
    ) {
      // Do not throttle / reject requests yet for a minor resource use issue.
      sails.models.status.errorCheck.throttleAPI = 0;

      await sails.helpers.status.change.with({
        name: `server`,
        label: `Server`,
        status: 3,
        data: `Server resource use is mildly high. CPU: 1-min ${load[0]}, 5-min: ${load[1]}, 15-min: ${load[2]}. Free memory: ${mem}`,
      });
    } else {
      sails.models.status.errorCheck.throttleAPI = 0;
      await sails.helpers.status.change.with({
        name: `server`,
        label: `Server`,
        status: 5,
        data: `Server resource use is good. CPU: 1-min ${load[0]}, 5-min: ${load[1]}, 15-min: ${load[2]}. Free memory: ${mem}`,
      });
    }
  },
};
