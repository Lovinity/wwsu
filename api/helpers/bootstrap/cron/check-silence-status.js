module.exports = {
  friendlyName: "helpers.bootstrap.cron.checkSilenceStatus",

  description:
    "Update the silence status to critical when DJ Controls has not reported info about silence detection.",

  schedule: "13 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if in maintenance mode.
    if (sails.config.custom.lofi) return "SKIPPED; maintenance mode active.";

    let silence = await sails.models.status.findOne({ name: "silence" });
    let responsible = await sails.models.hosts.count({
      silenceDetection: true,
    });
    if (
      moment(silence.updatedAt).add(3, "minutes").isBefore(moment()) &&
      responsible > 0
    ) {
      await sails.helpers.status.change.with({
        name: "silence",
        label: "Silence",
        data: `There has been no information received about the silence detection system for over 3 minutes. Please ensure the host responsible for silence detection is active, the silence detection process is running on the responsible host (green mute icon on the Audio tab in DJ Controls), and an input device is selected for silence monitoring.`,
        status: 2,
      });
    } else if (responsible < 1) {
      await sails.helpers.status.change.with({
        name: `silence`,
        status: 2,
        label: `Silence`,
        data: `There are no hosts currently set for silence detection (you should set one to monitor on-air audio). To set a responsible host, go in an administration DJ Controls and click Hosts.`,
      });
    }
  },
};
