module.exports = {
  friendlyName: "helpers.bootstrap.cron.checkDelayStatus",

  description:
    "Update the delay status to critical if it has not been updated in 1 minute by DJ Controls (assumed it is offline).",

  schedule: "13 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if in maintenance mode.
    if (sails.config.custom.lofi) return "SKIPPED; maintenance mode active.";

    // Delay system; send to critical status if no status data for over 1 minute
    let delay = await sails.models.status.findOne({ name: "delay-system" });
    let responsible = await sails.models.hosts.count({ delaySystem: true });
    if (
      moment(delay.updatedAt).add(1, "minutes").isBefore(moment()) &&
      responsible > 0
    ) {
      await sails.helpers.status.change.with({
        name: "delay-system",
        label: "Delay System",
        data: `There has been no information received about the delay system for over 1 minute. Please ensure the delay system is online, the serial port is properly connected to the responsible computer, and DJ Controls is running on the responsible computer. Also be aware that DJ Controls is programmed for a specific delay system; if you changed delay systems, DJ Controls codebase will need to be updated.<br /><strong>Remote broadcasts will not be allowed</strong> until this issue is resolved because it is currently impossible to remotely dump.`,
        status: 1,
      });
      await sails.helpers.meta.change.with({ delaySystem: null });
    } else if (responsible < 1) {
      await sails.helpers.status.change.with({
        name: "delay-system",
        label: "Delay System",
        data: `There are no hosts currently set for delay system monitoring (this is critical status because radio delay is required by the FCC). To set a responsible host, go in an administration DJ Controls and click Hosts.<br /><strong>Remote broadcasts will not be allowed</strong> until this issue is resolved because it is currently impossible to remotely dump.`,
        status: 1,
      });
      await sails.helpers.meta.change.with({ delaySystem: null });
    }
  },
};
