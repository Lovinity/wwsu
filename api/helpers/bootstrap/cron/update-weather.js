module.exports = {


  friendlyName: 'helpers.bootstrap.cron.updateWeather',


  description: 'Fetch and process weather data',

  schedule: "11 */5 * * * *",

  inputs: {

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs) {
    // Do not execute if in maintenance mode.
    if (sails.config.custom.lofi) return "SKIPPED; maintenance mode active.";

    // Update weather
    await sails.helpers.climacell.update();
  }


};

