module.exports = {
  friendlyName: "helpers.bootstrap.cron.checkCriticalLogs",

  description:
    "Check the number of critical logs generated. If 10 unacknowledged danger logs were generated in the last hour, activate maintenance mode.",

  schedule: "16 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    let criticalLogs = await sails.models.logs.count({
      loglevel: "danger",
      acknowledged: false,
      createdAt: { ">=": moment().subtract(1, "hours").toISOString(true) },
    });

    if (criticalLogs >= 10 && !sails.config.custom.lofi) {
      sails.config.custom.lofi = true;
      await sails.models.logs
        .create({
          attendanceID: sails.models.meta.memory.attendanceID,
          logtype: "system-lockout",
          loglevel: "danger",
          logsubtype: "",
          logIcon: `fas fa-exclamation-triangle`,
          title: `The system was sent into maintenance mode due to excessive problems.`,
          event: `There have been at least 10 critical issues in the last hour. The system was put into maintenance mode. Please resolve the issues in DJ Controls -> To-Do (in the Administration menu), mark them off, and then deactivate maintenance mode.`,
        })
        .fetch()
        .tolerate((err) => {
          sails.log.error(err);
        });

        await sails.helpers.emails.queueEmergencies(
          `[URGENT] WWSU system locked out in maintenance mode`,
          `Directors,<br /><br />
Several critical issues occurred in the WWSU system in the last hour. As a precaution, <strong>WWSU was sent into maintenance mode.</strong>. This means error / failsafe checks are no longer running, and you could be off the air!<br /><br />
Please review the logs in DJ Controls -> To-Do (Administration menu) immediately. Fix the issues. Mark the logs off as complete. And then de-activate maintenance mode.`,
          true
        );
    }
  },
};
