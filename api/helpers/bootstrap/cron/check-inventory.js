module.exports = {
  friendlyName: "helpers.bootstrap.cron.checkInventory",

  description: "Check for overdue checkouts and missing items.",

  schedule: "15 */5 * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if shutting down or in maintenance mode.
    if (sails.config.custom.lofi) return "SKIPPED; maintenance mode active.";

    let status = 5;
    let issues = [];

    // Check for items not checked in yet that are overdue
    let records = await sails.models.checkout
      .find({
        checkInDate: null,
        checkInDue: { "!=": null },
      })
      .populate("item");
    records
      .filter((record) => moment(record.checkInDue).isBefore(moment()))
      .map((record) => {
        if (status > 4) status = 4;
        issues.push(
          `${record.checkOutQuantity} of ${record.item.name} (${
            record.item.location
          } / ${
            record.item.subLocation
          }) are still checked out. They were supposed to be returned by ${moment(
            record.checkInDue
          ).format("LLLL")}.`
        );
      });

    // Check for missing items
    let items = await sails.models.items.find();
    records = await sails.models.checkout
      .find({
        checkOutDate: { "!=": null },
        checkInDate: { "!=": null },
      })
      .populate("item");
    items.map((item) => {
      let quantity = item.quantity;
      let record = records.find((record) => record.item.ID === item.ID);
      if (record) quantity -= record.checkOutQuantity - record.checkInQuantity;
      if (quantity < item.quantity) {
        if (status > 4) status = 4;
        issues.push(
          `${item.name} (${item.location} / ${item.subLocation}) is missing items! Expected ${item.quantity} quantity, but only ${quantity} quantity was checked in.`
        );
      }
    });

    // Update status
    await sails.helpers.status.change.with({
      name: `inventory`,
      label: `Inventory`,
      status: status,
      data:
        issues.length === 0
          ? `No issues.`
          : `<ul>${issues.map((issue) => `<li>${issue}</li>`).join("")}</ul>`,
    });
  },
};
