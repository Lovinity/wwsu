module.exports = {
  friendlyName: "helpers.bootstrap.cron.checkRadioDjs",

  description:
    "Check the REST servers on each configured RadioDJ; also stop playing RadioDJs that are playing something when they should not be.",

  schedule: "4,34 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if in maintenance mode.
    if (sails.config.custom.lofi) return "SKIPPED; maintenance mode active.";

    // eslint-disable-next-line no-async-promise-executor
    await sails.helpers.asyncForEach(
      sails.config.custom.radiodjs,
      (radiodj) => {
        // eslint-disable-next-line promise/param-names
        return new Promise((resolve2) => {
          sails.models.status
            .findOne({ name: `radiodj-${radiodj.name}` })
            .then(async (status) => {
              try {
                var resp = await needle(
                  "get",
                  `${radiodj.rest}/p?auth=${sails.config.custom.rest.auth}`,
                  {},
                  { headers: { "Content-Type": "application/json" } }
                );
                if (
                  resp &&
                  typeof resp.body !== "undefined" &&
                  typeof resp.body.children !== "undefined"
                ) {
                  await sails.helpers.status.change.with({
                    name: `radiodj-${radiodj.name}`,
                    label: `RadioDJ ${radiodj.label}`,
                    data: "RadioDJ is online.",
                    status: 5,
                  });
                  // We were waiting for a good RadioDJ to switch to. Switch to it immediately.
                  if (sails.models.status.errorCheck.waitForGoodRadioDJ) {
                    sails.models.status.errorCheck.waitForGoodRadioDJ = false;

                    // Mark all other RadioDJs as non-critical since we now have a working RadioDJ
                    var maps = sails.config.custom.radiodjs
                      .filter(
                        (instance) =>
                          instance.rest === sails.models.meta.memory.radiodj &&
                          instance.name !== radiodj.name
                      )
                      .map(async (instance) => {
                        await sails.helpers.status.change.with({
                          name: `radiodj-${instance.name}`,
                          label: `RadioDJ ${instance.label}`,
                          status: instance.level,
                          data: `RadioDJ is not operational. Please ensure this RadioDJ is running and the REST server is online, configured properly, and accessible. When opening RadioDJ, you may have to start playing a track before REST begins working.`,
                        });
                        return true;
                      });
                    await Promise.all(maps);

                    // Switch to the new RadioDJ, and re-queue what we have in memory for the current queue.
                    var queue = sails.models.meta.automation;
                    await sails.helpers.meta.change.with({
                      radiodj: radiodj.rest,
                    });
                    await sails.helpers.rest.cmd("ClearPlaylist", 1);
                    await sails.helpers.error.post(queue);
                  }

                  // If this RadioDJ is inactive, check to see if it is playing anything
                  if (
                    sails.models.meta.memory.radiodj !== radiodj.rest &&
                    sails.models.meta.memory.altRadioDJ !== radiodj.rest
                  ) {
                    let automation = await sails.helpers.rest.getQueue(
                      radiodj.rest
                    );
                    // If this if condition passes, the RadioDJ is playing something.
                    if (
                      typeof automation[0] !== "undefined" &&
                      parseInt(automation[0].ID) !== 0
                    ) {
                      // If the active radioDJ is playing something too, we should stop the inactive RadioDJ or we will have more than one thing going out on the air.
                      if (sails.models.meta.memory.queueFinish !== null) {
                        await sails.helpers.rest.cmd(
                          "StopPlayer",
                          null,
                          10000,
                          radiodj.rest
                        );
                        // If the active RadioDJ is NOT playing anything, we should switch the active RadioDJ to the one playing something if not in a show.
                      } else if (
                        sails.models.meta.memory.changingState === null &&
                        [
                          "live_on",
                          "remote_on",
                          "sports_on",
                          "sportsremote_on",
                        ].indexOf(sails.models.meta.memory.state) === -1
                      ) {
                        await sails.helpers.meta.change.with({
                          changingState: `Switching radioDJ instances`,
                        });
                        await sails.helpers.rest.cmd("EnableAssisted", 1, 0);
                        await sails.helpers.rest.cmd("EnableAutoDJ", 1, 0);
                        await sails.helpers.rest.cmd("StopPlayer", 0, 0);
                        await sails.helpers.rest.changeRadioDj(radiodj.rest);
                        await sails.helpers.error.post();
                        await sails.helpers.meta.change.with({
                          changingState: null,
                        });
                      }
                    }
                  }
                } else {
                  if (status && status.status !== 1) {
                    await sails.helpers.status.change.with({
                      name: `radiodj-${radiodj.name}`,
                      label: `RadioDJ ${radiodj.label}`,
                      data: "RadioDJ REST did not return queue data. Please ensure the REST server is online, configured properly, and accessible. When opening RadioDJ, you may have to start playing a track before REST begins working.",
                      status: radiodj.level,
                    });
                  }
                }
                return resolve2(false);
              } catch (unusedE) {
                if (status && status.status !== 1) {
                  await sails.helpers.status.change.with({
                    name: `radiodj-${radiodj.name}`,
                    label: `RadioDJ ${radiodj.label}`,
                    data: "RadioDJ REST returned an error or is not responding. Please ensure RadioDJ is open and functional, and the REST server is online, configured properly, and accessible. When opening RadioDJ, you may have to start playing a track before REST begins working.",
                    status: radiodj.level,
                  });
                }
                return resolve2(false);
              }
            });
        });
      }
    );
  },
};
