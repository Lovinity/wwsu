module.exports = {
  friendlyName: "helpers.bootstrap.cron.pruneOldRecipients",

  description: "",

  schedule: "10 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    let destroyIt = [];
    let searchto = moment().subtract(4, "hours"); // TODO: Add to sails.config.custom

    let records = await sails.models.recipients.find({
      host: { "!=": ["website"] },
      status: 0,
    });


    records.forEach((record) => {
      if (moment(record.time).isBefore(moment(searchto))) {
        destroyIt.push(record.ID);
      }
    });
    if (destroyIt.length > 0) {
      await sails.models.recipients.destroy({ ID: destroyIt }).fetch();
    }
  },
};
