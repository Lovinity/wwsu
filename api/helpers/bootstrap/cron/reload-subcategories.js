module.exports = {
  friendlyName: "helpers.bootstrap.cron.reloadSubcategories",

  description:
    "Refresh the mapping of configured categories to RadioDJ subcategory IDs.",

  schedule: "8 */5 * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if in maintenance mode.
    if (sails.config.custom.lofi) return "SKIPPED; maintenance mode active.";

    // Load subcategories into config
    await sails.helpers.songs.reloadSubcategories();
  },
};
