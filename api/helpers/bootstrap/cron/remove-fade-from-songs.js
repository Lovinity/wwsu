const queryString = require("query-string");

module.exports = {
  friendlyName: "helpers.bootstrap.cron.removeFadeFromSongs",

  description: "Remove crossfading from songs in the noFade configuration.",

  schedule: "12 0 * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if in maintenance mode.
    if (sails.config.custom.lofi) return "SKIPPED; maintenance mode active.";

    // Get all noFade tracks
    let records = await sails.models.songs.find({
      id_subcat: sails.config.custom.subcats.noFade,
    });

    if (records && records.length > 0) {
      records.map((record) => {
        var cueData = queryString.parse(record.cue_times);

        // If fade in and fade out are both 0 (treat when fade in or fade out is not specified as being 0), skip this track; nothing to do.
        if (
          (!cueData.fin || cueData.fin === 0) &&
          (!cueData.fou || cueData.fou === 0)
        ) {
          return null;
        }

        // Get rid of any fading, and reset the xta cue point
        cueData.fin = 0;
        cueData.fou = 0;
        cueData.xta = cueData.end || record.duration;

        cueData = `&${queryString.stringify(cueData)}`;

        // Update the track with the new cue points
        (async (record2, cueData2) => {
          // LINT: RadioDJ table
          // eslint-disable-next-line camelcase
          await sails.models.songs.update(
            { ID: record2.ID },
            { cue_times: cueData2 }
          );
        })(record, cueData);
      });
    }

    // Now, update tracks with a duration less than 0 and change their enabled status to -1; these are bad tracks that will crash RadioDJ.
    await sails.models.songs.update(
      { duration: { "<": 0 }, enabled: { "!=": -1 } },
      { enabled: -1 }
    );
  },
};
