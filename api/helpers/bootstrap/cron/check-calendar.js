module.exports = {
  friendlyName: "helpers.bootstrap.cron.checkCalendar",

  description:
    "Check for, validate, and execute scheduled calendar events when applicable",

  schedule: "2 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if in maintenance mode.
    if (sails.config.custom.lofi) return "SKIPPED; maintenance mode active.";

    // Check integrity on every 5th minute; this is a CPU intensive process and therefore why we only check these every 5 minutes.
    if (moment().minute() % 5 === 0) {
      await sails.helpers.calendar.check(false, true);
    } else {
      await sails.helpers.calendar.check();
    }
  },
};
