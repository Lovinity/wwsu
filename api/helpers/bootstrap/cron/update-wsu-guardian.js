module.exports = {
  friendlyName: "helpers.bootstrap.cron.updateWsuGuardian",

  description: "Update the latest posts on The Guardian Media Group.",

  schedule: "11 */5 * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if in maintenance mode.
    if (sails.config.custom.lofi) return "SKIPPED; maintenance mode active.";

    // Update weather
    await sails.helpers.rss.guardian.update();
  },
};
