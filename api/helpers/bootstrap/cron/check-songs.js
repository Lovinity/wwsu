module.exports = {
  friendlyName: "helpers.bootstrap.cron.checkSongs",

  description:
    "Count the number of corrupt / bad tracks in RadioDJ and calculate the number of music tracks whose duration is too long.",

  schedule: "9 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if in maintenance mode.
    if (sails.config.custom.lofi) return "SKIPPED; maintenance mode active.";

    let problems = [];
    let status = 5;

    try {
      // Count the number of -1 enabled tracks
      let found = await sails.models.songs
        .count({ enabled: -1 })
        .tolerate(() => {});
      if (
        found &&
        found >= sails.config.custom.status.musicLibrary.verify.critical
      ) {
        status = 1;
        problems.push(
          `Music library has ${found} bad tracks. This is critically high and should be fixed immediately! Tracks are marked bad when either corrupt or cannot be accessed by RadioDJ. Please ensure all tracks used by RadioDJ are saved on a [network] drive that can be accessed by all RadioDJs. Run the "verify tracks" utility in RadioDJ to see which tracks are bad.`
        );
      } else if (
        found &&
        found >= sails.config.custom.status.musicLibrary.verify.error
      ) {
        status = status > 2 ? 2 : status;
        problems.push(
          `Music library has ${found} bad tracks. This is quite high. Tracks are marked bad when either corrupt or cannot be accessed by RadioDJ. Please ensure all tracks used by RadioDJ are saved on a [network] drive that can be accessed by all RadioDJs. Run the "verify tracks" utility in RadioDJ to see which tracks are bad.`
        );
      } else if (
        found &&
        found >= sails.config.custom.status.musicLibrary.verify.warn
      ) {
        status = status > 3 ? 3 : status;
        problems.push(
          `Music library has ${found} bad tracks. Tracks are marked bad when either corrupt or cannot be accessed by RadioDJ. Please ensure all tracks used by RadioDJ are saved on a [network] drive that can be accessed by all RadioDJs. Run the "verify tracks" utility in RadioDJ to see which tracks are bad.`
        );
      } else if (found && found > 0) {
        status = status > 4 ? 4 : status;
        problems.push(
          `Music library has ${found} bad tracks. Tracks are marked bad when either corrupt or cannot be accessed by RadioDJ. Please ensure all tracks used by RadioDJ are saved on a [network] drive that can be accessed by all RadioDJs. Run the "verify tracks" utility in RadioDJ to see which tracks are bad.`
        );
      }
    } catch (e) {
      sails.log.error(e);
      status = 1;
      problems.push(
        `There was an error checking the RadioDJ music library for bad tracks. This usually indicates a database problem.`
      );
    }

    // Now check for long tracks
    try {
      let found = await sails.models.songs
        .count({
          enabled: 1,
          id_subcat: sails.config.custom.subcats.music,
          duration: { ">=": 60 * 10 }, // TODO: use break configuration to determine when a track is too long
        })
        .tolerate(() => {});

      if (
        found &&
        found >= sails.config.custom.status.musicLibrary.verify.critical
      ) {
        status = 1;
        problems.push(
          `Music library has ${found} long tracks (that are enabled). This is critically high and should be fixed immediately! Tracks which are longer than 10 minutes should be shortened, disabled, or removed. Excessively long tracks can prevent the FCC required top-of-hour ID from airing on time.`
        );
      } else if (
        found &&
        found >= sails.config.custom.status.musicLibrary.verify.error
      ) {
        status = status > 2 ? 2 : status;
        problems.push(
          `Music library has ${found} long tracks (that are enabled). This is high. Tracks which are longer than 10 minutes should be shortened, disabled, or removed. Excessively long tracks can prevent the FCC required top-of-hour ID from airing on time.`
        );
      } else if (
        found &&
        found >= sails.config.custom.status.musicLibrary.verify.warn
      ) {
        status = status > 3 ? 3 : status;
        problems.push(
          `Music library has ${found} long tracks (that are enabled). Tracks which are longer than 10 minutes should be shortened, disabled, or removed. Excessively long tracks can prevent the FCC required top-of-hour ID from airing on time.`
        );
      } else if (found && found > 0) {
        status = status > 4 ? 4 : status;
        problems.push(
          `Music library has ${found} long tracks (that are enabled). Tracks which are longer than 10 minutes should be shortened, disabled, or removed. Excessively long tracks can prevent the FCC required top-of-hour ID from airing on time.`
        );
      }
    } catch (e) {
      sails.log.error(e);
      status = 1;
      problems.push(
        `There was an error checking the RadioDJ music library for long tracks. This usually indicates a database problem.`
      );
    }

    await sails.helpers.status.change.with({
      name: "music-library",
      status: status,
      data: `<ul>${problems
        .map((problem) => `<li>${problem}</li>`)
        .join("")}</ul>`,
    });
  },
};
