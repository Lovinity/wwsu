module.exports = {
  friendlyName: "helpers.bootstrap.cron.resetSongPriorities",

  description: "Reset song priorities according to the set rating in RadioDJ if applicable.",

  schedule: "51 59 23 * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if in maintenance mode.
    if (sails.config.custom.lofi) return "SKIPPED; maintenance mode active.";

    // First, get the value of default priority in RadioDJ
    let defaultPriority = await sails.models.settings
      .find({ source: "settings_general", setting: "DefaultTrackPriority" })
      .limit(1);
    if (
      typeof defaultPriority[0] === "undefined" ||
      defaultPriority === null ||
      defaultPriority[0] === null
    ) {
      throw new Error(
        "Could not find DefaultTrackPriority setting in the RadioDJ database"
      );
    }

    // Now, process every song
    let songs = await sails.models.songs.find();
    songs.map((song) => {
      try {
        let minPriority =
          song.rating === 0 ? 0 : defaultPriority[0] * (song.rating / 9);
        minPriority = Math.round(minPriority * 10) / 10;
        if (song.weight < minPriority) {
          (async (song2, minPriority2) => {
            await sails.models.songs.update(
              { ID: song2.ID },
              { weight: minPriority2 }
            );
          })(song, minPriority);
        }
      } catch (e) {
        sails.log.error(e);
      }
    });
  },
};
