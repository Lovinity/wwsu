module.exports = {
  friendlyName: "helpers.bootstrap.cron.checkRadioVideoStream",

  description:
    "Check the status of the internet radio and video streams, and log current listeners / viewers.",

  schedule: "3,33 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if shutting down or in maintenance mode.
    if (sails.config.custom.lofi) return "SKIPPED; maintenance mode active.";

    // Initialize with current stats in case a check fails (certain checks will set to 0 is we reasonably believe no one is connected)
    let listeners = sails.models.meta.memory.listeners;
    let viewers = sails.models.listeners.memory.viewers;

    // SHOUTCAST 2.6
    if (sails.config.custom.stream && sails.config.custom.stream !== "") {
      try {
        let resp = await needle(
          "get",
          sails.config.custom.stream + `/statistics?json=1`,
          {},
          { headers: { "Content-Type": "application/json" } }
        );

        if (resp && typeof resp.body !== "undefined") {
          if (typeof resp.body.streams !== "undefined") {
            let streams = resp.body.streams;

            // Check public stream
            if (
              typeof streams !== "undefined" &&
              typeof streams[0] !== "undefined" &&
              typeof streams[0].streamstatus !== "undefined" &&
              streams[0].streamstatus !== 0
            ) {
              // Mark stream as good
              await sails.helpers.status.change.with({
                name: "stream-public",
                label: "Radio Stream",
                data: "Shoutcast v2 stream is online.",
                status: 5,
              });
              listeners = streams[0].uniquelisteners;
            } else {
              await sails.helpers.status.change.with({
                name: "stream-public",
                label: "Radio Stream",
                data: `Online Shoutcast v2 radio stream is offline. Please ensure the audio encoder is connected and streaming to the ${sails.config.custom.stream} Shoutcast server.`,
                status: 2,
              });

              // If the stream is actually offline, we know no one is listening.
              listeners = 0;
            }
          } else {
            await sails.helpers.status.change.with({
              name: "stream-public",
              label: "Radio Stream",
              data: `Online Shoutcast v2 radio stream is offline. Please ensure the audio encoder is connected and streaming to the ${sails.config.custom.stream} Shoutcast server.`,
              status: 2,
            });

            // If the stream is actually offline, we know no one is listening.
            listeners = 0;
          }
        } else {
          await sails.helpers.status.change.with({
            name: "stream-public",
            label: "Radio Stream",
            data: `Error parsing data from the Shoutcast v2 server. Online radio stream might be offline or not working. Please ensure the Shoutcast server ${sails.config.custom.stream} is online and working properly.`,
            status: 2,
          });
        }
      } catch (e) {
        await sails.helpers.status.change.with({
          name: "stream-public",
          label: "Radio Stream",
          data: "Error checking Shoutcast v2 server. Please see node server logs. The online radio stream might be offline right now.",
          status: 2,
        });

        sails.log.error(e);
      }
    } else {
      await sails.helpers.status.change.with({
        name: "stream-public",
        label: "Radio Stream",
        data: "A shoutcast v2 radio stream URL is not defined in configuration. Please define to record listener analytics and check radio stream status!",
        status: 4,
      });
      listeners = 0;
    }

    // OWNCAST
    // TODO: Add sails.config.custom for URL, and add a check for if it is defined.
    try {
      let resp = await needle(
        "get",
        `https://wwsuvideo.pdstig.me/api/status`,
        {},
        { headers: { "Content-Type": "application/json" } }
      );

      if (resp && typeof resp.body !== "undefined") {
        if (typeof resp.body.viewerCount !== "undefined") {
          // Mark stream as good
          await sails.helpers.status.change.with({
            name: "stream-video",
            label: "Video Stream",
            data: "Owncast Video stream server is operational.",
            status: 5,
          });
          viewers = resp.body.online ? resp.body.viewerCount : null; // Use "null" if the stream is offline
        } else {
          await sails.helpers.status.change.with({
            name: "stream-video",
            label: "Video Stream",
            data: `Owncast video stream server is offline or not working properly. Video streams might not be possible at this time.`,
            status: 2,
          });

          // We have a response, but the video server is offline. We can reasonably believe no one is connected, including the encoder.
          viewers = null;
        }
      } else {
        await sails.helpers.status.change.with({
          name: "stream-video",
          label: "Video Stream",
          data: `Error parsing data from the Owncast server. The video stream server might be offline or not working. Video streaming might not be possible at this time.`,
          status: 2,
        });
      }
    } catch (e) {
      await sails.helpers.status.change.with({
        name: "stream-video",
        label: "Video Stream",
        data: "Error checking Owncast server. Video streaming might not be working at this time. Please see node server logs.",
        status: 2,
      });
      sails.log.error(e);
    }

    // Log listeners or viewers if there are any changes to them or to the host DJ
    if (
      sails.models.meta.memory.dj !== sails.models.listeners.memory.dj ||
      (typeof listeners !== "undefined" &&
        listeners !== sails.models.listeners.memory.listeners) ||
      (typeof viewers !== "undefined" &&
        viewers !== sails.models.listeners.memory.viewers)
    ) {
      await sails.models.listeners
        .create({
          dj: sails.models.meta.memory.dj,
          listeners:
            typeof listeners !== "undefined"
              ? listeners
              : sails.models.listeners.memory.listeners,
          viewers:
            typeof viewers !== "undefined"
              ? viewers
              : sails.models.listeners.memory.viewers,
        })
        .tolerate(() => {});
      await sails.helpers.meta.change.with({
        listeners:
          typeof listeners !== "undefined"
            ? listeners
            : sails.models.listeners.memory.listeners,
        viewers:
          typeof viewers !== "undefined"
            ? viewers
            : sails.models.listeners.memory.viewers,
      });
    }
    sails.models.listeners.memory = {
      dj: sails.models.meta.memory.dj,
      listeners:
        typeof listeners !== "undefined"
          ? listeners
          : sails.models.listeners.memory.listeners,
      viewers:
        typeof viewers !== "undefined"
          ? viewers
          : sails.models.listeners.memory.viewers,
    };
  },
};
