module.exports = {
  friendlyName: "helpers.bootstrap.cron.checkWebsite",

  description: "Check the status of the WWSU website",

  schedule: "5 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if in maintenance mode.
    if (sails.config.custom.lofi) return "SKIPPED; maintenance mode active.";

    // Do not execute if no website is defined
    if (!sails.config.custom.website || sails.config.custom.website === "")
      return "SKIPPED; website not defined in configuration.";

    try {
      var resp = await needle(
        "get",
        sails.config.custom.website,
        {},
        { headers: { "Content-Type": "application/json" } }
      );
      if (resp && typeof resp.body !== "undefined") {
        await sails.helpers.status.change.with({
          name: `website`,
          label: `Website`,
          data: "Website is online.",
          status: 5,
        });
      } else {
        await sails.helpers.status.change.with({
          name: `website`,
          label: `Website`,
          data: `Website ${sails.config.custom.website} did not return body data. Please ensure the website is online.`,
          status: 2,
        });
      }
    } catch (e) {
      await sails.helpers.status.change.with({
        name: `website`,
        label: `Website`,
        data: `Error checking the status of the ${sails.config.custom.website} website. The website might be offline. Please check server logs.`,
        status: 2,
      });
      sails.log.error(e);
    }
  },
};
