module.exports = {
  friendlyName: "helpers.bootstrap.cron.dailyCleanup",

  description:
    "Keep the application running smooth by pruning old records from the datastores.",

  schedule: "52 59 23 * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if in maintenance mode.
    if (sails.config.custom.lofi) return "SKIPPED; maintenance mode active.";

    let cleanedUp = ``;
    let records;
    let wasLofi = sails.config.custom.lofi;

    sails.models.status.tasks.cleanup++;
    try {
      // Activate lofi; clean-up tends to freeze Node for several seconds. We don't want accidental false positive errors tripping.
      sails.config.custom.lofi = true;

      // Delete announcements that expired over a month ago
      records = await sails.models.announcements
        .destroy({
          expires: {
            "<": moment().subtract(1, "months").toISOString(true),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Announcements deleted (expired over 1 months ago): ${records
          .map((record) => record.title)
          .join()}</li>`;

      // Delete attendance records older than 2 years
      records = await sails.models.attendance
        .destroy({
          createdAt: {
            "<": moment().subtract(2, "years").toISOString(true),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Number of attendance records deleted (over 2 years old): ${records.length}</li>`;

      // Mark blog posts inactive that expired.
      records = await sails.models.blogs
        .update(
          {
            active: true,
            expires: {
              "!=": null,
              "<": moment().toISOString(true),
            },
          },
          { active: false }
        )
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Number of blog posts marked inactive (expired): ${records.length}</li>`;

      // Delete blog posts that have been updated over 1 year ago and have no author (usually means org member was removed).
      records = await sails.models.blogs
        .destroy({
          author: null,
          updatedAt: {
            "<": moment().subtract(1, "years").toISOString(true),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Number of blog posts deleted (no active author; unedited for over a year): ${records.length}</li>`;

      // Delete on-air programming calendar events (excluding sports) that did not air for over a year
      records = await sails.models.calendar
        .destroy({
          ID: { "!=": 1 }, // Do not delete the test event
          type: ["show", "remote", "prerecord", "genre", "playlist"],
          lastAired: {
            "<": moment().subtract(1, "years").toISOString(true),
          },
          active: false,
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Calendar events deleted (did not air over a year): ${records
          .map((record) => `${record.type}: ${record.hosts} - ${record.name}`)
          .join()}</li>`;

      // Clean up old schedules
      records = await sails.helpers.calendar.cleanSchedules(
        moment().subtract(1, "weeks")
      );
      if (records > 0)
        cleanedUp += `<li>Number of Schedules/occurrences/overrides deleted (ended 1+ weeks ago): ${records}</li>`;

      // Sometimes, garbage weather records exist. Remove these so DJ Controls doesn't freeze up trying to process weather.
      records = await sails.models.climacell
        .destroy({
          updatedAt: {
            "<": moment().subtract(2, "days").toISOString(true),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Number of garbage climacell weather records deleted (2+ days without updates): ${records.length}</li>`;

      // Delete flagged records older than 2 years
      records = await sails.models.flaggedlist
        .destroy({
          createdAt: {
            "<": moment().subtract(2, "years").toISOString(true),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Number of flagged track / broadcast records deleted (over 2 years old): ${records.length}</li>`;

      // Mark DJs inactive who have not been seen for over a year

      // TODO: Fix this (lastSeen parses wrong)

      /*
      records = await sails.models.djs
        .update(
          {
            active: true,
            lastSeen: {
              "<": moment().subtract(1, "years").toISOString(true),
            },
            ID: { "!=": 1 }, // Never mark master member inactive
          },
          { active: false }
        )
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Members marked inactive (did not air any shows / authorize anything in the last year): ${records
          .map((record) => `${record.name} (${record.realName})`)
          .join()}</li>`;
          */

      // Clean up old email records from over 2 years ago
      records = await sails.models.emails
        .destroy({
          createdAt: {
            "<": moment().subtract(2, "years").toISOString(true),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Number of email records deleted (over 2 years old): ${records.length}</li>`;

      // Delete flagged records older than 2 years
      records = await sails.models.flaggedlist
        .destroy({
          createdAt: {
            "<": moment().subtract(2, "years").toISOString(true),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Number of flagged track / broadcast records deleted (over 2 years old): ${records.length}</li>`;

      // Delete RadioDJ history records older than 2 years
      records = await sails.models.history
        .destroy({
          createdAt: {
            "<": moment().subtract(2, "years").toISOString(true),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Number of RadioDJ track history records deleted (over 2 years old): ${records.length}</li>`;

      // Delete listener analytics older than 2 years
      records = await sails.models.listeners
        .destroy({
          createdAt: {
            "<": moment().subtract(2, "years").toISOString(true),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Number of listener records deleted (over 2 years old): ${records.length}</li>`;

      // Delete lockdown records older than 2 years
      records = await sails.models.lockdown
        .destroy({
          createdAt: {
            "<": moment().subtract(2, "years").toISOString(true),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Number of lockdown / reservation records deleted (over 2 years old): ${records.length}</li>`;

      // Delete logs older than 2 years
      records = await sails.models.logs
        .destroy({
          createdAt: {
            "<": moment().subtract(2, "years").toISOString(true),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Number of log records deleted (over 2 years old): ${records.length}</li>`;

      // Delete old message records older than 1 years
      records = await sails.models.messages
        .destroy({
          createdAt: {
            "<": moment().subtract(1, "years").toISOString(true),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Number of message records deleted (over 1 year old): ${records.length}</li>`;

      // Delete track request records older than 2 years
      records = await sails.models.requests
        .destroy({
          createdAt: {
            "<": moment().subtract(2, "years").toISOString(true),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Number of track request records deleted (over 2 years old): ${records.length}</li>`;

      // Delete liked songs records older than 2 years
      records = await sails.models.songsliked
        .destroy({
          createdAt: {
            "<": moment().subtract(2, "years").toISOString(true),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Number of liked tracks records deleted (over 2 years old): ${records.length}</li>`;

      // Delete timesheet records older than 2 years
      records = await sails.models.timesheet
        .destroy({
          createdAt: {
            "<": moment().subtract(2, "years").toISOString(true),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Number of timesheet records deleted (over 2 years old): ${records.length}</li>`;

      // Delete UAB timesheet records older than 2 years
      records = await sails.models.uabtimesheet
        .destroy({
          createdAt: {
            "<": moment().subtract(2, "years").toISOString(true),
          },
        })
        .fetch();
      if (records.length > 0)
        cleanedUp += `<li>Number of UAB timesheet records deleted (over 2 years old): ${records.length}</li>`;

      // Add a log with the clean-up results
      await sails.models.logs
        .create({
          attendanceID: null,
          logtype: "cleanup",
          loglevel: "info",
          logsubtype: "",
          logIcon: `fas fa-broom`,
          title: `Daily clean-up was run.`,
          event: `<ul>${cleanedUp}</ul>`,
        })
        .fetch()
        .tolerate((err) => {
          // Don't throw errors, but log them
          sails.log.error(err);
        });

      // Turn lofi back off if it was off when this process started.
      sails.config.custom.lofi = wasLofi;

      sails.models.status.tasks.cleanup--;
    } catch (e) {
      // Add a log indicating an error
      await sails.models.logs
        .create({
          attendanceID: null,
          logtype: "cleanup",
          loglevel: "warning",
          logsubtype: "",
          logIcon: `fas fa-broom`,
          title: `There was a problem running the daily cleanup!`,
          event: `${e.message}<br />See node logs for more information.`,
        })
        .fetch()
        .tolerate((err) => {
          // Don't throw errors, but log them
          sails.log.error(err);
        });

      sails.config.custom.lofi = wasLofi;
      sails.log.error(e);

      sails.models.status.tasks.cleanup--;
    }
  },
};
