module.exports = {
  friendlyName: "helpers.bootstrap.cron.fetchEasAlerts",

  description:
    "Fetch active alerts from the National Weather Service and from Campus Alerts.",

  schedule: "6 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if shutting down or in maintenance mode.
    if (sails.config.custom.lofi) return "SKIPPED; maintenance mode active.";

    // Initial housekeeping
    await sails.helpers.eas.preParse();

    let complete = 0;
    let bad = [];

    let asyncLoop = async function (array, callback) {
      for (let index = 0; index < array.length; index++) {
        // LINT: This is a loop; we do not want to return the callback.
        // eslint-disable-next-line callback-return
        await callback(array[index], index, array);
      }
    };

    // Iterate through every configured county and get their weather alerts
    if (
      sails.config.custom.EAS.NWSX &&
      sails.config.custom.EAS.NWSX.length > 0
    ) {
      await asyncLoop(sails.config.custom.EAS.NWSX, async (county) => {
        try {
          let resp = await needle(
            "get",
            `https://alerts.weather.gov/cap/wwaatmget.php?x=${
              county.code
            }&y=0&t=${moment().valueOf()}`,
            {},
            { headers: { "Content-Type": "application/json" } }
          );
          await sails.helpers.eas.parseCaps(county.name, resp.body);
          complete++;
        } catch (err) {
          bad.push(county.name);
          // Do not reject on error; just go to the next county
          sails.log.error(err);
        }
      });

      // If all counties succeeded, mark EAS-NWS as operational
      if (complete >= sails.config.custom.EAS.NWSX.length) {
        await sails.helpers.status.change.with({
          name: "EAS-NWS",
          label: "EAS NWS CAPS",
          data: `All NWS CAPS (${sails.config.custom.EAS.NWSX.map(
            (cap) => cap.name
          ).join()}) are online.`,
          status: 5,
        });
      } else {
        await sails.helpers.status.change.with({
          name: "EAS-NWS",
          label: "EAS NWS CAPS",
          data: `Could not fetch the following NWS CAPS counties: ${bad.join(
            ", "
          )}. This is usually caused by a network issue, or the NWS CAPS server is experiencing a temporary service disruption.`,
          status: 3,
        });
      }
    } else {
      await sails.helpers.status.change.with({
        name: "EAS-NWS",
        label: "EAS NWS CAPS",
        data: `There are no counties / zones defined in EAS.NWSX configuration; National Weather Service EAS checking is disabled.`,
        status: 4,
      });
    }

    // Now, query for Campus alerts
    // TODO: Add object of Campus Alert feeds to sails.config.custom config. And check if any are defined.
    try {
      let resp = await needle(
        "get",
        `https://www.wright.edu/files/alerts-dayton.json?t=${moment().valueOf()}`,
        {},
        { headers: { "Content-Type": "application/json" } }
      );
      if (!resp || !resp.body || !resp.body.success) {
        throw new Error("Wright State Alert fetch was not successful.");
      }

      // Alerts in effect; parse them.
      if (resp.body.alerts && resp.body.alerts.length > 0) {
        let maps = resp.body.alerts.map(async (alert) => {
          await sails.helpers.eas.addAlert(
            alert.updated || alert.date || alert.title || alert.description,
            "Wright State Alert",
            "Wright State University - Dayton Campus",
            alert.title || "Unknown Campus Alert",
            "Severe",
            moment(alert.updated || alert.date || undefined).toISOString(true),
            null,
            "#046A38",
            alert.description.join(" | "),
            true
          );
        });
        await Promise.all(maps);
      }

      // At this point, mark WSU EAS as good.
      await sails.helpers.status.change.with({
        name: "EAS-WSU",
        label: "EAS Wright State Alert",
        data: `Wright State Alert feed is operational.`,
        status: 5,
      });
    } catch (err) {
      await sails.helpers.status.change.with({
        name: "EAS-WSU",
        label: "EAS Wright State Alert",
        data: `Could not fetch active Campus Alerts. This is usually caused by a network issue, or the Wright State Alert feed is experiencing a temporary service disruption.`,
        status: 3,
      });
    }

    // Finish up
    await sails.helpers.eas.postParse();
  },
};
