module.exports = {
  friendlyName: "helpers.bootstrap.cron.publishBlogs",

  description: "Send notifications of blog posts when they get published.",

  schedule: "17 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Determine which blog posts are ready to be notified / "published"
    let toPublish = await sails.models.blogs.find({
      active: true,
      needsApproved: false,
      notified: false,
      or: [
        {
          starts: { "<=": moment().toISOString(true) },
          expires: { ">": moment().toISOString(true) },
        },
        {
          starts: { "<=": moment().toISOString(true) },
          expires: null,
        },
        {
          starts: null,
          expires: { ">": moment().toISOString(true) },
        },
        {
          starts: null,
          expires: null,
        },
      ],
    });

    if (toPublish.length) {
      let maps = toPublish.map(async (blog) => {
        // notify oneSignal subscribers
        await sails.helpers.onesignal.sendBlog(blog);

        // Post in Discord
        await sails.helpers.discord.sendBlog(blog);

        // Update notified status
        await sails.models.blogs.updateOne({ ID: blog.ID }, { notified: true });
      });
      await Promise.all(maps);
    }
  },
};
