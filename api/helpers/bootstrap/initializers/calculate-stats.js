module.exports = {
  friendlyName: "helpers.bootstrap.initializers.calculateStats",

  description:
    "Calculate weekly analytics (in the background) when server is booted.",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not await
    sails.helpers.attendance.calculateStats().exec(() => {});
  },
};
