module.exports = {
  friendlyName: "helpers.bootstrap.initializers.sportsCalendarEvents",

  description:
    "Refresh active calendar sports events to match what is in the configuration for sports.",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // TODO: remove this initializer when using new configuration system
    if (sails.config.custom.sports) {
      sails.log.verbose(`BOOTSTRAP: Initiating sports events in calendar`);

      // Add sports into the calendar as a non-scheduled event if they do not exist
      sails.config.custom.sports.map((sport) => {
        (async (_sport) => {
          sails.models.calendar
            .findOrCreate(
              { type: "sports", name: sport },
              {
                type: "sports",
                active: true,
                priority: sails.models.calendar.calendardb.getDefaultPriority({
                  type: "sports",
                }),
                name: sport,
                startDate: moment().toISOString(true),
              }
            )
            .exec(async (err, record, wasCreated) => {
              if (err) {
                sails.models.logs
                  .create({
                    attendanceID: sails.models.meta.memory.attendanceID,
                    logtype: "database-error",
                    loglevel: "warning",
                    logsubtype: ``,
                    logIcon: `fas fa-database`,
                    title: `Error adding sports calendar event!`,
                    event: `A database error occurred when trying to add a sports event, ${sport}, to the calendar. ${err.message}. Please see node/server logs for more info.<br />
                    <strong>TO FIX:</strong> After identifying and fixing the database issue, mark off this issue and reboot the sails.js application; it will try to create the event again upon reboot.`,
                  })
                  .fetch()
                  .exec(() => {});
                sails.log.error(err);
              }
              if (!wasCreated)
                await sails.models.calendar
                  .update(
                    { ID: record.ID },
                    {
                      active: true,
                      priority:
                        sails.models.calendar.calendardb.getDefaultPriority({
                          type: "sports",
                        }),
                      startDate: moment().toISOString(true),
                    }
                  )
                  .fetch();
            });
        })(sport);
      });

      // De-activate main sports events that do not exist in the system configured list of sports
      await sails.models.calendar
        .update(
          { type: "sports", name: { nin: sails.config.custom.sports } },
          { active: false }
        )
        .fetch();
    }
  },
};
