module.exports = {
  friendlyName: "helpers.bootstrap.initializers.discord",

  description: "Discord initializer",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Globals Discord and DiscordClient need to be defined in bootstrap.js BEFORE initializer is run!

    // Skip if we do not have a DiscordClient
    if (!DiscordClient) return "Skipped; Discord configuration was not defined.";
    
    // Initialize DiscordClient event handlers (every Discord.js event is handled in a sails.helpers.events file)
    if (sails.helpers && sails.helpers.discord && sails.helpers.discord.events) {
      for (var event in sails.helpers.discord.events) {
        if (
          Object.prototype.hasOwnProperty.call(
            sails.helpers.discord.events,
            event
          )
        ) {
          // Needs to be in a self-calling function to provide the proper value of event
          let temp = (async (event2) => {
            // ready should only ever fire once whereas other events should be allowed to fire multiple times.
            if (["ready"].indexOf(event2) !== -1) {
              DiscordClient.once(event2, async (...args) => {
                await sails.helpers.discord.events[event2](...args);
              });
            } else {
              DiscordClient.on(event2, async (...args) => {
                await sails.helpers.discord.events[event2](...args);
              });
            }
          })(event);
        }
      }
    }

    // Try logging in; try again every 5 minutes on a failure.
    const discordLogin = async () => {
      try {
        await DiscordClient.login(sails.config.custom.discord.token);
      } catch (e) {
        setTimeout(async () => discordLogin(), 1000 * 60 * 5);
        await sails.helpers.status.change.with({
          name: "discord",
          status: 3,
          data: "There was a problem connecting to Discord (will retry every 5 minutes). Things to check:<ul><li>Network connection / Discord status</li><li>Bot token</li><li>If any API rate limits / violations occurred</li><li>If Discord banned the bot / application</li></ul>",
        });
      }
    };
    discordLogin();
  },
};
