module.exports = {
  friendlyName: "Clean schedules",

  description: "Delete old schedules from the calendar",

  inputs: {
    deleteBefore: {
      type: "ref",
      custom: function (value) {
        return moment(value).isValid();
      },
      defaultsTo: moment().subtract(1, "weeks"),
      description:
        "Delete schedules whose last air date was before the provided moment time.",
    },
  },

  fn: async function (inputs) {
    let deletedSchedules = 0; // Keep track of number of schedules we deleted

    let schedules = await sails.models.schedule.find();

    if (schedules && schedules.length > 0) {
      let maps = schedules.map(async (schedule) => {
        let endTime;

        // Process regular schedule
        if (
          !schedule.scheduleType ||
          schedule.scheduleType === "" ||
          schedule.scheduleType === "unscheduled"
        ) {
          // Determine latest air date/time from endDate, startDate, oneTime schedules, and updated overrides
          endTime = moment(
            schedule.endDate || schedule.startDate || "2002-01-01T00:00:00Z"
          ).add(1, "days");
          if (schedule.oneTime && schedule.oneTime.length > 0) {
            schedule.oneTime.map((ot) => {
              if (moment(ot).isAfter(moment(endTime))) {
                endTime = moment(ot).add(1, "days");
              }
            });
          }
          let overrides = await sails.models.schedule.find({
            or: [{ scheduleID: schedule.ID }, { overriddenID: schedule.ID }],
            type: ["updated", "updated-system"],
          });
          if (overrides && overrides.length > 0) {
            overrides.map((ov) => {
              if (moment(ov.newTime).isAfter(moment(endTime))) {
                endTime = moment(ov.newTime).add(1, "days");
              }
            });
          }

          // All other scheduleTypes, determine last air / applicable datetime based on newTime or originalTime
        } else {
          endTime = moment(
            schedule.newTime || schedule.originalTime || "2002-01-01T00:00:00Z"
          ).add(1, "days");
        }

        // If the schedule endTime is before our deleteBefore time, then it should be deleted.
        if (moment(inputs.deleteBefore).isSameOrAfter(moment(endTime))) {
          await sails.models.schedule.destroyOne({ ID: schedule.ID });
          deletedSchedules++;

          // Also delete its overrides
          let overrides = await sails.models.schedule.find({
            or: [{ scheduleID: schedule.ID }, { overriddenID: schedule.ID }],
          });
          let maps2 = overrides.map(async (override) => {
            await sails.models.schedule.destroyOne({ ID: override.ID });
          });
          await Promise.all(maps2);
        }
      });
      await Promise.all(maps);
    }

    return deletedSchedules;
  },
};
