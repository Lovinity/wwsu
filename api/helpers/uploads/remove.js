const fs = require("fs");

module.exports = {
  friendlyName: "sails.helpers.uploads.remove",

  description: "Remove an uploaded file.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "ID of the temp file to delete",
    },
  },

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    try {
      // Find the record
      let record = await sails.models.uploads.findOne(inputs.ID);
      if (!record) return;

      // Now, delete the record and the file
      await sails.models.uploads.destroyOne({ ID: inputs.ID });
      fs.unlinkSync(record.path);
    } catch (e) {
      // Do not throw errors
      sails.log.error(e);
      return;
    }
  },
};
