let analyticsCalculation = 0;

module.exports = {
  friendlyName: "attendance.calculateStats",

  description:
    "Re-calculate weekly analytics and broadcast them through the websockets",

  inputs: {},

  fn: async function (inputs, exits) {
    sails.log.debug("Helper attendance.calculateStats called.");

    try {
      // Indicate we are recalculating
      await sails.helpers.meta.change.with({ recalculatingAnalytics: true });
      analyticsCalculation++;

      let earliest = moment().subtract(7, "days");

      // Get all showtime stats for shows within the last week
      let stats = await sails.helpers.analytics.showtime(
        undefined,
        undefined,
        earliest.toISOString(true)
      );
      stats = stats[1]; // We only want show stats, not DJ stats

      // Map stats into array of objects, and move key into ID property.
      let stats2 = [];
      for (let key in stats) {
        if (!Object.prototype.hasOwnProperty.call(stats, key)) continue;
        stats2.push(Object.assign(stats[key], { ID: parseInt(key) }));
      }

      // Prepare with a clean template
      sails.models.attendance.weeklyAnalytics = {
        topShows: [],
        topGenre: { name: "None", score: 0 },
        topPlaylist: { name: "None", score: 0 },
        onAir: 0,
        listeners: 0,
        onAirListeners: 0,
        listenerPeak: 0,
        listenerPeakTime: null,
        viewers: 0,
        onAirViewers: 0,
        VideoTime: 0,
        viewerPeak: 0,
        viewerPeakTime: null,
        tracksLiked: 0,
        tracksRequested: 0,
        webMessagesExchanged: 0,
        discordMessagesExchanged: 0,
      };

      // Function for calculating a program's score for determining which should be top
      // TODO: Move to configuration
      let showScore = (show) => {
        // Shows with less than 15 minutes showtime are disqualified from any points; they are too short to be considered quality broadcasts.
        if (show.showTime < 15) return 0;

        let aScore = 0;

        // Percentile converts proportions of stats covering full show time down to 1 hour via multiplication.
        // Division by zero is not possible as we bailed already if showTime is less than 15.
        let divide60by = 60 / show.showTime;

        // 60 points for every 1 online listener to showtime ratio (roughly translates to 60 points for every average online listener)
        // Awards members more for higher listenership numbers during their show
        aScore += show.ratio * 60;

        // 6 points for every 1 average message per hour of showtime.
        // This awards members for interactivity
        aScore += show.webMessages * divide60by * (6 / 60);

        // Do not award the proceeding points for genres nor playlists
        if (["genre", "playlist"].indexOf(show.type) === -1) {
          // Score 60 points for taking 3 or more average breaks per hour of show time; score less if took less breaks.
          // This awards members for taking regular breaks during their show (regular breaks = higher quality show)
          let aMaxBreaks = (show.showTime / 60) * 3;
          aScore += Math.min(aMaxBreaks, show.breaks) * divide60by * (60 / 60);

          // 120 points for every 1 video viewer to showtime ratio (roughly translates to 120 points for every average online viewer)
          // Awards members more for higher viewership numbers during their show; video should award more than just audio.
          aScore += show.viewerRatio * 120;

          // 30 points for every 1 hour of video streaming on average per 1 hour of show time.
          // This ensures members still get a small reward for video streaming even when they don't have watchers.
          aScore += show.videoTime * divide60by * (30 / 60);
        }

        // if reputationScoreMax is 0, reputationPercent is always 100 because of division by zero.
        if (show.reputationScoreMax <= 0) show.reputationPercent = 100;

        // Multiply final score by reputationPercent
        // Reputationpercent goes down (and severely penalizes the member) for violations such as:
        // Missed top-of-hour ID breaks, early/late starts/ends, silence alarms, unexcused absences, excessive cancellations, etc
        aScore *= show.reputationPercent / 100;

        return aScore;
      };

      // Sort array in place according to show score, then round the score down (we do rounding after sorting)
      stats2 = stats2
        .map((stat) => {
          return Object.assign(stat, { showScore: showScore(stat) });
        })
        .sort((a, b) => b.showScore - a.showScore)
        .map((stat) => {
          stat.showScore = Math.floor(stat.showScore);
          return stat;
        });

      // Prepare parallel function 1
      let f1 = async () => {
        // Grab count of liked tracks from last week
        sails.models.attendance.weeklyAnalytics.tracksLiked =
          await sails.models.songsliked.count({
            createdAt: { ">=": earliest.toISOString(true) },
          });

        // Grab count of requested tracks
        sails.models.attendance.weeklyAnalytics.tracksRequested =
          await sails.models.requests.count({
            createdAt: { ">=": earliest.toISOString(true) },
          });

        // Grab count of webMessagesExchanged
        sails.models.attendance.weeklyAnalytics.webMessagesExchanged =
          await sails.models.messages.count({
            status: "active",
            or: [
              { from: { startsWith: "website" } },
              { to: { startsWith: "website" } },
            ],
            createdAt: { ">=": earliest.toISOString(true) },
          });

        // Grab count of discordMessagesExchanged
        sails.models.attendance.weeklyAnalytics.discordMessagesExchanged =
          await sails.models.messages.count({
            status: "active",
            or: [
              { from: { startsWith: "discord-" } },
              { to: { startsWith: "discord-" } },
            ],
            createdAt: { ">=": earliest.toISOString(true) },
          });
      };

      // Prepare parallel function 2
      let f2 = async () => {
        let records;

        // Start with shows, remotes, and prerecords
        records = stats2.filter(
          (stat) =>
            ["show", "remote", "prerecord"].indexOf(stat.type) !== -1 &&
            stat.showTime >= 15
        );
        records.forEach((record) => {
          sails.models.attendance.weeklyAnalytics.topShows.push(
            record
              ? {
                  name: record.name,
                  score: record.showScore,
                  discordChannel: record.discordChannel,
                }
              : { name: "N/A", score: 0, discordChannel: null }
          );
        });

        // Next, genres
        records = stats2.filter(
          (stat) => stat.type === "genre" && stat.showTime >= 15
        );
        if (records[0])
          sails.models.attendance.weeklyAnalytics.topGenre = {
            name: records[0].name,
            score: records[0].showScore,
          };

        // Next, playlists
        records = stats2.filter(
          (stat) => stat.type === "playlist" && stat.showTime >= 15
        );
        if (records[0])
          sails.models.attendance.weeklyAnalytics.topPlaylist = {
            name: records[0].name,
            score: records[0].showScore,
          };

        // Finally, populate other stats
        sails.models.attendance.weeklyAnalytics.onAir =
          stats[-1].showTime + stats[-2].showTime;
        sails.models.attendance.weeklyAnalytics.onAirListeners =
          stats[-1].listenerMinutes + stats[-2].listenerMinutes;
        sails.models.attendance.weeklyAnalytics.onAirViewers =
          stats[-1].viewerMinutes + stats[-2].viewerMinutes;
        sails.models.attendance.weeklyAnalytics.listeners =
          stats[0].listenerMinutes;
        sails.models.attendance.weeklyAnalytics.videoTime = stats[0].videoTime;
        sails.models.attendance.weeklyAnalytics.viewers =
          stats[0].viewerMinutes;
        sails.models.attendance.weeklyAnalytics.listenerPeak =
          stats[0].listenerPeak;
        sails.models.attendance.weeklyAnalytics.viewerPeak =
          stats[0].viewerPeak;
        sails.models.attendance.weeklyAnalytics.listenerPeakTime =
          stats[0].listenerPeakTime;
        sails.models.attendance.weeklyAnalytics.viewerPeakTime =
          stats[0].viewerPeakTime;
      };

      // Execute our parallel functions and wait for them to resolve.
      await Promise.all([f1(), f2()]);

      // Broadcast socket
      sails.sockets.broadcast(
        "analytics-weekly-dj",
        "analytics-weekly-dj",
        sails.models.attendance.weeklyAnalytics
      );

      // Deactivate recalculation message
      analyticsCalculation--;
      if (analyticsCalculation < 1)
        await sails.helpers.meta.change.with({ recalculatingAnalytics: false });

      // Update Discord analytics
      // TODO: Move to configuration
      if (DiscordClient && DiscordClient.readyTimestamp && DiscordClient.ws.status !== 5) {
        let channel = DiscordClient.channels.resolve("863301654748069898");
        if (channel) {
          let message = await channel.messages.fetch("864184742642712588");
          if (message) {
            message.edit(`
:medal: **__Top performing shows__** :medal: 
              
:first_place: **${
              sails.models.attendance.weeklyAnalytics.topShows[0]
                ? `${
                    sails.models.attendance.weeklyAnalytics.topShows[0].name
                  } (:star: ${
                    sails.models.attendance.weeklyAnalytics.topShows[0].score
                  }) ${
                    sails.models.attendance.weeklyAnalytics.topShows[0]
                      .discordChannel
                      ? `<#${sails.models.attendance.weeklyAnalytics.topShows[0].discordChannel}>`
                      : ``
                  }`
                : `None`
            }**
:second_place: ${
              sails.models.attendance.weeklyAnalytics.topShows[1]
                ? `${
                    sails.models.attendance.weeklyAnalytics.topShows[1].name
                  } (:star: ${
                    sails.models.attendance.weeklyAnalytics.topShows[1].score
                  }) ${
                    sails.models.attendance.weeklyAnalytics.topShows[1]
                      .discordChannel
                      ? `<#${sails.models.attendance.weeklyAnalytics.topShows[1].discordChannel}>`
                      : ``
                  }`
                : `None`
            }
:third_place: ${
              sails.models.attendance.weeklyAnalytics.topShows[2]
                ? `${
                    sails.models.attendance.weeklyAnalytics.topShows[2].name
                  } (:star: ${
                    sails.models.attendance.weeklyAnalytics.topShows[2].score
                  }) ${
                    sails.models.attendance.weeklyAnalytics.topShows[2]
                      .discordChannel
                      ? `<#${sails.models.attendance.weeklyAnalytics.topShows[2].discordChannel}>`
                      : ``
                  }`
                : `None`
            }

**Top genre:** ${
              sails.models.attendance.weeklyAnalytics.topGenre.name
            } (:star: ${sails.models.attendance.weeklyAnalytics.topGenre.score})
**Top playlist:** ${
              sails.models.attendance.weeklyAnalytics.topPlaylist.name
            } (:star: ${
              sails.models.attendance.weeklyAnalytics.topPlaylist.score
            })

(Top shows / scores are determined by online listenership / viewership, interactivity [messages sent and received], and compliance with WWSU / FCC regulations)

:heavy_minus_sign:

**On-air hours** (live shows, remotes, prerecords, and sports): ${
              Math.round(
                (sails.models.attendance.weeklyAnalytics.onAir || 0) / 6
              ) / 10
            } (${
              Math.round(
                (sails.models.attendance.weeklyAnalytics.onAir /
                  60 /
                  (24 * 7)) *
                  1000
              ) / 10
            }% of the week)
**Online listener hours**: ${
              Math.round(
                (sails.models.attendance.weeklyAnalytics.listeners || 0) / 6
              ) / 10
            } (${
              sails.models.attendance.weeklyAnalytics.listeners &&
              sails.models.attendance.weeklyAnalytics.onAirListeners &&
              sails.models.attendance.weeklyAnalytics.listeners > 0
                ? Math.round(
                    (sails.models.attendance.weeklyAnalytics.onAirListeners /
                      sails.models.attendance.weeklyAnalytics.listeners) *
                      1000
                  ) / 10
                : 0
            }% of listenership was during on-air programming)
**Peak online listeners:** ${
              sails.models.attendance.weeklyAnalytics.listenerPeak
            } (${moment(
              sails.models.attendance.weeklyAnalytics.listenerPeakTime
            ).format("MM/DD h:mm A")} station time)
**Video stream viewer hours**: ${
              Math.round(
                (sails.models.attendance.weeklyAnalytics.viewers || 0) / 6
              ) / 10
            } (Video stream was active for ${
              Math.round(
                (sails.models.attendance.weeklyAnalytics.videoTime || 0) / 6
              ) / 10
            } hours)
**Peak video stream viewers:** ${
              sails.models.attendance.weeklyAnalytics.viewerPeak
            } (${moment(
              sails.models.attendance.weeklyAnalytics.viewerPeakTime
            ).format("MM/DD h:mm A")} station time)

:heavy_minus_sign: 

**Tracks Requested Online:** ${
              sails.models.attendance.weeklyAnalytics.tracksRequested
            } (Liked: ${sails.models.attendance.weeklyAnalytics.tracksLiked})
**Messages exchanged online:** ${
              sails.models.attendance.weeklyAnalytics.webMessagesExchanged
            }
**Messages exchanged in Discord server:** ${
              sails.models.attendance.weeklyAnalytics.discordMessagesExchanged
            }

:heavy_minus_sign: 

**Total members in Discord server:** ${
              DiscordClient.guilds.resolve("830253278465097758").memberCount // TODO: Move guild ID to config
            }
`);
          }
        }
      }

      return exits.success([
        sails.models.attendance.weeklyAnalytics,
        stats,
        stats2,
      ]);
    } catch (e) {
      analyticsCalculation--;
      if (analyticsCalculation < 1)
        await sails.helpers.meta.change.with({ recalculatingAnalytics: false });
      return exits.error(e);
    }
  },
};
