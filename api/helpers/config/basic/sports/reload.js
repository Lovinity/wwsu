module.exports = {
  friendlyName: "helpers.config.basic.sports.reload",

  description:
    "Add calendar events for each configured sports; deactivate calendar events no longer existing in sports config.",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Add sports into the calendar as a non-scheduled event if they do not exist
    sails.config.custom.basic.sports.map((sport) => {
      (async (_sport) => {
        sails.models.calendar
          .findOrCreate(
            { type: "sports", name: sport },
            {
              type: "sports",
              active: true,
              priority: sails.models.calendar.calendardb.getDefaultPriority({
                type: "sports",
              }),
              name: sport,
              startDate: moment().toISOString(true),
            }
          )
          .exec(async (err, record, wasCreated) => {
            if (!wasCreated)
              await sails.models.calendar
                .update(
                  { ID: record.ID },
                  {
                    active: true,
                    priority:
                      sails.models.calendar.calendardb.getDefaultPriority({
                        type: "sports",
                      }),
                    startDate: moment().toISOString(true),
                  }
                )
                .fetch();
          });
      })(sport);
    });

    // De-activate main sports events that do not exist in the system configured list of sports
    await sails.models.calendar
      .update(
        { type: "sports", name: { nin: sails.config.custom.basic.sports } },
        { active: false }
      )
      .fetch();
  },
};
