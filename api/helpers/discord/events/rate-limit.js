let rateLimitTimeoutUntil;
let rateLimitTimeout;

module.exports = {


  friendlyName: 'Rate limit',


  description: '',


  inputs: {
    rateLimitData: {
      type: "ref"
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs) {
    await sails.helpers.status.change.with({
      name: "discord",
      status: 4,
      data: `The Discord bot hit a rate limit: ${inputs.rateLimitData.method} ${inputs.rateLimitData.path} ${inputs.rateLimitData.route}`,
    });

    if (moment(rateLimitTimeoutUntil).isBefore(moment().add(inputs.rateLimitData.timeout, "milliseconds"))) {
      rateLimitTimeoutUntil = moment().add(inputs.rateLimitData.timeout, "milliseconds");
      clearTimeout(rateLimitTimeout);

      rateLimitTimeout = setTimeout(async () => {
        if (!DiscordClient || !DiscordClient.readyTimestamp || DiscordClient.ws.status === 5) return;

        await sails.helpers.status.change.with({
          name: "discord",
          status: 5,
          data: `The Discord Bot is online and operational.`,
        });
      }, inputs.rateLimitData.timeout);
    }
  }


};

