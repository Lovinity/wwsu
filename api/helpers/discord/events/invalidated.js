module.exports = {
  friendlyName: "Invalidated",

  description:
    "Invalidated event in Discord; emitted when disconnecting and not allowed to reconnect.",

  inputs: {},

  exits: {},

  fn: async function (inputs) {
    await sails.helpers.status.change.with({
      name: "discord",
      status: 3,
      data: "The Discord Bot has been disconnected and refuses to re-connect due to an error. Things to check:<ul><li>Network connection / Discord status</li><li>Bot token</li><li>If any API rate limits / violations occurred</li><li>If Discord banned the bot / application</li></ul>",
    });
  },
};
