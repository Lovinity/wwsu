module.exports = {
  friendlyName: "Ready",

  description: "Discord ready event",

  inputs: {
    client: {
      type: "ref",
    },
  },

  fn: async function (inputs) {
    sails.log.debug(`DISCORD: Bot is online.`);

    // Guild check
    let guild = await inputs.client.guilds.fetch(
      sails.config.custom.discord.guild
    );
    if (guild) {
      await sails.helpers.status.change.with({
        name: "discord",
        status: 5,
        data: "The Discord Bot is online and operational.",
      });
    } else {
      await sails.helpers.status.change.with({
        name: "discord",
        status: 3,
        data: "The Discord Bot is online. However, the provided guild Snowflake ID in configuration did not resolve to a guild. Make sure you have the correct guild Snowflake ID for the WWSU server in the configuration. And make sure the bot is in the guild.",
      });
    }
  },
};
