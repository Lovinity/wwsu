module.exports = {
  friendlyName: "Send blog",

  description: "Post about a new blog post on WWSU's website.",

  inputs: {
    blog: {
      type: "ref",
      required: true,
      description: "The blog post.",
    },
  },

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    if (
      DiscordClient &&
      DiscordClient.readyTimestamp &&
      DiscordClient.ws.status !== 5
    ) {
      // Get author
      let author = inputs.blog.author
        ? await sails.models.djs.findOne({ ID: inputs.blog.author })
        : undefined;

      // Get the blog channel
      let channel = DiscordClient.channels.resolve(
        "955966625209794600" // TODO: move to config.custom
      );

      // Send the message
      if (channel) {
        let message = await channel.send({
          content: `**__${inputs.blog.title}__**
by ${
            author
              ? `${author.realName || "Unknown Author"} (aka. "${author.name}")`
              : "Unknown Author"
          }
                
${inputs.blog.summary}

${sails.config.custom.baseUrl}/blog/${inputs.blog.ID}`,
        });

        // Publish the message if we can do so.
        if (message && message.crosspostable) message.crosspost();
      }

      // Also send message in Wright News Feeds guild

      // Get the feed channel
      let channel2 = DiscordClient.channels.resolve("915236850912866304"); // TODO: move to sails.config.custom

      // Send the embed
      if (channel2)
        channel2.send({
          content: `**__${inputs.blog.title}__**
by ${
            author
              ? `${author.realName || "Unknown Author"} (aka. "${author.name}")`
              : "Unknown Author"
          }
              
${inputs.blog.summary}

${sails.config.custom.baseUrl}/blog/${inputs.blog.ID}`,
        });
    }

    return;
  },
};
