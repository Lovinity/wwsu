const weatherCodeString = {
  0: "Unknown",
  1000: "Clear",
  1001: "Cloudy",
  1100: "Mostly Clear",
  1101: "Partly Cloudy",
  1102: "Mostly Cloudy",
  2000: "Fog",
  2100: "Light Fog",
  3000: "Light Wind",
  3001: "Wind",
  3002: "Strong Wind",
  4000: "Drizzle",
  4001: "Rain",
  4200: "Light Rain",
  4201: "Heavy Rain",
  5000: "Snow",
  5001: "Flurries",
  5100: "Light Snow",
  5101: "Heavy Snow",
  6000: "Freezing Drizzle",
  6001: "Freezing Rain",
  6200: "Light Freezing Rain",
  6201: "Heavy Freezing Rain",
  7000: "Ice Pellets",
  7101: "Heavy Ice Pellets",
  7102: "Light Ice Pellets",
  8000: "Downpour Rain",

  // Custom
  8100: "Whiteout Snow",
  8200: "Freezing Downpour",
  8300: "Hail",
};

const weatherCodeColor = {
  0: "#000000",
  1000: "#FFD700",
  1001: "#665600",
  1100: "#FFD700",
  1101: "#B29600",
  1102: "#B29600",
  2000: "#665600",
  2100: "#665600",
  3000: "#7FBF7F",
  3001: "#008000",
  3002: "#004000",
  4000: "#B2B2FF",
  4001: "#6666FF",
  4200: "#B2B2FF",
  4201: "#0000FF",
  5000: "#787878",
  5001: "#AEAEAE",
  5100: "#AEAEAE",
  5101: "#484848",
  6000: "#E2A3FF",
  6001: "#CF66FF",
  6200: "#E2A3FF",
  6201: "#B000FF",
  7000: "#CF66FF",
  7101: "#B000FF",
  7102: "#E2A3FF",
  8000: "#FF0000",
  8100: "#000000",
  8200: "#4400FF",
  8300: "#4400FF",
};

const adjustedWeather = (record) => {
  // Modify weather based on wind gust
  if (record.data.windGust >= 34 || record.data.windSpeed >= 22)
    record.data.weatherCode = 3000; // Light Wind
  if (record.data.windGust >= 46 || record.data.windSpeed >= 31)
    record.data.weatherCode = 3001; // Wind (wind advisory criteria)
  if (record.data.windGust >= 58 || record.data.windSpeed >= 40)
    record.data.weatherCode = 3002; // Strong Wind (high wind warning criteria)

  // If a precip Type is set and intensity is > 0, weather code should reflect the precipitation event
  // We are also using our own scale of how heavy the precip is.
  if (record.data.precipitationType && record.data.precipitationIntensity > 0) {
    switch (record.data.precipitationType) {
      case 1: // Rain
        record.data.weatherCode = 4000; // Default to lowest: drizzle
        if (record.data.precipitationIntensity >= 0.009)
          record.data.weatherCode = 4200; // Light Rain
        if (record.data.precipitationIntensity >= 0.19)
          record.data.weatherCode = 4001; // Rain
        if (record.data.precipitationIntensity >= 0.7)
          record.data.weatherCode = 4201; // Heavy Rain
        if (record.data.precipitationIntensity >= 4.7)
          record.data.weatherCode = 8000; // Downpour
        break;
      case 2: // Snow
        record.data.weatherCode = 5001; // Default to lowest: flurries
        if (record.data.precipitationIntensity >= 0.014)
          record.data.weatherCode = 5100; // Light Snow
        if (record.data.precipitationIntensity >= 0.07)
          record.data.weatherCode = 5000; // Snow
        if (record.data.precipitationIntensity >= 0.6)
          record.data.weatherCode = 5101; // Heavy Snow
        if (record.data.precipitationIntensity >= 2.36)
          record.data.weatherCode = 8100; // Whiteout Snow
        break;
      case 3: // Freezing Rain
        record.data.weatherCode = 6000; // Default to lowest: freezing drizzle
        if (record.data.precipitationIntensity >= 0.014)
          record.data.weatherCode = 6200; // Light Freezing Rain
        if (record.data.precipitationIntensity >= 0.07)
          record.data.weatherCode = 6001; // Freezing Rain
        if (record.data.precipitationIntensity >= 0.6)
          record.data.weatherCode = 6201; // Heavy Freezing Rain
        if (record.data.precipitationIntensity >= 2.36)
          record.data.weatherCode = 8200; // Freezing Downpour
        break;
      case 4: // Ice
        record.data.weatherCode = 6000; // Default to lowest: freezing drizzle
        if (record.data.precipitationIntensity >= 0.014)
          record.data.weatherCode = 7102; // Light Ice Pellets
        if (record.data.precipitationIntensity >= 0.07)
          record.data.weatherCode = 7000; // Ice Pellets
        if (record.data.precipitationIntensity >= 0.6)
          record.data.weatherCode = 7101; // Heavy Ice Pellets
        if (record.data.precipitationIntensity >= 2.36)
          record.data.weatherCode = 8300; // Hail
        break;
    }
  }

  return record;
};

module.exports = {
  friendlyName: "Update",

  description: "Update climacell weather data.",

  inputs: {},

  exits: {},

  fn: async function (inputs) {
    if (
      !sails.config.custom.climacell.api ||
      sails.config.custom.climacell.api === ""
    )
      return;

    // Get data from climacell
    let url = new URL("https://api.tomorrow.io/v4/timelines");
    url.searchParams.append("apikey", sails.config.custom.climacell.api);
    url.searchParams.append("location", `${sails.config.custom.climacell.position.latitude},${sails.config.custom.climacell.position.longitude}`);
    url.searchParams.append("units", "imperial");
    [
      "temperature",
      "temperatureApparent",
      "dewPoint",
      "humidity",
      "windSpeed",
      "windDirection",
      "windGust",
      "precipitationIntensity",
      "precipitationProbability",
      "precipitationType",
      "visibility",
      "cloudCover",
      "weatherCode",
      "pressureSurfaceLevel",
      "pressureSeaLevel",
      "uvIndex",
      "epaIndex",
      "epaHealthConcern",
    ].forEach((field) => {
      url.searchParams.append("fields", field);
    });
    ["current", "5m", "1h"].forEach((timestep) => {
      url.searchParams.append("timesteps", timestep);
    });
    let { body } = await needle("get", url.toString(), {
      json: true
    });

    // Exit if there was an error, there was no body, or the body timelines constructor is not an array
    if (
      !body ||
      body.errorCode ||
      !body.data ||
      !body.data.timelines ||
      body.data.timelines.constructor !== Array
    ) {
      sails.log.error(new Error(body));
      await sails.helpers.status.change.with({
        name: "tomorrowio",
        label: "Tomorrow.io",
        status: 3,
        data: "Tomorrow.io did not return a proper body with data and a timeline. Make sure the website is responding, your developer credentials are correct, and the account is in good standing.",
      });
      return;
    }

    // Run through operations in the body for each timestep in the array
    let maps = body.data.timelines.map(async (timeline) => {
      // Skip if there are no intervals
      if (!timeline.intervals || timeline.intervals.constructor !== Array)
        return;

      // Run through each interval in the timeline
      let iMaps = timeline.intervals.map(async (interval, index) => {
        // No values? Exit.
        if (!interval.values) return;

        let dataClass = `${timeline.timestep}-${index}`;

        // Create the record if it does not exist
        sails.models.climacell
          .findOrCreate(
            { dataClass: dataClass },
            {
              dataClass: dataClass,
              data: interval.values,
              dataTime: moment(interval.startTime).toISOString(true),
            }
          )
          .exec(async (err, record, wasCreated) => {
            // Exit on error or if the record was new / created
            if (err) {
              sails.log.error(err);
              return;
            }
            if (wasCreated) {
              return;
            }

            // Update only if the data changed
            if (
              record &&
              !_.isEqual(
                { data: record.data, dataTime: record.dataTime },
                {
                  data: interval.values,
                  dataTime: moment(interval.startTime).toISOString(true),
                }
              )
            )
              await sails.models.climacell
                .update(
                  { dataClass: dataClass },
                  {
                    dataClass: dataClass,
                    data: interval.values,
                    dataTime: moment(interval.startTime).toISOString(true),
                  }
                )
                .fetch();
          });

        // Cache current-0 in climacell
        if (dataClass === "current-0") {
          let adjusted = adjustedWeather({
            data: Object.assign({}, interval.values),
          });
          // Create the record if it does not exist
          sails.models.cache
            .findOrCreate(
              { ID: "climacell-current-0" },
              {
                ID: "climacell-current-0",
                model: "climacell",
                data: Object.assign(adjusted.data, {
                  ID: "climacell-current-0",
                }),
              }
            )
            .exec(async (err, record, wasCreated) => {
              if (err) {
                sails.log.error(err);
                return;
              }

              // Update existing records if data changed
              if (!wasCreated && !_.isEqual(record.data, adjusted.data)) {
                await sails.models.cache.updateOne(
                  { ID: "climacell-current-0" },
                  {
                    ID: "climacell-current-0",
                    model: "climacell",
                    data: Object.assign(adjusted.data, {
                      ID: "climacell-current-0",
                    }),
                  }
                );
              }
            });
        }
      });
      await Promise.all(iMaps);
    });
    await Promise.all(maps);

    // Wait 5 seconds (to allow climacell database records to update) and then Generate short-term hourly forecast and clockwheel data
    setTimeout(async () => {
      let records = await sails.models.climacell.find();

      // Generate clockwheels
      let segments = [];

      // Initialize segments array
      for (let i = 0; i < 720; i++) {
        segments[i] = {};
      }

      let updateClockwheel = (weather, start, length) => {
        while (length > 0) {
          length--;
          start++;
          segments[start] = weather;
          if (start >= 720) {
            start -= 720;
          }
        }
      };

      // Determine what the exact date/time is for the "12" (start of the doughnut chart) on the clock
      let topOfClock = moment().startOf("day").add(1, "days");
      if (moment().hours() < 12) {
        topOfClock = moment.parseZone(topOfClock).subtract(12, "hours");
      }

      // Determine number of minutes from current time to topOfClock
      let untilTopOfClock = moment(topOfClock).diff(moment(), "minutes");

      // Initialize variables for tracking when the weather changes
      let weatherCode = 0;
      let weatherData = {};
      let totalLength = 0;
      let startTime = moment();

      // Initialize the above with current conditions
      let currently = records.find(
        (record) => record.dataClass === "current-0"
      );
      if (currently) {
        currently = adjustedWeather(currently);
        startTime = currently.dataTime;
        weatherCode = currently.data.weatherCode;
      }

      // Now process every record sorted by dataTime
      let currentTime = moment();
      let shortTerm = records
        .sort((a, b) => {
          if (!a.dataTime) return 1;
          if (!b.dataTime) return -1;
          if (moment(a.dataTime).isBefore(moment(b.dataTime))) return -1;
          if (moment(b.dataTime).isBefore(moment(a.dataTime))) return 1;
          return 0;
        })
        .map((record) => {
          record = adjustedWeather(record);
          // Determine clockwheel segments
          if (
            !record.dataClass.startsWith("1h-") ||
            moment(currentTime)
              .add(6, "hours")
              .isBefore(moment(record.dataTime))
          ) {
            if (
              moment(record.dataTime).isSameOrBefore(
                moment(currentTime),
                "minutes"
              )
            ) {
              weatherCode = record.data.weatherCode;
              weatherData = record.data;
              startTime = currentTime;

              // If weatherCode changed, create a new segment on the chart
            } else if (weatherCode !== record.data.weatherCode) {
              // Calculate segment length and start
              let length = moment(record.dataTime).diff(startTime, "minutes");
              let start =
                720 -
                untilTopOfClock +
                moment(startTime).diff(currentTime, "minutes");

              // Correct length if it goes beyond 12 hours
              if (
                moment(record.dataTime).isAfter(
                  moment.parseZone(currentTime).add(12, "hours"),
                  "minutes"
                )
              ) {
                let correction = moment(record.dataTime).diff(
                  moment.parseZone(currentTime).add(12, "hours"),
                  "minutes"
                );
                length -= correction;
              }

              if (start >= 720) {
                start -= 720;
              }

              // Add segment
              updateClockwheel(weatherData, start, length);

              // Update memory info
              startTime = record.dataTime;
              weatherCode = record.data.weatherCode;
              weatherData = record.data;
              totalLength += length;
            }
          }
        });

      // Now, begin updating clockwheel segments
      let clockwheelDonutData = {
        ID: "climacell-clockwheel",
        labels: [],
        datasets: [
          {
            data: [],
            backgroundColor: [],
          },
        ],
      };

      // Process donut segments
      let currentSegment = { weatherCode: null, minutes: 0 };
      segments.map((segment) => {
        // If we have a new id at this minute, create a new segment
        if (segment.weatherCode !== currentSegment.weatherCode) {
          clockwheelDonutData.labels.push(
            weatherCodeString[currentSegment.weatherCode] || "Unknown"
          );
          clockwheelDonutData.datasets[0].data.push(currentSegment.minutes);
          clockwheelDonutData.datasets[0].backgroundColor.push(
            weatherCodeColor[currentSegment.weatherCode] || "#000000"
          );
          currentSegment = Object.assign(segment, { minutes: 1 });
        } else {
          currentSegment.minutes++;
        }
      });

      // Push the last remaining segment into data
      clockwheelDonutData.labels.push(
        weatherCodeString[currentSegment.weatherCode] || "Unknown"
      );
      clockwheelDonutData.datasets[0].data.push(currentSegment.minutes);
      clockwheelDonutData.datasets[0].backgroundColor.push(
        weatherCodeColor[currentSegment.weatherCode] || "#000000"
      );

      // Create the record if it does not exist
      sails.models.cache
        .findOrCreate(
          { ID: "climacell-clockwheel", model: "climacell" },
          {
            ID: "climacell-clockwheel",
            model: "climacell",
            data: clockwheelDonutData,
          }
        )
        .exec(async (err, record, wasCreated) => {
          if (err) {
            sails.log.error(err);
            return;
          }

          // Update existing records if data changed
          if (!wasCreated && !_.isEqual(record.data, clockwheelDonutData)) {
            await sails.models.cache.updateOne(
              { ID: "climacell-clockwheel", model: "climacell" },
              {
                ID: "climacell-clockwheel",
                model: "climacell",
                data: clockwheelDonutData,
              }
            );
          }
        });

      // Generate short term info
      currentTime = moment();
      let data = { ID: "climacell-forecast-hourly-12h", forecast: {} };
      shortTerm = records
        .sort((a, b) => {
          if (!a.dataTime) return 1;
          if (!b.dataTime) return -1;
          if (moment(a.dataTime).isBefore(moment(b.dataTime))) return -1;
          if (moment(b.dataTime).isBefore(moment(a.dataTime))) return 1;
          return 0;
        })
        .map((record) => {
          record = adjustedWeather(record);
          // Add weather descriptions
          if (record.dataClass === `current-0`) {
            data.forecast["Now"] = {
              weather: weatherCodeString[record.data.weatherCode],
              temperature: `${parseInt(record.data.temperature)}°F`,
            };
          } else if (
            record.dataClass.startsWith("1h-") &&
            record.dataClass !== `1h-0` &&
            moment(currentTime)
              .add(12, "hours")
              .isSameOrAfter(moment(record.dataTime))
          ) {
            data.forecast[moment(record.dataTime).format("h:mm A")] = {
              weather: weatherCodeString[record.data.weatherCode],
              temperature: `${parseInt(record.data.temperature)}°F`,
            };
          }
        });

      // Create the record if it does not exist
      sails.models.cache
        .findOrCreate(
          { ID: "climacell-forecast-hourly-12h" },
          {
            ID: "climacell-forecast-hourly-12h",
            model: "climacell",
            data: data,
          }
        )
        .exec(async (err, record, wasCreated) => {
          if (err) {
            sails.log.error(err);
            return;
          }

          // Update existing records if data changed
          if (!wasCreated && !_.isEqual(record.data, data)) {
            await sails.models.cache.updateOne(
              { ID: "climacell-forecast-hourly-12h" },
              {
                ID: "climacell-forecast-hourly-12h",
                model: "climacell",
                data: data,
              }
            );
          }
        });
    }, 5000);

    await sails.helpers.status.change.with({
      name: "tomorrowio",
      label: "Tomorrow.io",
      status: 5,
      data: "Tomorrow.io is operational.",
    });

    return maps;
  },
};
