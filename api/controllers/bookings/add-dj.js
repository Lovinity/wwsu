module.exports = {
  friendlyName: "bookings/add-dj",

  description: "Add DJ booking",

  inputs: {
    type: {
      type: "string",
      isIn: ["onair-booking", "prod-booking"],
      required: true,
      description: "The type of booking",
    },
    reason: {
      type: "string",
      required: true,
      description: "Reason for booking the studio",
    },
    start: {
      type: "ref",
      required: true,
      custom: function (value) {
        return moment(value).isValid();
      },
    },
    duration: {
      type: "number",
      min: 1,
      max: 60 * 24,
      required: true,
    },
  },

  exits: {},

  fn: async function (inputs) {
    try {
      sails.models.status.tasks.calendar++;

      if (inputs.duration > 60 * 2) {
        sails.models.status.tasks.calendar--;
        return "Members may not book more than 2 consecutive hours of studio time / 3 total hours in a day. If you need more time, please contact a director.";
      }

      // Find the booking event
      let record = await sails.models.calendar
        .find({ type: inputs.type })
        .limit(1);
      if (!record || !record[0])
        throw new Error(
          `Internal error: a calendar event for type ${inputs.type} does not exist.`
        );

      record = record[0];

      // Find the DJ record
      let dj = await sails.models.djs.findOne({ ID: this.req.payload.ID });
      if (!dj)
        throw new Error(
          "Internal error: Authorized DJ was not found in the system."
        );

      // Check to make sure the DJ has permission to use and book studios
      if (!dj.permissions || dj.permissions.indexOf("book") === -1) {
        sails.models.status.tasks.calendar--;
        return "You have not been granted permission to book the studios. Please contact a director for assistance.";
      }

      // Check how much time the DJ has booked on the day they are requesting; reject if they booked 3 hours for that day already or if this reservation will push them above 3 hours.
      let bookedTime = inputs.duration;
      let events = sails.models.calendar.calendardb.getEvents(
        null,
        moment(inputs.start).startOf("day"),
        moment(inputs.start).startOf("day").add(1, "days"),
        { active: true, type: ["onair-booking", "prod-booking"] }
      );
      if (events && events.length) {
        events
          .filter(
            (event) =>
              moment(event.starts).isSameOrAfter(
                moment(inputs.start).startOf("day")
              ) &&
              moment(event.ends).isBefore(
                moment(inputs.start).startOf("day").add(1, "days")
              ) &&
              [
                event.hostDJ,
                event.cohostDJ1,
                event.cohostDJ2,
                event.cohostDJ3,
              ].indexOf(this.req.payload.ID) !== -1 &&
              event.scheduleType !== "canceled" &&
              event.scheduleType !== "canceled-system"
          )
          .forEach((event) => (bookedTime += event.duration));
      }

      if (bookedTime > 60 * 3) {
        // TODO: make configurable
        return `Members may not book more than 3 total hours / 180 minutes on the same day (you booked ${bookedTime} minutes). Please contact a director for assistance if you need more time.`;
      }

      let event = {
        calendarID: record.ID,
        type: inputs.type,
        hostDJ: this.req.payload.ID,
        description: inputs.reason,
        oneTime: [moment(inputs.start).format("YYYY-MM-DD")],
        startTime: moment(inputs.start).format("HH:mm"),
        duration: inputs.duration,
      };

      // Verify the event
      try {
        event = await sails.helpers.calendar.verify(event);
      } catch (e) {
        sails.models.status.tasks.calendar--;
        return e.message;
      }

      // Check for event conflicts.
      let conflicts = sails.models.calendar.calendardb.checkConflicts(null, [
        { insert: event },
      ]);
      let additions =
        conflicts.additions && conflicts.additions.length
          ? conflicts.additions.filter(
              (addition) => addition.calendarID === record.ID
            )
          : [];
      let errors = conflicts.errors;

      if (errors && errors.length) {
        sails.models.status.tasks.calendar--;
        return errors.join();
      } else if (additions && additions.length) {
        sails.models.status.tasks.calendar--;
        return `You cannot book the studio for the specified date/time; the studio is already booked.`;
      }

      // At this point, we can add the booking
      let _event = _.cloneDeep(event);
      await sails.models.schedule.create(_event).fetch();
    } catch (e) {
      sails.models.status.tasks.calendar--;
      throw e;
    }
  },
};
