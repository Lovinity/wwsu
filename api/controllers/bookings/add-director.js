module.exports = {
  friendlyName: "bookings/add-director",

  description: "Add director booking",

  inputs: {
    type: {
      type: "string",
      isIn: ["onair-booking", "prod-booking"],
      required: true,
      description: "The type of booking",
    },
    reason: {
      type: "string",
      required: true,
      description: "Reason for booking the studio",
    },
    start: {
      type: "ref",
      required: true,
      custom: function (value) {
        return moment(value).isValid();
      },
    },
    duration: {
      type: "number",
      min: 1,
      max: 60 * 24, // Directors are allowed up to system max of 24 hours.
      required: true,
    },
  },

  exits: {},

  fn: async function (inputs) {
    try {
      sails.models.status.tasks.calendar++;

      // Find the booking event
      let record = await sails.models.calendar
        .find({ type: inputs.type })
        .limit(1);
      if (!record || !record[0])
        throw new Error(
          `Internal error: a calendar event for type ${inputs.type} does not exist.`
        );

      record = record[0];

      let event = {
        calendarID: record.ID,
        type: inputs.type,
        director: this.req.payload.ID,
        description: inputs.reason,
        oneTime: [moment(inputs.start).format("YYYY-MM-DD")],
        startTime: moment(inputs.start).format("HH:mm"),
        duration: inputs.duration,
      };

      // Verify the event
      try {
        event = await sails.helpers.calendar.verify(event);
      } catch (e) {
        sails.models.status.tasks.calendar--;
        return e.message;
      }

      // Check for event conflicts.
      let conflicts = sails.models.calendar.calendardb.checkConflicts(null, [
        { insert: event },
      ]);
      let additions =
        conflicts.additions && conflicts.additions.length
          ? conflicts.additions.filter(
              (addition) => addition.calendarID === record.ID
            )
          : [];
      let errors = conflicts.errors;

      if (errors && errors.length) {
        sails.models.status.tasks.calendar--;
        return errors.join();
      } else if (additions && additions.length) {
        sails.models.status.tasks.calendar--;
        return `You cannot book the studio for the specified date/time; the studio is already booked.`;
      }

      // At this point, we can add the booking
      let _event = _.cloneDeep(event);
      await sails.models.schedule.create(_event).fetch();
    } catch (e) {
      sails.models.status.tasks.calendar--;
      throw e;
    }
  },
};
