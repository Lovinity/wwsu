module.exports = {
  friendlyName: "Upload",

  description: "Upload a file to the system.",

  inputs: {
    host: {
      type: "string",
      required: true,
      description:
        "The host string uploading content (since jquery.fileupload does not support the authentication module).",
    },
    type: {
      type: "string",
      required: true,
      description: "The type of file being uploaded",
      isIn: ["calendar/logo", "calendar/banner", "directors", "djs", "blogs"],
    },
  },

  exits: {},

  fn: async function (inputs) {
    return await new Promise(async (resolve, reject) => {
      // First, validate the host.
      let host = await sails.models.hosts.findOrCreate(
        { host: inputs.host },
        {
          host: inputs.host,
          friendlyname: `Unknown ${inputs.host.substr(inputs.host.length - 8)}`,
        }
      );
      if (!host || !host.authorized) {
        return reject("The host provided is not authorized.");
      }

      // Then, determine the query parameter by mapping inputs.type with parameter
      const paramMap = {
        "calendar/logo": "logo_files",
        "calendar/banner": "banner_files",
        directors: "avatar_files",
        djs: "avatar_files",
        blogs: "featuredImage_files"
      };

      // Now, get the files.
      this.req.file(paramMap[inputs.type]).upload(
        {
          dirname: `./${inputs.type}`,
          maxBytes: 1024 * 1024 * 8,
        },
        async (err, _files) => {
          if (err) return reject(err);

          let files = _files.map(async (file, index) => {
            let record = await sails.models.uploads
              .create({
                hostID: host.ID,
                path: file.fd,
                type: inputs.type,
              })
              .fetch();

            // Alpaca format
            return {
              id: record.ID,
              name: file.filename,
              size: file.size,
              url: `${sails.config.custom.baseUrl}/uploads/get/${record.ID}`,
              thumbnailUrl: `${sails.config.custom.baseUrl}/uploads/get/${record.ID}`,
              deleteUrl: `${sails.config.custom.baseUrl}/uploads/remove?ID=${record.ID}&host=${inputs.host}`,
              deleteType: "GET",
            };
          });

          let filesFinal = await Promise.all(files);

          return resolve({ files: filesFinal });
        }
      );
    });
  },
};
