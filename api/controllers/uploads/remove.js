module.exports = {
  friendlyName: "Delete",

  description: "Delete an upload.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "ID of the temp file to delete",
    },
    host: {
      type: "string",
      required: true,
      description:
        "The host string deleting content (since jquery.fileupload does not support the authentication module).",
    },
  },

  exits: {},

  fn: async function (inputs) {
    // First, validate the host.
    let host = await sails.models.hosts.findOrCreate(
      { host: inputs.host },
      {
        host: inputs.host,
        friendlyname: `Unknown ${inputs.host.substr(inputs.host.length - 8)}`,
      }
    );
    if (!host || !host.authorized) {
      return reject("The host provided is not authorized.");
    }

    return await sails.helpers.uploads.remove(inputs.ID);
  },
};
