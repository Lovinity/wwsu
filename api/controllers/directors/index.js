module.exports = {
    friendlyName: "directors/",
  
    description: "Directors page",
  
    inputs: {},
  
    exits: {
      success: {
        responseType: "view",
        viewTemplatePath: "website/directors",
      },
    },
  
    fn: async function (inputs) {
      // All done.
      return {
        layout: "website/layout",
        meta: {
          description: `View the director profiles of WWSU, their office hours for the next 7 days, and whether or not they are currently in the office.`,
          author: "WWSU 106.9 FM",
          "og:title": `Directors - WWSU 106.9 FM`,
          "og:url": `${sails.config.custom.baseUrl}/directors`,
          "og:type": "website",
          "og:description": `View the director profiles of WWSU, their office hours for the next 7 days, and whether or not they are currently in the office.`,
          "og:site_name": "WWSU 106.9 FM",
        },
      };
    },
  };