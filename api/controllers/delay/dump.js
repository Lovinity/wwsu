module.exports = {
  friendlyName: "delay / dump",

  description:
    "Transmit socket event indicating whichever DJ Controls responsible for the delay system should command the delay system to dump.",

  inputs: {},

  exits: {},

  fn: async function(inputs, exits) {
    sails.log.debug("Controller delay/dump called.");

    try {
      // Prevent dumping if host has a belongsTo and the specified member is not on the air
      if (
        this.req.payload.ID !== sails.models.meta.memory.host &&
        this.req.payload.belongsTo !== null &&
        this.req.payload.belongsTo !== sails.models.meta.memory.dj &&
        this.req.payload.belongsTo !== sails.models.meta.memory.cohostDJ1 &&
        this.req.payload.belongsTo !== sails.models.meta.memory.cohostDJ2 &&
        this.req.payload.belongsTo !== sails.models.meta.memory.cohostDJ3
      )
        throw "forbidden";

      // Transmit dump event through sockets; DJ Controls will manage from there.
      sails.sockets.broadcast("delay-system-dump", "delay-system-dump", null);

      // Note that we dumped
      sails.models.status.lastDump = moment();
      
      return exits.success();
    } catch (e) {
      return exits.error(e);
    }
  }
};
