var goingDown = false;
var isSame = false;

module.exports = {
  friendlyName: "delay / status",

  description:
    "The host responsible for the delay system should hit this endpoint every 15 seconds, as well as immediately after calling delay/dump, to specify current delay system status and info.",

  inputs: {
    seconds: {
      type: "number",
      description:
        "Specify how many seconds of delay the delay system is reporting.",
    },
    bypass: {
      type: "boolean",
      description:
        "Specify true if the bypass function on the delay system is activated.",
    },
  },

  exits: {},

  fn: async function (inputs, exits) {
    sails.log.debug("Controller delay/status called.");

    let delayDumpTimer;

    try {
      isSame =
        sails.models.meta.memory.delaySystem ===
        (inputs.bypass ? null : inputs.seconds);

      // Check if the dump or stop button was potentially pressed. If it was, log it.
      if (
        !inputs.bypass &&
        sails.models.meta.memory.delaySystem !== null &&
        inputs.seconds < sails.models.meta.memory.delaySystem
      ) {
        if (!goingDown) {
          // Note that we dumped
          sails.models.status.lastDump = moment();

          // Log it
          await sails.models.logs
            .create({
              attendanceID: sails.models.meta.memory.attendanceID,
              logtype: "delay-dump",
              loglevel: "warning",
              logsubtype: ``,
              logIcon: `fas fa-eject`,
              title: `Delay system dump or stop button pressed.`,
              event: `The delay system's dump or stop button might have been pressed. You may wish to review the broadcast recordings to ensure no inappropriate content went on the air.<br />${sails.models.meta.memory.line1}<br />${sails.models.meta.memory.line2}`,
              createdAt: moment().toISOString(true),
            })
            .fetch()
            .tolerate((err) => {
              sails.log.error(err);
            });
        }
        goingDown = true;
      } else {
        goingDown = false;
      }

      // Delay system is in bypass mode. This is bad!
      if (inputs.bypass) {
        // Critical status
        await sails.helpers.status.change.with({
          name: "delay-system",
          label: "Delay System",
          data: `Delay system is in bypass mode and not actively delaying! This is against FCC regulations. Please disable bypass by pressing the bypass button on the delay system.<br /><strong>Remote broadcasts will not be allowed</strong> until this issue is resolved because it is impossible to remotely dump.`,
          status: 1,
        });

        // API Note: We will NOT automatically try to get the delay system out of bypass mode; it may have been intentionally activated.

        // If we are just now entering bypass, log it
        if (sails.models.meta.memory.delaySystem !== null) {
          await sails.models.logs
            .create({
              attendanceID: sails.models.meta.memory.attendanceID,
              logtype: "delay-bypass",
              loglevel: "orange",
              logsubtype: ``,
              logIcon: `fas fa-eject`,
              title: `Delay system entered bypass mode.`,
              event: `The delay system entered bypass mode, probably because someone pressed the bypass button. <strong>Please disable bypass mode ASAP when/if not needed.</strong>`,
              createdAt: moment().toISOString(true),
            })
            .fetch()
            .tolerate((err) => {
              sails.log.error(err);
            });
        }
        await sails.helpers.meta.change.with({ delaySystem: null });

        // Delay is reporting 0 seconds
      } else if (inputs.seconds <= 0) {
        // Check to see if we are stuck on 0 seconds (was 0 seconds on last status, AND it was more than 30 seconds since dump was pressed)
        // If so, critical status! If not, just inform we are on 0 right now.
        if (
          sails.models.meta.memory.delaySystem <= 0 &&
          moment()
            .subtract(30, "seconds")
            .isAfter(moment(sails.models.status.lastDump))
        ) {
          if (sails.models.meta.memory.delaySystem !== null) {
            await sails.helpers.status.change.with({
              name: "delay-system",
              label: "Delay System",
              data: `Delay system is returning 0 seconds of delay and does not appear to be re-building. This is against FCC regulations (requirement is 7 seconds or more). Please ensure the delay system is activated. You may have to press the start button. The system will repeately attempt to recover the delay system automatically.<br /><strong>Remote broadcasts will not be allowed</strong> until this issue is resolved because it is impossible to remotely dump.`,
              status: 1,
            });

            // API note: We will attempt to recover the delay system. Sometimes, "start" may not have been transmitted after a dump. And if someone wanted to turn the delay off, they should use bypass instead of setting it to 0 seconds.

            // Transmit dump event through sockets in an attempt to reset the delay system and get it working again; DJ Controls will manage from there.
            delayDumpTimer = setTimeout(() => {
              sails.sockets.broadcast(
                "delay-system-dump",
                "delay-system-dump",
                null
              );
            }, 30000);
          }
        } else {
          await sails.helpers.status.change.with({
            name: "delay-system",
            label: "Delay System",
            data: `Delay system is returning 0 seconds of delay, however the cough / dump button might have been pressed recently. If the delay system does not start re-building in the next several seconds, this status will turn critical.`,
            status: 4,
          });
        }
        await sails.helpers.meta.change.with({ delaySystem: 0 });

        // FCC requires 7 seconds of delay at minimum; so in this case, delay is returning more than 0 but less than 7 seconds of delay.
      } else if (inputs.seconds < 7) {
        clearTimeout(delayDumpTimer);

        // Delay does not seem to be re-building. Trigger a critical status!
        if (
          isSame &&
          moment()
            .subtract(30, "seconds")
            .isAfter(moment(sails.models.status.lastDump))
        ) {
          await sails.helpers.status.change.with({
            name: "delay-system",
            label: "Delay System",
            data: `Delay system is returning ${inputs.seconds} seconds of delay and does not appear to be re-building further. This is against FCC regulations (requirement is 7 seconds or more). Please ensure the delay system is set at a delay of at least 7 seconds. <strong>Remote broadcasts will not be allowed</strong> until this issue is resolved.`,
            status: 1,
          });

          // API note: We will NOT try to automatically recover the delay system; if the delay is above 0 but below 7 seconds, it must be re-configured manually.
        } else {
          await sails.helpers.status.change.with({
            name: "delay-system",
            label: "Delay System",
            data: `Delay system is returning ${inputs.seconds} seconds of delay, however it might still be re-building from a push of the cough / dump button.`,
            status: 4,
          });
        }
        await sails.helpers.meta.change.with({ delaySystem: inputs.seconds });

        // 7 seconds or more? We are within FCC regulations. Change to good status.
      } else {
        clearTimeout(delayDumpTimer);
        
        await sails.helpers.status.change.with({
          name: "delay-system",
          label: "Delay System",
          data: `Delay System is reporting ${inputs.seconds} seconds of delay. This is within FCC limits (7 seconds or more).`,
          status: 5,
        });
        await sails.helpers.meta.change.with({ delaySystem: inputs.seconds });
      }

      return exits.success();
    } catch (e) {
      return exits.error(e);
    }
  },
};
