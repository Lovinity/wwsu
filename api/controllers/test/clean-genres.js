module.exports = {
  friendlyName: "Clean genres",

  description:
    "Clean out duplicate empty genres, and report track counts for each genre.",

  inputs: {},

  exits: {},

  fn: async function (inputs) {
    let genreNames = [];
    let duplicates = [];
    let buffer;
    let toDelete = [];
    let returnData = {};

    let genres = await sails.models.genre.find().sort("name ASC");

    // First, determine our duplicates
    genres.forEach((genre) => {
      if (genreNames.indexOf(genre.name) === -1) {
        genreNames.push(genre.name);
        buffer = genre.ID;
      } else {
        duplicates.push(genre.ID);
        if (buffer) duplicates.push(buffer);
        buffer = undefined;
      }
    });

    // Next, determine our song counts and duplicates to delete
    let maps = genres.map(async (genre) => {
      let countTracks = await sails.models.songs.count({ id_genre: genre.ID });
      if (countTracks === 0 && duplicates.indexOf(genre.ID) !== -1) {
        toDelete.push(genre.ID);
        returnData[`*${genre.name} (${genre.ID})`] = "DELETED (empty duplicate)";
      } else if (duplicates.indexOf(genre.ID) !== -1) {
        returnData[`*${genre.name} (${genre.ID})`] = countTracks;
      } else {
        returnData[`${genre.name} (${genre.ID})`] = countTracks;
      }
    });
    await Promise.all(maps);

    // Now, delete our empty duplicate genres
    await sails.models.genre.destroy({ ID: toDelete });

    return returnData;
  },
};
