module.exports = {
  friendlyName: "Convert times",

  description: "Stuff",

  inputs: {},

  exits: {},

  fn: async function (inputs) {
    let records = await sails.models.schedule.find();

    let maps = records.map(async (record) => {
      if (record.startTime)
        record.recurrenceInterval = { measure: "weeks", unit: 1 };
      if (record.oneTime && record.oneTime.length > 0)
        record.startTime = moment(record.oneTime[0]).format("HH:mm");
      record.oneTime =
        record.oneTime && record.oneTime.length > 0
          ? record.oneTime.map((oneTime) =>
              moment(oneTime).format("YYYY-MM-DD")
            )
          : null;

      sails.log.verbose(record);

      await sails.models.schedule.updateOne(
        { ID: record.ID },
        {
          startTime: record.startTime,
          recurrenceInterval: record.recurrenceInterval,
          oneTime: record.oneTime,
        }
      );
    });

    await Promise.all(maps);
  },
};
