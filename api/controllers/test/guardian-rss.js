module.exports = {
  friendlyName: "Guardian rss",

  description: "",

  inputs: {},

  exits: {},

  fn: async function (inputs) {
    let resp = needle("get", `http://wsuguardian.com/wprss`, {});
    if (
      resp &&
      resp.body &&
      resp.body.children &&
      resp.body.children.length > 0
    ) {
      resp.body.children.forEach((child) => {
        if (
          child.name !== "entry" ||
          !child.children ||
          child.children.length === 0
        )
          return;

        let returnData = { source: "wsuguardian" };
        child.children.forEach((child2) => {
          switch (child2.name) {
            case "id":
              returnData.entryID = child2.value;
              break;
            case "title":
              returnData.title = child2.value;
              break;
            case "updated":
              returnData.date = child2.value;
              break;
            case "summary":
              returnData.summary = child2.value;
              break;
            case "author":
              if (child2.children && child2.children.length > 0) {
                returnData.author = [];
                child2.children.forEach((child3) => {
                  if (child3.name !== "name") return;
                  returnData.author.push(child3.value);
                });
                returnData.author = returnData.author.join(", ");
              }
              break;
          }
        });

        let criteria = _.cloneDeep(returnData);
        sails.models.rss
          .findOrCreate({ entryID: returnData.entryID }, criteria)
          .exec((err, record, wasCreated) => {
            if (!err && !wasCreated) {
              let updateIt = false;
              for (let key in returnData) {
                if (
                  key === "date" &&
                  !moment(record[key]).isSame(moment(returnData[key]))
                ) {
                  updateIt = true;
                  break;
                }
                if (
                  (key !== "date" && typeof record[key] === "undefined") ||
                  record[key] !== returnData[key]
                ) {
                  updateIt = true;
                  break;
                }
              }
              if (updateIt)
                sails.models.rss
                  .updateOne({ entryID: returnData.entryID }, criteria)
                  .exec(() => {});
            }
          });
      });
    }
  },
};
