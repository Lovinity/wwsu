module.exports = {

    friendlyName: 'recalculate showtime',

    description: 'recalculate showtime test.',

    inputs: {
        ID: {
            type: "number",
            required: true
        }
    },

    fn: async function (inputs, exits) {
        let results = await sails.helpers.attendance.recalculate(inputs.ID);
        return exits.success(results);
    }

}