module.exports = {
  friendlyName: "call/",

  description: "Call us page",

  inputs: {},

  exits: {
    success: {
      responseType: "view",
      viewTemplatePath: "website/call",
    },
  },

  fn: async function (inputs) {
    // All done.
    return {
      layout: "website/layout",
      meta: {
        description: `Learn how to get in touch with WWSU on the phone.`,
        author: "WWSU 106.9 FM",
        "og:title": `Call the station - WWSU 106.9 FM`,
        "og:url": `${sails.config.custom.baseUrl}/call`,
        "og:type": "website",
        "og:description": `Learn how to get in touch with WWSU on the phone.`,
        "og:site_name": "WWSU 106.9 FM",
      },
    };
  },
};
