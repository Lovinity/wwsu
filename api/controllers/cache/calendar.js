module.exports = {
  friendlyName: "Cache / Calendar",

  description:
    "Retrieve pre-generated calendar events from calendardb.getEvents for the next 14 days, and subscribe to socket updates.",

  inputs: {},

  exits: {},

  fn: async function (inputs) {
    // Get calendar cache records; strip everything but data
    let records = await sails.models.cache.find({ model: "calendar" });
    records = records.map((record) => record.data);

    // Subscribe to sockets if applicable
    if (this.req.isSocket) {
      sails.sockets.join(this.req, "cache-calendar");
      sails.log.verbose("Request was a socket. Joining cache-calendar.");
    }

    // All done.
    return records;
  },
};
