module.exports = {
  friendlyName: "Cache / Climacell",

  description:
    "Fetch cached climacell information and subscribe to socket updates.",

  inputs: {},

  exits: {},

  fn: async function (inputs) {
    // Get cache records; strip everything but data
    let records = await sails.models.cache.find({ model: "climacell" });
    records = records.map((record) => record.data);

    // Subscribe to sockets if applicable
    if (this.req.isSocket) {
      sails.sockets.join(this.req, "cache-climacell");
      sails.log.verbose("Request was a socket. Joining cache-climacell.");
    }

    // All done.
    return records;
  },
};
