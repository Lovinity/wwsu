module.exports = {
  friendlyName: "Clock dj",

  description: "Log an entry for a DJ using a locked down system",

  inputs: {
    host: {
      type: "number",
      required: true,
      description: "The ID of the host from which this log comes.",
    },
  },

  exits: {},

  fn: async function (inputs) {
    // Integrity check for existing host that is locked down
    let host = await sails.models.hosts.findOne({
      ID: inputs.host,
      lockDown: { "!=": null },
    });
    if (!host)
      throw new Error(
        "The provided host either does not exist or is not locked down."
      );
    if (host.lockDown === "director")
      throw new Error("DJs cannot log in to this host");

    // Find existing records
    let records = await sails.models.lockdown.find({
      host: inputs.host,
      clockOut: null,
    });

    // error if there is already a clock-in right now on this host
    if (records.length > 0) {
      throw new Error("Someone is already logged in on that host.");
    }

    // Clock in record
    await sails.models.lockdown
      .create({
        host: inputs.host,
        type: "DJ",
        typeID: this.req.payload.ID,
        clockIn: moment().toISOString(true),
        clockOut: null,
      })
      .fetch();

    // Update lastSeen of the DJ
    await sails.models.djs.updateOne(
      {
        ID: this.req.payload.ID,
      },
      { lastSeen: moment().toISOString(true) }
    );

    return;
  },
};
