module.exports = {
  friendlyName: "lockdown / Clock out",

  description: "Log out of a locked down host",

  inputs: {
    host: {
      type: "number",
      required: true,
      description: "The ID of the host from which this log comes.",
    },
  },

  exits: {},

  fn: async function (inputs) {
    // Integrity check for existing host. Do not check if locked down because we might be checking out after a previously lockdown host is no longer lockdown.
    let host = await sails.models.hosts.findOne({
      ID: inputs.host,
    });
    if (!host)
      throw new Error(
        "The provided host does not exist"
      );

    await sails.models.lockdown
      .update(
        {
          host: inputs.host,
          clockOut: null,
        },
        { clockOut: moment().toISOString(true) }
      )
      .fetch();

    // All done.
    return;
  },
};
