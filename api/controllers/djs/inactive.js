module.exports = {
  friendlyName: "djs / inactive",

  description: "Mark a DJ as inactive in the system.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The DJ ID to mark inactive.",
    },
  },

  exits: {},

  fn: async function (inputs, exits) {
    sails.log.debug("Controller djs/inactive called.");

    try {
      // Do not allow marking master member inactive
      if (inputs.ID === 1)
        return new Error("Not allowed to mark master member as inactive.");

      // Mark DJ as inactive.
      await sails.models.djs.updateOne({ ID: inputs.ID }, { active: false });

      return exits.success();
    } catch (e) {
      return exits.error(e);
    }
  },
};
