const jwt = require("jsonwebtoken");

module.exports = {
  friendlyName: "Get door code",

  description:
    "Get the door code of a DJ (encoded using the authorization token)",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: `The DJ to get the door code`,
    },
  },

  exits: {},

  fn: async function (inputs) {
    // Grab the DJ and decrypt the door code
    let dj = await sails.models.djs.findOne({ ID: inputs.ID }).decrypt();
    if (!dj) throw new Error("The provided DJ was not found.");

    // Encode the door code using the authorization token; we do not want to send it in plain text back to the client. We expect the client to decode using the token they authorized as the secret.
    return jwt.sign(
      {
        doorCode: dj.doorCode,
        exp: Math.floor(Date.now() / 1000) + 60 * 10,
      },
      "session-attached" // TODO: change this
    );
  },
};
