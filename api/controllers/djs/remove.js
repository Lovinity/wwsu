module.exports = {
  friendlyName: "djs / remove",

  description: "Permanently remove a DJ from the system.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The DJ ID to remove.",
    },
  },

  exits: {},

  fn: async function (inputs, exits) {
    sails.log.debug("Controller djs/remove called.");

    try {
      // Do not allow removing of master member
      if (inputs.ID === 1)
        return new Error("Not allowed to remove the master member");

      // Remove DJ.
      await sails.models.djs.destroyOne({ ID: inputs.ID });

      return exits.success();
    } catch (e) {
      return exits.error(e);
    }
  },
};
