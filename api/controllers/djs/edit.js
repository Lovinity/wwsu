const bcrypt = require("bcrypt");

module.exports = {
  friendlyName: "djs / edit",

  description:
    "Change the name or login of a DJ. If a DJ with the same name already exists, the two DJs will be merged.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID of the dj to edit.",
    },
    name: {
      type: "string",
      custom: function (value) {
        // Prevent use of space dash space, or "; ", in names as this will cause problems in the system
        var temp2 = value.split(" - ");
        if (temp2.length > 1) {
          return false;
        }
        var temp3 = value.split("; ");
        if (temp3.length > 1) {
          return false;
        }
        return true;
      },
      description: "The new name for the DJ.",
    },
    realName: {
      type: "string",
      description: "The full real name of the DJ.",
    },
    email: {
      type: "string",
      description: "The email address of the DJ.",
    },
    login: {
      type: "string",
      description: "The new login for the DJ.",
    },
    avatar: {
      type: "number",
      allowNull: true,
      description: "The ID of the Uploads file for the dj's avatar",
    },
    profile: {
      type: "string",
      description: "New profile information for the DJ.",
    },
    permissions: {
      type: "json",
      description: "The permissions granted to this DJ",
      custom: (value) => {
        if (value.constructor !== Array) return false;

        for (let key in value) {
          if (
            ["org", "sportsbroadcaster", "book", "live", "remote", "sports"].indexOf(value[key]) ===
            -1
          ) {
            return false;
          }
        }

        return true;
      },
    },

    doorCode: {
      type: "string",
      description: "The door code for the DJ",
    },

    doorCodeAccess: {
      type: "json",
      description: "The doors which the door code for this DJ grants access",
      custom: (value) => {
        if (value.constructor !== Array) return false;

        for (let key in value) {
          if (
            ["018", "018A", "018B", "018C", "018D"].indexOf(value[key]) === -1
          ) {
            return false;
          }
        }

        return true;
      },
    },
  },

  exits: {},

  fn: async function (inputs, exits) {
    sails.log.debug("Controller djs/edit called.");

    try {
      // Get original record before updating
      let dj = await sails.models.djs.findOne({ ID: inputs.ID });

      // Determine what needs updating
      let criteria = {};

      if (inputs.name && inputs.name !== "") {
        criteria.name = inputs.name;

        // If a name is provided, ensure this name is not already taken.
        if (dj && dj.name !== inputs.name) {
          let dj2 = await sails.models.djs.find({ name: inputs.name });
          if (dj2.length > 0) {
            return exits.error(new Error("A DJ with this name already exists"));
          }
        }
      }

      if (inputs.realName) criteria.realName = inputs.realName;

      if (inputs.email && inputs.email !== null) criteria.email = inputs.email;

      // Encrypt login
      if (inputs.login !== null && typeof inputs.login !== "undefined") {
        criteria.login = bcrypt.hashSync(inputs.login, 10);
      }

      if (inputs.avatar !== null && typeof inputs.avatar !== "undefined") {
        criteria.avatar = inputs.avatar;
      }
      if (inputs.profile) {
        if (inputs.profile === "null") {
          criteria.profile = null;
        } else {
          criteria.profile = await sails.helpers.sanitize(inputs.profile);
        }
      }
      if (inputs.permissions) criteria.permissions = inputs.permissions;
      if (inputs.doorCode) criteria.doorCode = inputs.doorCode;
      if (inputs.doorCodeAccess)
        criteria.doorCodeAccess = inputs.doorCodeAccess;

      // We must clone the InitialValues object due to how Sails.js manipulates any objects passed as InitialValues.
      let criteriaB = _.cloneDeep(criteria);

      // Edit it
      await sails.models.djs.updateOne({ ID: inputs.ID }, criteriaB);

      // Also edit directors with the same original name to the new name, if applicable
      if (dj && inputs.realName) {
        await sails.models.directors
          .update({ name: dj.realName }, { name: inputs.realName })
          .fetch();
      }

      return exits.success();
    } catch (e) {
      return exits.error(e);
    }
  },
};
