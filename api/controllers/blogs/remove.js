module.exports = {


  friendlyName: 'blogs/remove',


  description: 'Remove blog post.',


  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "ID of the blog entry to remove",
    },
  },


  exits: {

  },


  fn: async function (inputs) {
    let record = await sails.models.blogs.destroyOne({ID: inputs.ID});
    if (!record) throw "notFound";

    return;
  }


};
