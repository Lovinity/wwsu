module.exports = {
  friendlyName: "blogs/add",

  description: "Add a blog post.",

  inputs: {
    active: {
      type: "boolean",
      defaultsTo: true,
      description:
        "If false, this post will not be displayed regardless of starts, expires, and needsApproved settings.",
    },

    needsApproved: {
      type: "boolean",
      defaultsTo: false,
      description:
        "Does this blog post need approval from directors before it is displayed?",
    },

    categories: {
      type: "json",
      required: true,
      description: "Which categories (blogs) does this post belong?",
      custom: (value) => {
        // Must be array
        if (value.constructor !== Array) return false;

        // Must be at least 1 item long
        if (value.length < 1) return false;

        // Every item in array must be a string
        let badRecord = value.find((item) => typeof item !== "string");
        if (badRecord) return false;

        return true;
      },
    },

    tags: {
      type: "json",
      description:
        "List of keywords for this post which can easily be searched.",
      custom: (value) => {
        // Must be array
        if (value.constructor !== Array) return false;

        // Every item in array must be a string
        let badRecord = value.find((item) => typeof item !== "string");
        if (badRecord) return false;

        return true;
      },
    },

    title: {
      type: "string",
      required: true,
      maxLength: 64,
      description: "Post title / headline",
    },

    author: {
      type: "number",
      required: true,
      description: "DJ ID of the author",
    },

    featuredImage: {
      type: "number",
      allowNull: true,
      description: "Uploads ID of the blog post featured image",
    },

    summary: {
      type: "string",
      required: true,
      maxLength: 255,
      description: "An excerpt of the post",
    },

    contents: {
      type: "string",
      required: true,
      description: "The post content itself (HTML format).",
    },

    starts: {
      type: "ref",
      custom: function (value) {
        return moment(value).isValid();
      },
      description:
        "If provided, post will not be visible until this date/time (providing it is active and does not need approved).",
    },

    expires: {
      type: "ref",
      custom: function (value) {
        return moment(value).isValid();
      },
      description: "If provided, post will disappear at this date/time.",
    },
  },

  exits: {},

  fn: async function (inputs) {
    // Sanitize
    inputs.title = await sails.helpers.sanitize(inputs.title);
    inputs.summary = await sails.helpers.sanitize(inputs.summary);
    inputs.contents = await sails.helpers.sanitize(inputs.contents);

    // Create the record
    await sails.models.blogs
      .create({
        active: inputs.active,
        needsApproved: inputs.needsApproved,
        categories: inputs.categories,
        tags: inputs.tags,
        title: inputs.title,
        author: inputs.author,
        featuredImage: inputs.featuredImage,
        summary: inputs.summary,
        contents: inputs.contents,
        starts: inputs.starts,
        expires: inputs.expires,
      })
      .fetch();
  },
};
