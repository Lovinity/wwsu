module.exports = {
  friendlyName: "blogs/get",

  description:
    "Get blog entries and subscribe to sockets. Ignore active / needsApproved / expiry.",

  inputs: {},

  exits: {},

  fn: async function (inputs) {
    // Subscribe to websockets if applicable
    if (this.req.isSocket) {
      sails.sockets.join(this.req, "blogs");
      sails.log.verbose("Request was a socket. Joined blogs.");
    }

    let records = await sails.models.blogs.find();

    // Only include basic information; other info should be returned in blogs/get-one.
    records = records.map((record) => {
      return {
        ID: record.ID,
        createdAt: record.createdAt,
        updatedAt: record.updatedAt,
        active: record.active,
        needsApproved: record.needsApproved,
        categories: record.categories,
        title: record.title,
        starts: record.starts,
        expires: record.expires,
      };
    });

    return records;
  },
};
