module.exports = {
  friendlyName: "blogs/get-one",

  description:
    "Get a single blog entry; ignore active / needsApproved / expiry.",

  inputs: {
    ID: {
      type: "number",
      required: true,
    },
  },

  exits: {},

  fn: async function (inputs) {
    let record = await sails.models.blogs.findOne({ ID: inputs.ID });
    if (!record) throw new Error("Blog not found");

    return record;
  },
};
