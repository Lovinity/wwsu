module.exports = {
  friendlyName: "EAS / Get",

  description: "Get the currently active EAS alerts.",

  inputs: {
    jsonp: {
      type: "string",
      description:
        "If using a cross-site origin request (JSONP) from The Guardian Media Group, specify the callback name here.",
    },
  },

  fn: async function (inputs) {
    sails.log.debug("Controller eas/get called.");

    // Get records
    let records = await sails.models.eas.find();
    sails.log.verbose(`Retrieved Eas records: ${records.length}`);
    sails.log.silly(records);

    // Subscribe to sockets, if applicable
    if (this.req.isSocket) {
      sails.sockets.join(this.req, "eas");
      sails.log.verbose("Request was a socket. Joining eas.");
    }

    if (inputs.jsonp) {
      return `${inputs.jsonp}(${JSON.stringify(records)});`;
    }

    return records;
  },
};
