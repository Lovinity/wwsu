module.exports = {
  friendlyName: "EAS / Test",

  description: "Issue a test in the WWSU Emergency Alert System.",

  inputs: {},

  fn: async function (inputs, exits) {
    try {
      sails.log.debug("Controller eas/test called.");

      // Add test alerts
      var value = moment().valueOf();
      await sails.helpers.eas.addAlert(
        value,
        "WWSU 106.9 FM",
        "WWSU listening area at Wright State University Dayton campus",
        "Test",
        "Severe",
        moment().toISOString(true),
        moment().add(3, "minutes").toISOString(true),
        "#FFFFFF",
        "This is a test of the WWSU Emergency Alert System. This is only a test. The WWSU Emergency Alert System is designed to provide the public with timely emergency information, Wright State Campus Alerts, and National Weather Service alerts for Clark, Greene, and Montgomery counties of Ohio. Had this been an actual emergency, you would have received information and instructions regarding the emergency. This concludes the test of the WWSU Emergency Alert System.",
        true
      );

      // Post EAS tasks (what actually pushes the new alert)
      await sails.helpers.eas.postParse();

      return exits.success();
    } catch (e) {
      return exits.error(e);
    }
  },
};
