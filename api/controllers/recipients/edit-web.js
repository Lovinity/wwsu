var sh = require("shorthash");

module.exports = {
  friendlyName: "Recipients / Edit-web",

  description: "Changes a label for a public recipient.",

  inputs: {
    label: {
      type: "string",
      description: "The new label or nickname for this recipient.",
    },
    device: {
      type: "string",
      description: "The new device Id for this user",
    },
  },

  fn: async function (inputs, exits) {
    sails.log.debug("Controller recipients/edit-web called.");

    try {
      // Request must be a socket
      if (!this.req.isSocket) {
        return exits.error(new Error("This controller requires a websocket."));
      }

      // Get the recipient host
      var fromIP = await sails.helpers.getIp(this.req);
      var host = this.req.session
        ? sh.unique(this.req.session.id)
        : sh.unique(fromIP + sails.config.custom.hostSecret);

      if (inputs.label) {
        // Filter disallowed HTML
        inputs.label = await sails.helpers.sanitize(inputs.label);

        // Do not allow profane labels
        inputs.label = await sails.helpers.filterProfane(inputs.label);

        // Truncate after 64 characters
        inputs.label = await sails.helpers.truncateText(inputs.label, 64);
      }

      let criteria = {
        label: inputs.label ? `Web (${inputs.label})` : undefined,
        device: inputs.device,
      };

      let criteriaB = _.cloneDeep(criteria);

      // Update the recipient
      var records = await sails.models.recipients
        .update({ host: `website-${host}` }, criteriaB)
        .fetch();
      sails.log.verbose(`Updated ${records.length} records of host ${host}.`);

      return exits.success();
    } catch (e) {
      return exits.error(e);
    }
  },
};
