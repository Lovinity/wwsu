module.exports = {
  friendlyName: "State / Remote",

  description: "Request to begin a remote broadcast.",

  inputs: {
    topic: {
      type: "string",
      defaultsTo: "",
      description:
        "A string containing a short blurb about this remote broadcast.",
    },

    // TODO: Deprecate this and change to DJ / show name fields in v8
    showname: {
      type: "string",
      required: true,
      custom: function (value) {
        var temp2 = value.split(" - ");
        if (temp2.length !== 2) {
          return false;
        }
        var temp3 = temp2[0].split("; ");
        if (temp3.length > 4) {
          return false;
        }
        return true;
      },
      description:
        'Name of the broadcast beginning. It must follow the format "DJ names/handles (each separated with "; ", maximum 4) - show name".',
    },

    webchat: {
      type: "boolean",
      defaultsTo: true,
      description:
        "Should the web chat be enabled during this show? Defaults to true.",
    },
  },

  fn: async function (inputs, exits) {
    sails.log.debug("Controller state/remote called.");
    sails.log.silly(`Parameters passed: ${JSON.stringify(inputs)}`);

    try {
      // Do not continue if not in automation mode; client should request automation before requesting remote
      if (
        !sails.models.meta.memory.state.startsWith("automation_") &&
        !sails.models.meta.memory.state.startsWith("prerecord_")
      ) {
        throw "forbidden";
      }

      // Block this request if we are changing states right now
      if (sails.models.meta.memory.changingState !== null) {
        return exits.error(
          new Error(
            `The system is in the process of changing states. The request was blocked to prevent clashes.`
          )
        );
      }

      // Block if running an alert
      if (sails.models.meta.memory.altRadioDJ !== null) {
        return exits.error(
          new Error(`Cannot change states while an alert is being broadcast.`)
        );
      }

      // Disallow starting a remote broadcast if not allowed either via host or via org member

      // Disallow starting a remote broadcast if the host has belongsTo and the provided DJ either does not match belongsTo or is not scheduled to start a remote broadcast
      if (
        !(await sails.helpers.canStartRemoteBroadcast(
          this.req.payload.ID,
          "remote"
        ))
      ) {
        throw new Error(
          "This host is not allowed to start a remote broadcast."
        );
      }

      if (this.req.payload.belongsTo) {
        // We need an additional check to ensure if belongsTo is specified, there is a scheduled remote broadcast where that member is one of the hosts.
        let dj = inputs.showname.split(" - ");
        let show = dj[1];
        var record = sails.models.calendar.calendardb.whatShouldBePlaying(
          null,
          false
        );
        record = record.filter(
          (event) =>
            event.type === "remote" &&
            event.name === show &&
            [
              event.hostDJ,
              event.cohostDJ1,
              event.cohostDJ2,
              event.cohostDJ3,
            ].indexOf(this.req.payload.belongsTo) !== -1
        );
        if (record.length < 1) {
          throw new Error(
            "This host belongs to a member who does not have a remote broadcast currently scheduled. Starting a remote broadcast is not allowed."
          );
        }

        // We also need to make sure at least one of the specified DJs for show hosts belongsTo this host
        let djs = dj[0].split("; ");
        let djRecord = await sails.models.djs.findOne({
          ID: this.req.payload.belongsTo,
        });
        if (!djRecord || djs.indexOf(djRecord.name) === -1) {
          throw new Error(
            "None of the specified broadcast hosts are allowed to start a remote broadcast from this computer."
          );
        }
      }

      // Do not allow starting a remote broadcast if delay system status is 1 (critical).
      let delayStatus = await sails.models.status.find({
        name: "delay-system",
      });
      if (delayStatus && delayStatus[0] && delayStatus[0].status === 1)
        throw new Error(
          "Delay System is not operational. Remote broadcasts cannot be started at this time."
        );

      // Lock so that other state changing requests get blocked until we are done.
      await sails.helpers.meta.change.with({
        changingState: `Switching to remote`,
      });

      // Filter profanity and sanitize
      if (inputs.topic !== "") {
        inputs.topic = await sails.helpers.filterProfane(inputs.topic);
        inputs.topic = await sails.helpers.sanitize(inputs.topic);
        inputs.topic = await sails.helpers.truncateText(inputs.topic, 256);
      }
      if (inputs.showname !== "") {
        inputs.showname = await sails.helpers.filterProfane(inputs.showname);
        inputs.showname = await sails.helpers.sanitize(inputs.showname);
      }

      // Send meta to prevent accidental interfering messages in Dj Controls
      await sails.helpers.meta.change.with({
        state: "automation_remote",
        host: this.req.payload.ID,
        show: inputs.showname,
        topic: inputs.topic,
        trackStamp: null,
      });

      // await sails.helpers.error.count('goRemote');

      // Operation: Remove all music tracks, queue clockwheel break, and disable auto DJ.
      await sails.helpers.rest.cmd("EnableAutoDJ", 0);
      await sails.helpers.songs.remove(
        true,
        sails.config.custom.subcats.noClearShow,
        false,
        true
      );
      await sails.helpers.rest.cmd("EnableAssisted", 1);
      await sails.helpers.break.checkClockwheel(false);
      await sails.helpers.break.executeArray(
        sails.config.custom.specialBreaks.remote.start,
        "Remote Start"
      );

      // Queue a show opener if there is one
      if (
        typeof sails.config.custom.showcats[inputs.showname.split(" - ")[1]] !==
        "undefined"
      ) {
        await sails.helpers.songs.queue(
          [
            sails.config.custom.showcats[inputs.showname.split(" - ")[1]][
              "Show Openers"
            ],
          ],
          "Bottom",
          1
        );
      } else {
        await sails.helpers.songs.queue(
          [sails.config.custom.showcats["Default"]["Show Openers"]],
          "Bottom",
          1
        );
      }

      await sails.helpers.rest.cmd("EnableAssisted", 0);

      var queueLength = await sails.helpers.songs.calculateQueueLength();

      // If radioDJ queue is unacceptably long, try to make it shorter.
      if (queueLength >= sails.config.custom.queueCorrection.remote) {
        await sails.helpers.rest.cmd("EnableAutoDJ", 0); // Try to Disable autoDJ again in case it was mistakenly still active
        // await sails.helpers.songs.remove(true, sails.config.custom.subcats.noClearShow, false, false);

        // Add a PSA to give the DJ a little more time
        await sails.helpers.songs.queue(
          sails.config.custom.subcats.PSAs,
          "Top",
          1
        );

        if (
          sails.config.custom.subcats.noClearShow &&
          sails.config.custom.subcats.noClearShow.indexOf(
            sails.models.meta.memory.trackIDSubcat
          ) === -1
        ) {
          await sails.helpers.rest.cmd("PlayPlaylistTrack", 0);
        } // Skip currently playing track if it is not a noClearShow track

        queueLength = await sails.helpers.songs.calculateQueueLength();
      }

      // Add a station ID
      await sails.helpers.songs.queue(
        sails.config.custom.subcats.IDs,
        "Top",
        1
      );

      await sails.helpers.meta.changeDjs(inputs.showname);
      await sails.helpers.meta.change.with({
        queueFinish: moment().add(queueLength, "seconds").toISOString(true),
        show: inputs.showname,
        topic: inputs.topic,
        trackStamp: null,
        webchat: inputs.webchat,
      });

      await sails.helpers.meta.change.with({ changingState: null });
      return exits.success();
    } catch (e) {
      await sails.helpers.meta.change.with({
        state: "automation_on",
        host: null,
        show: "",
        topic: "",
        trackStamp: null,
        changingState: null,
      });
      return exits.error(e);
    }
  },
};
