module.exports = {
  friendlyName: "State / Sports-remote",

  description: "Request to begin a remote sports broadcast.",

  inputs: {
    topic: {
      type: "string",
      defaultsTo: "",
      description:
        "A string containing a short blurb about this sports broadcast.",
    },

    sport: {
      type: "string",
      required: true,
      isIn: sails.config.custom.sports,
      description: "Name of the sport that is being broadcast.",
    },

    webchat: {
      type: "boolean",
      defaultsTo: true,
      description:
        "Should the web chat be enabled during this broadcast? Defaults to true.",
    },
  },

  fn: async function (inputs, exits) {
    sails.log.debug("Controller state/sports-remote called.");

    try {
      // Do not continue if not in automation mode; client should request automation before requesting sports
      if (
        !sails.models.meta.memory.state.startsWith("automation_") &&
        !sails.models.meta.memory.state.startsWith("prerecord_")
      ) {
        throw "forbidden";
      }

      // Block this request if we are already switching states
      if (sails.models.meta.memory.changingState !== null) {
        return exits.error(
          new Error(
            `The system is in the process of changing states. The request was blocked to prevent clashes.`
          )
        );
      }

      // Block if running an alert
      if (sails.models.meta.memory.altRadioDJ !== null) {
        return exits.error(
          new Error(`Cannot change states while an alert is being broadcast.`)
        );
      }

      // Disallow starting a remote sports broadcast if there is no permission
      if (
        !(await sails.helpers.canStartRemoteBroadcast(
          this.req.payload.ID,
          "sports"
        ))
      ) {
        throw new Error(
          "This host is not allowed to start a remote sports broadcast."
        );
      }

      // Disallow starting a sports remote broadcast if the host has belongsTo and there is no scheduled sports broadcast at this time
      if (this.req.payload.belongsTo !== null) {
        var record = sails.models.calendar.calendardb.whatShouldBePlaying(
          null,
          false
        );
        record = record.filter(
          (event) =>
            event.type === "sports" && event.name.startsWith(inputs.sport)
        );
        if (record.length < 1) {
          throw new Error(
            "This host may not start a remote sports broadcast at this time because none are scheduled."
          );
        }
      }

      // Do not allow starting a remote broadcast if delay system status is 1 (critical).
      let delayStatus = await sails.models.status.find({
        name: "delay-system",
      });
      if (delayStatus && delayStatus[0] && delayStatus[0].status === 1)
        throw new Error(
          "Delay System is not operational. Remote broadcasts cannot be started at this time."
        );

      // Lock so that any other state changing requests are blocked until we are done
      await sails.helpers.meta.change.with({
        changingState: `Switching to sports-remote`,
      });

      // Filter profanity
      if (inputs.topic !== "") {
        inputs.topic = await sails.helpers.filterProfane(inputs.topic);
        inputs.topic = await sails.helpers.sanitize(inputs.topic);
        inputs.topic = await sails.helpers.truncateText(inputs.topic, 256);
      }

      // Set meta to prevent accidental messages in DJ Controls
      await sails.helpers.meta.change.with({
        state: "automation_sportsremote",
        host: this.req.payload.ID,
        show: inputs.sport,
        topic: inputs.topic,
        trackStamp: null,
      });

      // await sails.helpers.error.count('goLive');

      // Operation: Remove all music tracks, queue a station ID, queue an opener if one exists for this sport, and start the next track if current track is music.
      await sails.helpers.rest.cmd("EnableAutoDJ", 0);
      await sails.helpers.songs.remove(
        true,
        sails.config.custom.subcats.noClearShow,
        false,
        true
      );
      await sails.helpers.rest.cmd("EnableAssisted", 1);
      await sails.helpers.break.checkClockwheel(false);
      await sails.helpers.break.executeArray(
        sails.config.custom.specialBreaks.sports.start,
        "Sports Start"
      );

      // Queue a Sports opener if there is one
      if (typeof sails.config.custom.sportscats[inputs.sport] !== "undefined") {
        await sails.helpers.songs.queue(
          [sails.config.custom.sportscats[inputs.sport]["Sports Openers"]],
          "Bottom",
          1
        );
      }

      await sails.helpers.rest.cmd("EnableAssisted", 0);

      var queueLength = await sails.helpers.songs.calculateQueueLength();

      // If the radioDJ queue is unacceptably long, try to reduce it.
      if (queueLength >= sails.config.custom.queueCorrection.sports) {
        await sails.helpers.rest.cmd("EnableAutoDJ", 0); // Try to Disable autoDJ again in case it was mistakenly still active
        // await sails.helpers.songs.remove(true, sails.config.custom.subcats.noClearShow, false, false);
        if (
          sails.config.custom.subcats.noClearShow &&
          sails.config.custom.subcats.noClearShow.indexOf(
            sails.models.meta.memory.trackIDSubcat
          ) === -1
        ) {
          await sails.helpers.rest.cmd("PlayPlaylistTrack", 0);
        } // Skip currently playing track if it is not a noClearShow track

        queueLength = await sails.helpers.songs.calculateQueueLength();
      }

      // Add a station ID
      await sails.helpers.songs.queue(
        sails.config.custom.subcats.IDs,
        "Top",
        1
      );

      // Change meta
      await sails.helpers.meta.change.with({
        dj: null,
        cohostDJ1: null,
        cohostDJ2: null,
        cohostDJ3: null,
        queueFinish: moment().add(queueLength, "seconds").toISOString(true),
        show: inputs.sport,
        topic: inputs.topic,
        trackStamp: null,
        webchat: inputs.webchat,
      });

      await sails.helpers.meta.change.with({ changingState: null });
      return exits.success();
    } catch (e) {
      await sails.helpers.meta.change.with({
        state: "automation_on",
        host: null,
        show: "",
        topic: "",
        trackStamp: null,
        changingState: null,
      });
      return exits.error(e);
    }
  },
};
