module.exports = {
  friendlyName: "State / Automation",

  description: "Request to go into automation mode.",

  inputs: {
    transition: {
      type: "boolean",
      defaultsTo: false,
      description:
        "If true, system will go into break mode instead of automation to allow for quick transitioning between radio shows.",
    },
  },

  fn: async function (inputs, exits) {
    sails.log.debug("Controller state/automation called.");

    try {
      // Block if we are in the process of changing states
      if (sails.models.meta.memory.changingState !== null) {
        return exits.error(
          new Error(
            `The system is in the process of changing states. The request was blocked to prevent clashes.`
          )
        );
      }

      // Block if running an alert
      if (sails.models.meta.memory.altRadioDJ !== null) {
        return exits.error(
          new Error(
            `Cannot change states while an alert is being broadcast.`
          )
        );
      }

      // Prevent state changing if host is belongsTo and the specified belongsTo is not on the air
      if (
        this.req.payload.ID !== sails.models.meta.memory.host &&
        this.req.payload.belongsTo !== null &&
        this.req.payload.belongsTo !== sails.models.meta.memory.dj &&
        this.req.payload.belongsTo !== sails.models.meta.memory.cohostDJ1 &&
        this.req.payload.belongsTo !== sails.models.meta.memory.cohostDJ2 &&
        this.req.payload.belongsTo !== sails.models.meta.memory.cohostDJ3
      )
        throw "forbidden";

      // Lock system from any other state changing requests until we are done.
      await sails.helpers.meta.change.with({
        changingState: `Changing to automation`,
      });

      // Actually go to automation
      await sails.helpers.state.automation(inputs.transition);

      return exits.success();
    } catch (e) {
      await sails.helpers.meta.change.with({ changingState: null });
      return exits.error(e);
    }
  },
};
