module.exports = {
  friendlyName: "State / Break",

  description: "Go to a break.",

  inputs: {
    halftime: {
      type: "boolean",
      defaultsTo: false,
      description:
        "Halftime is true if this is an extended or halftime sports break, rather than a standard one.",
    },

    problem: {
      type: "boolean",
      defaultsTo: false,
      description:
        "If true, will play a configured technicalIssue liner as the break begins, such as if the break was triggered because of an issue. Defaults to false.",
    },
  },

  fn: async function (inputs) {
    // Prevent state changing if host is belongsTo and the specified belongsTo is not on the air.
    // EXCEPTION: We are doing a remote broadcast, the host has answerCalls or can start a broadcast, and inputs.problem = true
    if (
      this.req.payload.ID !== sails.models.meta.memory.host &&
      this.req.payload.belongsTo !== null &&
      this.req.payload.belongsTo !== sails.models.meta.memory.dj &&
      this.req.payload.belongsTo !== sails.models.meta.memory.cohostDJ1 &&
      this.req.payload.belongsTo !== sails.models.meta.memory.cohostDJ2 &&
      this.req.payload.belongsTo !== sails.models.meta.memory.cohostDJ3
    ) {
      if (
        !inputs.problem ||
        (!sails.models.meta.memory.state.startsWith("remote") &&
          !sails.models.meta.memory.state.startsWith("sportsremote")) ||
        (!(await sails.helpers.call.canStartRemoteBroadcast(
          this.req.payload.ID
        )) &&
          !this.req.payload.answerCalls)
      ) {
        throw "forbidden";
      }
    }

    await sails.helpers.state.goBreak(inputs.halftime, inputs.problem);

    return;
  },
};
