module.exports = {
  friendlyName: "calendar / get-shows",

  description:
    "Used by the WSU website to retrieve our current radio shows and schedules.",

  inputs: {},

  exits: {},

  fn: async function (inputs) {
    let returnData = [];

    let events = await sails.models.calendar
      .find({
        ID: { "!=": 1 },
        active: true,
        type: ["show", "remote", "prerecord", "sports"],
      })
      .sort("priority DESC");

    let maps = events.map(async (event) => {
      let data = {
        name: event.name || "Unknown Event",
        type: event.type,
        hosts: event.hosts || "Unknown Hosts",
        description: event.description || undefined,
        logo: event.logo
          ? `${sails.config.custom.baseUrl}/uploads/get/${event.logo}`
          : undefined,
        banner: event.banner
          ? `${sails.config.custom.baseUrl}/uploads/get/${event.banner}`
          : undefined,
        schedules: [],
        updates: [],
      };

      let schedules = await sails.models.schedule.find({
        calendarID: event.ID,
      });

      schedules.forEach((sch) => {
        switch (sch.scheduleType) {
          case "updated":
          case "updated-system":
            if (sch.newTime || sch.duration) {
              data.updates.push(
                `RE-SCHEDULED: ${moment(sch.originalTime).format(
                  "llll"
                )} re-scheduled to ${moment(
                  sch.newTime || sch.originalTime
                ).format("llll")}${
                  sch.duration
                    ? ` - ${moment(sch.newTime || sch.originalTime)
                        .add(sch.duration, "minutes")
                        .format("LT")}`
                    : ``
                }`
              );
            }
            break;
          case "canceled":
          case "canceled-system":
            data.updates.push(
              `CANCELED: ${moment(sch.originalTime).format("llll")}`
            );
            break;
          case null:
          case undefined:
            data.schedules.push(
              sails.models.calendar.calendardb.generateScheduleText(sch)
            );
            break;
        }
      });

      if (data.schedules.length > 0) returnData.push(data); // Do not include shows with no schedule
    });
    await Promise.all(maps);

    return returnData;
  },
};
