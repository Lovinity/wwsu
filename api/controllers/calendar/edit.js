module.exports = {
  friendlyName: "Calendar / Edit",

  description: "Edit a main calendar event.",

  inputs: {
    ID: {
      required: true,
      type: "number",
    },

    type: {
      type: "string",
      isIn: [
        "show",
        "sports",
        "remote",
        "prerecord",
        "genre",
        "playlist",
        "event",
        "onair-booking",
        "prod-booking",
        "office-hours",
      ],
    },

    priority: {
      type: "number",
      allowNull: true,
    },

    hostDJ: {
      type: "number",
      allowNull: true,
    },

    cohostDJ1: {
      type: "number",
      allowNull: true,
    },

    cohostDJ2: {
      type: "number",
      allowNull: true,
    },

    cohostDJ3: {
      type: "number",
      allowNull: true,
    },

    eventID: {
      type: "number",
      allowNull: true,
    },

    playlistID: {
      type: "number",
      allowNull: true,
    },

    director: {
      type: "number",
      allowNull: true,
    },

    name: {
      type: "string",
      allowNull: true,
    },

    description: {
      type: "string",
      allowNull: true,
    },

    logo: {
      type: "number",
      allowNull: true,
    },

    banner: {
      type: "number",
      allowNull: true,
    },
  },

  fn: async function (inputs) {
    sails.log.debug("Controller calendar/edit called.");
    sails.models.status.tasks.calendar++;

    try {
      // Verify the event
      var event = {
        ID: inputs.ID,
        type: inputs.type ? inputs.type : undefined,
        priority: inputs.priority ? inputs.priority : undefined,
        hostDJ: inputs.hostDJ ? inputs.hostDJ : undefined,
        cohostDJ1: inputs.cohostDJ1 ? inputs.cohostDJ1 : undefined,
        cohostDJ2: inputs.cohostDJ2 ? inputs.cohostDJ2 : undefined,
        cohostDJ3: inputs.cohostDJ3 ? inputs.cohostDJ3 : undefined,
        eventID: inputs.eventID ? inputs.eventID : undefined,
        playlistID: inputs.playlistID ? inputs.playlistID : undefined,
        director: inputs.director ? inputs.director : undefined,
        name: inputs.name ? inputs.name : undefined,
        description: inputs.description ? inputs.description : undefined,
        logo: inputs.logo ? inputs.logo : undefined,
        banner: inputs.banner ? inputs.banner : undefined,
      };

      // Get the original calendar record
      var calendar = await sails.models.calendar.findOne({ ID: inputs.ID });
      if (!calendar) {
        sails.models.status.tasks.calendar--;
        return "No calendar record with that ID exists.";
      }

      // Polyfill main calendar info with edits requested
      var tempCal = {};
      for (var stuff in calendar) {
        if (Object.prototype.hasOwnProperty.call(calendar, stuff)) {
          if (
            typeof calendar[stuff] !== "undefined" &&
            calendar[stuff] !== null
          )
            tempCal[stuff] = calendar[stuff];
        }
      }
      for (var stuff in event) {
        if (Object.prototype.hasOwnProperty.call(event, stuff)) {
          if (typeof event[stuff] !== "undefined" && event[stuff] !== null)
            tempCal[stuff] = event[stuff];
        }
      }

      // Verify the edits
      try {
        event = await sails.helpers.calendar.verify(tempCal);
      } catch (e) {
        sails.models.status.tasks.calendar--;
        return e.message;
      }

      // Check for event conflicts
      sails.models.calendar.calendardb.checkConflicts(
        async (conflicts) => {
          // Edit the event into the calendar
          await sails.models.calendar.updateOne({ ID: inputs.ID }, event);

          // Remove records which should be removed first
          if (conflicts.removals.length > 0) {
            await sails.models.schedule
              .destroy({
                ID: conflicts.removals.map((removal) => removal.scheduleID),
              })
              .fetch();
          }

          // Now, add overrides
          if (conflicts.additions.length > 0) {
            let cfMaps = conflicts.additions.map(async (override) => {
              await sails.models.schedule.create(override).fetch();
            });
            await Promise.all(cfMaps);
          }

          // Finally, re-check the calendar events and update cache after 5 seconds
          setTimeout(async () => {
            await sails.helpers.calendar.check(false, true);
          }, 5000);

          sails.models.status.tasks.calendar--;
        },
        [{ updateCalendar: event }]
      );
    } catch (e) {
      sails.models.status.tasks.calendar--;
      throw e;
    }
  },
};
