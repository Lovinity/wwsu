module.exports = {
  friendlyName: "Calendar / Get",

  description:
    "Get calendar events to use in CalendarDb (or get a single one with analytics if ID provided). Also subscribe to sockets if ID not provided.",

  inputs: {
    ID: {
      type: "number",
      description: `If provided, instead of returning an array of events, will return information about the specified event.`,
    },
  },

  fn: async function (inputs) {
    sails.log.debug("Controller calendar/get called.");

    if (!inputs.ID) {
      var calendarRecords = await sails.models.calendar.find();

      // Subscribe to sockets if applicable
      if (this.req.isSocket) {
        sails.sockets.join(this.req, "calendar");
        sails.log.verbose("Request was a socket. Joining calendar.");
      }

      return calendarRecords;
    } else {
      var record = await sails.models.calendar.findOne({ ID: inputs.ID });
      var returnData = {
        startOfSemester: moment(
          sails.config.custom.startOfSemester
        ).toISOString(true),
        event: record,
      };

      if (!record || record === null) {
        return {};
      }

      returnData.attendance = await sails.models.attendance.find({
        calendarID: inputs.ID,
      });

      let weekStats = await sails.helpers.analytics.showtime(
        undefined,
        [inputs.ID],
        moment().subtract(7, "days").toISOString(true),
        undefined,
        true
      );
      let semesterStats = await sails.helpers.analytics.showtime(
        undefined,
        [inputs.ID],
        moment(sails.config.custom.startOfSemester).toISOString(true),
        undefined,
        true
      );
      let yearStats = await sails.helpers.analytics.showtime(
        undefined,
        [inputs.ID],
        moment().subtract(1, "years").toISOString(true),
        undefined,
        true
      );

      returnData.stats = {
        week: weekStats,
        semester: semesterStats,
        year: yearStats,
      };

      return returnData;
    }
  },
};
