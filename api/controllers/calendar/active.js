module.exports = {
  friendlyName: "Calendar / active",

  description: "Mark an event in the main calendar active",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID number of the calendar event to mark active.",
    },
  },

  fn: async function (inputs) {
    sails.log.debug("Controller calendar/active called.");

    let record = await sails.models.calendar.findOne({ ID: inputs.ID });
    if (!record) throw new Error("The provided ID was not found.");

    // verify the event
    try {
      await sails.helpers.calendar.verify(record);
    } catch (e) {
      return e.message;
    }

    // Mark the calendar as active
    await sails.models.calendar.updateOne(
      { ID: inputs.ID },
      {
        active: true,
        // Reset lastAired to now to prevent auto cleanup from re-marking inactive. It is assumed when an event is marked active, it will be airing soon.
        lastAired: moment().toISOString(true),
      }
    );

    // Finally, re-check the calendar events and update cache after 5 seconds
    setTimeout(async () => {
      await sails.helpers.calendar.check(false, true);
    }, 5000);

    return;
  },
};
