module.exports = {
  friendlyName: "Status / Get",

  description:
    "Get the current status of all subsystems. If socket request, subscribe to receiving status changes.",

  inputs: {
    filter: {
      type: "json",
      description:
        "If provided (array of strings), we will only return / socket-subscribe statuses with the provided names.",
    },
  },

  fn: async function (inputs, exits) {
    sails.log.debug("Controller status/get called.");
    try {
      // Get status records
      var records = await sails.models.status.find();
      sails.log.verbose(`Status records retrieved: ${records.length}.`);
      sails.log.silly(records);

      // Subscribe to websocket if applicable
      if (this.req.isSocket) {
        if (!inputs.filter || inputs.filter.constructor !== Array) {
          sails.sockets.join(this.req, "status");
          sails.log.verbose("Request was a socket. Joining status.");
        } else {
          inputs.filter.forEach((filter) => {
            sails.sockets.join(this.req, `status-${filter}`);
          });
        }
      }

      return exits.success(records);
    } catch (e) {
      return exits.error(e);
    }
  },
};
