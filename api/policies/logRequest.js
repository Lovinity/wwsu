/**
 * logRequest
 *
 * @description :: Log the API request in sails logger
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Policies
 */

module.exports = async function (req, res, next) {
  const thisIP = await sails.helpers.getIp(req);

  sails.config.log.api.http({
    message: `API route requested: ${req.path} (IP: ${thisIP})`,
    req: {
      headers: req.headers,
      ip: thisIP,
      isSocket: req.isSocket,
      path: req.path,
    },
  });

  if (sails._exiting) {
    sails.config.log.api.warn({
      message: `REJECTED request: Server is shutting down.`,
      rejected: true,
    });
    return res
      .status(503)
      .send(
        "The server is shutting down for a reboot or for maintenance."
      );
  }

  return next();
};
