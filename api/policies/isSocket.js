/**
 * isAuthorized
 *
 * @description :: Policy to check if user is authorized with JSON web token
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Policies
 */

module.exports = async function (req, res, next) {
  if (!req.isSocket) {
    return res
      .status(403)
      .json({ err: "This API endpoint cannot be called from an HTTP request; it must be called from a websocket connection." });
  }

  next();
};
