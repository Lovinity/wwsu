/* global sails */

/**
 * isAuthorized
 *
 * @description :: Policy to check if user is authorized with JSON web token
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Policies
 */

module.exports = async function (req, res, next) {
  var authorized = req.session.authAdminDirectorUAB;

  // This should never happen, but if there is no ID, we should error.
  if (!authorized || !authorized.ID || moment().isAfter(moment(authorized.exp)))
    return res.status(401).json({
      errToken:
        "This endpoint requires auth/admin-director-usb authorization. This session has not authorized or the authorization is expired.",
    });

    req.payload = authorized;

  return next();
};
