// Check the load of the server and throttle or block requests as necessary.
const WWSU = require("../../assets/plugins/wwsu-sails/js/wwsu.js");
const throttleQueue = new WWSU.WWSUqueue();

module.exports = async function (req, res, next) {
  // Continue immediately if the task queue is good and throttleAPI is not active.
  if (
    throttleQueue.taskList.length < 1000 &&
    !sails.models.status.errorCheck.throttleAPI
  )
    return next();

  if (
    throttleQueue.taskList.length >= 1000 ||
    sails.models.status.errorCheck.throttleAPI === 2
  ) {
    // Too many pending requests, or throttleAPI set to 2? 503 the request instead.
    sails.config.log.api.warn({
      message: `Request was REJECTED. ${throttleQueue.taskList.length} pending throttled requests. throttleAPI status was ${sails.models.status.errorCheck.throttleAPI}.`,
      rejected: true,
    });
    return res
      .status(503)
      .send(
        "The server is currently overloaded. Please try again in 5 minutes. If you continue to get this error, please contact wwsu4@wright.edu ."
      );
  } else {
    // Throttle the request
    sails.config.log.api.info({
      message: `Request was THROTTLED. ${throttleQueue.taskList.length} pending throttled requests.`,
      throttled: true,
    });

    // Put the next callback in the task queue, which throttles the request.
    throttleQueue.add(() => {
      next();
    });
  }
};
