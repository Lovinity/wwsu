/**
 * Uploads.js
 *
 * @description :: Record of file uploads.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: 'nodebase',
  ignoreEmptyTable: true,
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    hostID: {
      type: "number",
      required: true,
    },

    path: {
      type: "string",
      required: true,
    },

    type: {
      type: "string",
      required: true,
      isIn: ["calendar/logo", "calendar/banner", "directors", "djs", "blogs"],
    },
  },
};
