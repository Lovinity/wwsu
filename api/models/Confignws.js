/**
 * Confignws.js
 *
 * @description :: Configuration for NWS CAPS alerts in the internal EAS.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "disk",
  ignoreEmptyTable: true, // TODO: remove when working
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    code: {
      type: "string",
      required: true,
      description: "The CAPS county or zone code",
    },

    name: {
      type: "string",
      required: true,
      description:
        "The human readable name for this location, such as the name of the county.",
    },
  },

  initialize: async function () {
    return; // TODO: Remove when ready

    // Load full NWS configuration
    sails.config.custom.nws = await sails.models.confignws.find();
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`config-nws socket: ${data}`);
    sails.sockets.broadcast("config-nws", "config-nws", data);

    // Add to config
    sails.config.custom.nws.push(newlyCreatedRecord);

    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`config-nws socket: ${data}`);
    sails.sockets.broadcast("config-nws", "config-nws", data);

    // Update nws in config
    let findRecord = sails.config.custom.nws.findIndex(
      (record) => record.ID === updatedRecord.ID
    );
    if (findRecord < 0) {
      (async () => {
        sails.config.custom.nws = await sails.models.confignws.find();
      })();
    } else {
      sails.config.custom.nws[findRecord] = updatedRecord;
    }

    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`config-nws socket: ${data}`);
    sails.sockets.broadcast("config-nws", "config-nws", data);

    // Remove nws in config
    let findRecord = sails.config.custom.nws.findIndex(
      (record) => record.ID === destroyedRecord.ID
    );
    if (findRecord < 0) {
      (async () => {
        sails.config.custom.nws = await sails.models.confignws.find();
      })();
    } else {
      delete sails.config.custom.nws[findRecord];
    }

    return proceed();
  },
};
