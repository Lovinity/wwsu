/**
 * Climacell.js
 *
 * @description :: Climacell weather information
 */

module.exports = {
  datastore: "nodebase",
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    dataClass: {
      type: "string",
      required: true,
      unique: true,
      description: "The class identifier of the data",
    },

    data: {
      type: "json",
    },

    dataTime: {
      type: "ref",
      columnType: "datetime",
    },
  },

  // MariaDB does not allow ISO strings
  beforeCreate: function (criteria, proceed) {
    ["dataTime"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // MariaDB does not allow ISO strings
  beforeUpdate: function (criteria, proceed) {
    ["dataTime"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`climacell socket: ${data}`);
    sails.sockets.broadcast("climacell", "climacell", data);
    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`climacell socket: ${data}`);
    sails.sockets.broadcast("climacell", "climacell", data);
    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`climacell socket: ${data}`);
    sails.sockets.broadcast("climacell", "climacell", data);
    return proceed();
  },
};
