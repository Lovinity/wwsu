/**
 * Configcategories.js
 *
 * @description :: Configuration that maps system categories with RadioDJ subcategories.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "disk",
  ignoreEmptyTable: true, // TODO: remove when working

  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    name: {
      type: "string",
      required: true,
      unique: true,
    },

    categories: {
      type: "json",
      required: true,
      description:
        "Map of RadioDJ categories and subcategories assigned to this system category. Should be an object... each key is the name of the RadioDJ main category, and value is an array of subcategory names in the main category (or empty array for all of them).",
      custom: (value) => {
        if (typeof value !== "object") return false;

        let valid = true;
        let hasOneKey = false; // There must exist at least one key

        for (let key in value) {
          if (!Object.prototype.hasOwnProperty.call(value, key)) continue;

          // All property keys must be a string
          if (typeof key !== "string") {
            valid = false;
            break;
          }

          hasOneKey = true;

          // Values must be an array
          if (value[key].constructor !== Array) {
            valid = false;
            break;
          }

          // Every item in the value array must be a string
          if (value[key].length) {
            value[key].forEach((subcat) => {
              if (typeof subcat !== "string") valid = false;
            });
          }
        }

        return hasOneKey && valid;
      },
    },
  },

  // Defaults (NOTE: if any categories defined here get deleted, they will be re-created with default values; they cannot be permanently removed.)
  // WARNING: Do NOT remove/rename any of the keys marked "SYSTEM"; but you may modify its default value object.
  systemCategories: {
    // This should include ALL RadioDJ categories and subcategories containing automation system music.
    // This is also used by the request system; all tracks in this config can be requested... tracks not in these categories / subcategories cannot be requested.
    music: {
      // SYSTEM
      Music: [],
      "Top Adds": [],
    },

    // Categories and subcategories containing Top Adds.
    // DEFINITION: Recent music added to the system for high promotion.
    adds: {
      // SYSTEM
      "Top Adds": [],
    },

    // Legal Station IDs
    // DEFINITION: Required FCC ID at the top of every hour that includes call sign, frequency, and market area
    IDs: {
      // SYSTEM
      "Station IDs": ["Standard IDs"],
    },

    // Public Service Announcements
    // DEFINITION: A non-profit "commercial" that promotes a cause, organization, or social issue.
    PSAs: {
      // SYSTEM
      PSAs: [],
    },

    // Station Sweepers
    // DEFINITION: Fun audio clips, generally 15-30 seconds, identifying the station, played as defined in break configuration.
    sweepers: {
      Sweepers: ["Break Sweepers"],
    },

    // Station underwritings and commercials
    // DEFINITION: A promotional audio clip usually paid for by the organization / company. Used in the underwriting queuing system.
    // NOTE: Only tracks in these specified categories / subcategories can be used when scheduling underwritings in the system.
    underwritings: {
      // SYSTEM
      Commercials: [],
    },

    // Station Liners
    // DEFINITION: Short (usually < 7 seconds) station identification that plays in between music tracks during non-break times in automation.
    liners: {
      // SYSTEM
      Liners: ["Standard Liners"],
    },

    // Request liners
    // DEFINITION: Short (usually < 7 seconds) audio clips that play before the system plays requested tracks (if in automation).
    requestLiners: {
      // SYSTEM
      Liners: ["Request Liners"],
    },

    // radio show promos
    // DEFINITION: An audio clip (usually 30 - 60 seconds) promoting a radio show or broadcast, played as defined in break configuration.
    promos: {
      Promos: ["Radio Show Promos"],
    },

    // Music used for sports haltime and other extended sports breaks
    halftime: {
      // SYSTEM
      "Sports Music": ["Halftime and Break Music"],
    },

    // Liners played when the system is sent to break because of a technical issue (remote broadcasts)
    technicalIssues: {
      // SYSTEM
      Liners: ["Technical Issues Liners"],
    },

    // EAS stuff
    easNewAlertIntro: {
      // SYSTEM
      "Emergency Alert System": ["Intro New Alert"],
    },
    easNewAalertStream: {
      // SYSTEM
      "Emergency Alert System": ["New Alert Stream"],
    },
    easAlertUpdateStream: {
      // SYSTEM
      "Emergency Alert System": ["Alert Update Stream"],
    },

    // When the system changes to a new playlist or genre, all tracks will be removed from the current queue EXCEPT tracks that are in these defined RadioDJ categories / subcategories.
    noClearGeneral: {
      // SYSTEM
      Sweepers: [],
      "Station IDs": [],
      Jingles: [],
      Promos: [],
      Liners: [],
      Commercials: [],
      News: [],
      PSAs: [],
      "Radio Shows": [],
      "Sports Openers": [],
      "Sports Liners": [],
      "Sports Closers": [],
      "Show Openers": [],
      "Show Returns": [],
      "Show Closers": [],
      "Emergency Alert System": [],
      Segments: [],
    },

    // When someone starts a show/broadcast, all tracks in the queue will be removed EXCEPT tracks in these categories / subcategories.
    noClearShow: {
      // SYSTEM
      Sweepers: [],
      "Station IDs": [],
      Jingles: [],
      Promos: [],
      Commercials: [],
      "Sports Openers": [],
      "Sports Liners": [],
      "Sports Closers": [],
      "Show Openers": [],
      "Show Returns": [],
      "Show Closers": [],
      "Emergency Alert System": [],
    },

    // When a DJ or producer requests to resume a broadcast from a break, all tracks in these defined categories and subcategories will be removed from the queue
    clearBreak: {
      // SYSTEM
      PSAs: [],
      "Sports Music": ["Halftime and Break Music"],
    },

    // Whenever a track from any of these categories / subcategories play, the metadata will show configured alt(state) text instead of the track info.
    // NOTE: This also determines when the system determines when someone has gone on the air; the first track not existing in here is deemed when someone has started the broadcast.
    noMeta: {
      // SYSTEM
      Jingles: [],
      "Station IDs": [],
      PSAs: [],
      Liners: [],
      Sweepers: [],
      Promos: [],
      "Sports Music": [],
      "Show Returns": [],
      "Sports Liners": [],
      Commercials: [],
      "Emergency Alert System": [],
    },

    // CRON will routinely check tracks in the specified categories. If there is a set fade in or fade out on these tracks, the system will reset these to zero (eg. no fading).
    noFade: {
      // SYSTEM
      "Station IDs": [],
      Promos: [],
      PSAs: [],
      Commercials: [],
      "Emergency Alert System": [],
    },
  },

  initialize: async function () {
    return; // TODO: Remove when ready

    // Create any missing categories as defined in systemCategories
    for (let key in module.exports.systemCategories) {
      if (
        !Object.prototype.hasOwnProperty.call(
          module.exports.systemCategories,
          key
        )
      )
        continue;

      // Create the category if it does not exist with default values
      let record = await sails.models.configcategories.findOrCreate(
        { name: key },
        { name: key, categories: module.exports.systemCategories[key] }
      );
    }

    // Load full category configuration
    sails.config.custom.categories = await sails.models.configcategories.find();
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`config-categories socket: ${data}`);
    sails.sockets.broadcast("config-categories", "config-categories", data);

    // Add break to config
    sails.config.custom.categories.push(newlyCreatedRecord);

    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`config-categories socket: ${data}`);
    sails.sockets.broadcast("config-categories", "config-categories", data);

    // Re-run initialize in case we lost a system category (and to update full categories in memory)
    module.exports.initialize();

    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`config-categories socket: ${data}`);
    sails.sockets.broadcast("config-categories", "config-categories", data);

    // Re-run initialize in case we lost a system category (and to update full categories in memory)
    module.exports.initialize();

    return proceed();
  },
};
