/**
 * Configradiodjs.js
 *
 * @description :: A configuration of each RadioDJ used by the system.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "disk",
  ignoreEmptyTable: true, // TODO: remove when working
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    name: {
      type: "string",
      required: true,
      unique: true,
      description: "Alphanumeric name",
    },

    label: {
      type: "string",
      description: "Friendly name",
    },

    restURL: {
      type: "string",
      isURL: true,
      required: true,
      description: "The URL to the REST server for this RadioDJ",
    },

    restPassword: {
      type: "string",
      required: true,
      encrypt: true,
      description: "The password for the RadioDJ REST server",
    },

    errorLevel: {
      type: "number",
      min: 1,
      max: 5,
      required: true,
      description:
        "The error level that should be reported when this RadioDJ is not working (5 = good, 4 = info, 3 = minor, 2 = major, 1 = critical)",
    },
  },

  initialize: async function () {
    return; // TODO: remove when ready

    // Load full configuration
    sails.config.custom.radiodjs = await sails.models.configradiodjs
      .find()
      .decrypt();
  },

  // Websockets standards
  // TODO: Do any necessary maintenance of specific setting changes
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`config-radiodjs socket: ${data}`);
    sails.sockets.broadcast("config-radiodjs", "config-radiodjs", data);

    (async () => {
      // Reload configuration
      sails.config.custom.radiodjs = await sails.models.configradiodjs
        .find()
        .decrypt();
    })();

    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`config-radiodjs socket: ${data}`);
    sails.sockets.broadcast("config-radiodjs", "config-radiodjs", data);

    (async () => {
      // Reload configuration
      sails.config.custom.radiodjs = await sails.models.configradiodjs
        .find()
        .decrypt();
    })();

    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`config-radiodjs socket: ${data}`);
    sails.sockets.broadcast("config-radiodjs", "config-radiodjs", data);

    (async () => {
      // Reload configuration
      sails.config.custom.radiodjs = await sails.models.configradiodjs
        .find()
        .decrypt();
    })();

    return proceed();
  },
};
