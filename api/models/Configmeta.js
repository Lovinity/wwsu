/**
 * Configmeta.js
 *
 * @description :: Meta configuration. Be sure to check for ID 1 and create it if it does not exist.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "disk",
  ignoreEmptyTable: true, // TODO: remove when working
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    // TODO: renamed from clearTime
    manualClear: {
      type: "number",
      defaultsTo: 10,
      min: 0,
      description:
        "The amount of time (in minutes) before meta is automatically cleared from a manually-logged track.",
    },

    altAutomation: {
      type: "string",
      defaultsTo: "We'll get back to the music shortly...",
      description:
        "What will display for meta when a track in the noMeta category is playing during default automation.",
    },

    altPlaylist: {
      type: "string",
      defaultsTo: "We'll get back to the playlist shortly...",
      description:
        "What will display for meta when a track in the noMeta category is playing during a playlist.",
    },

    altGenre: {
      type: "string",
      defaultsTo: "We'll get back to the music shortly...",
      description:
        "What will display for meta when a track in the noMeta category is playing during a genre rotation.",
    },

    altLive: {
      type: "string",
      defaultsTo: "We'll get back to the show shortly...",
      description:
        "What will display for meta when a track in the noMeta category is playing during a live broadcast.",
    },

    altPrerecord: {
      type: "string",
      defaultsTo: "We'll get back to the show shortly...",
      description:
        "What will display for meta when a track in the noMeta category is playing during a prerecord.",
    },

    altRemote: {
      type: "string",
      defaultsTo: "We'll get back to the broadcast shortly...",
      description:
        "What will display for meta when a track in the noMeta category is playing during a remote broadcast.",
    },

    altSports: {
      type: "string",
      defaultsTo: "We'll get back to the sports broadcast shortly...",
      description:
        "What will display for meta when a track in the noMeta category is playing during a sports broadcast.",
    },

    prefixAutomation: {
      type: "string",
      defaultsTo: "Playing: ",
      description:
        "During automation, this prefix will appear before the currently playing track on line 1 of metadata.",
    },

    prefixGenre: {
      type: "string",
      defaultsTo: "Genre: ",
      description:
        "During genre rotations, this will appear before the genre currently airing on line 2 of metadata.",
    },

    prefixPlaylist: {
      type: "string",
      defaultsTo: "Playlist: ",
      description:
        "During a playlist, this will appear before the playlist currently airing on line 2 of metadata",
    },

    prefixRequest: {
      type: "string",
      defaultsTo: "Requested By: ",
      description:
        "When playing a track request... this will appear before the name of the person who requested the track on line 2 of metadata.",
    },

    prefixPendLive: {
      type: "string",
      defaultsTo: "Coming Up: ",
      description:
        "When a live show is about to begin, this will appear before the hosts - show on line 2 of metadata.",
    },

    prefixPendPrerecord: {
      type: "string",
      defaultsTo: "Coming Up: ",
      description:
        "When a prerecorded show is about to begin, this will appear before the hosts - show on line 2 of metadata.",
    },

    prefixPendRemote: {
      type: "string",
      defaultsTo: "Coming Up: ",
      description:
        "When a remote broadcast is about to begin, this will appear before the hosts - show on line 2 of metadata.",
    },

    prefixPendSports: {
      type: "string",
      defaultsTo: "Coming Up: Raider Sports - ",
      description:
        "When a sports broadcast is about to begin, this will appear before the sport on line 2 of metadata.",
    },

    prefixLive: {
      type: "string",
      defaultsTo: "On the Air: ",
      description:
        "During a live show, this will appear before the hosts - show on line 1 of metadata.",
    },

    prefixPrerecord: {
      type: "string",
      defaultsTo: "Broadcasting: ",
      description:
        "During a prerecorded show, this will appear before the hosts - show on line 1 of metadata.",
    },

    prefixRemote: {
      type: "string",
      defaultsTo: "On the Air: ",
      description:
        "During a remote broadcast, this will appear before the hosts - show on line 1 of metadata",
    },

    prefixSports: {
      type: "string",
      defaultsTo: "Raider Sports Live Coverage: ",
      description:
        "During a sports broadcast, this will appear before the sport being aired on line 1 of metadata",
    },

    prefixPlaying: {
      type: "string",
      defaultsTo: "Playing: ",
      description:
        "During a show/remote/prerecord/sports, this will appear before the track name on line 2 of metadata when something is being played",
    },
  },

  initialize: async function () {
    return; // TODO: remove when ready
    sails.config.custom.meta = await sails.models.configmeta.findOrCreate(
      { ID: 1 },
      { ID: 1 }
    );
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`config-meta socket: ${data}`);
    sails.sockets.broadcast("config-meta", "config-meta", data);

    // Update config
    sails.config.custom.meta = newlyCreatedRecord;

    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`config-meta socket: ${data}`);
    sails.sockets.broadcast("config-meta", "config-meta", data);

    // Update config
    sails.config.custom.meta = updatedRecord;

    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`config-meta socket: ${data}`);
    sails.sockets.broadcast("config-meta", "config-meta", data);

    // If the master record was destroyed, treat as a "reset to default" and re-create it
    if (destroyedRecord.ID === 1) {
      (async () => {
        sails.config.custom.meta = await sails.models.configmeta.findOrCreate(
          { ID: 1 },
          { ID: 1 }
        );
      })();
    }

    return proceed();
  },
};
