/**
 * Lockdown.js
 *
 * @description :: Contains records of people clocking in and out of locked down hosts.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "nodebase",
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    host: {
      type: "number",
      required: true,
    },

    type: {
      type: "string",
      isIn: ["director", "DJ"],
      required: true,
    },

    typeID: {
      type: "number",
      required: true,
    },

    clockIn: {
      type: "ref",
      columnType: "datetime",
    },

    clockOut: {
      type: "ref",
      columnType: "datetime",
    },
  },

  // MariaDB does not allow ISO strings
  beforeCreate: function (criteria, proceed) {
    ["clockIn", "clockOut"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // MariaDB does not allow ISO strings
  beforeUpdate: function (criteria, proceed) {
    ["clockIn", "clockOut"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`lockdown socket: ${data}`);
    sails.sockets.broadcast(`lockdown`, "lockdown", data);
    sails.sockets.broadcast(`lockdown-all`, "lockdown", data);
    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`lockdown socket: ${data}`);
    sails.sockets.broadcast(`lockdown`, "lockdown", data);
    sails.sockets.broadcast(`lockdown-all`, "lockdown", data);
    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`calendar socket: ${data}`);
    sails.sockets.broadcast(`lockdown`, "lockdown", data);
    sails.sockets.broadcast(`lockdown-all`, "lockdown", data);
    return proceed();
  },
};
