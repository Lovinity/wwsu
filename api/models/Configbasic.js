/**
 * Configbasic.js
 *
 * @description :: Basic configuration for the WWSU sails app. Make sure to check for ID 1 and create if not exists.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "disk",
  ignoreEmptyTable: true, // TODO: remove when working
  attributes: {
    ID: {
      type: "number",
      unique: true,
      required: true,
    },

    /*
      MAINTENANCE MODE
    */

    // TODO: used to be lofi; update in code
    maintenance: {
      description:
        "When true, many regular checks will be disabled. Enable when performing maintenance.",
      type: "boolean",
      defaultsTo: false,
    },

    /*
      HOSTS
    */

    hostSecret: {
      description:
        "A random string to encode hosts and IP addresses. WARNING! Changing this will invalidate all active discipline!",
      type: "string",
      defaultsTo: "q3NLrzcQHYJ9whBFj3kl",
    },

    /*
      DATES
    */

    startOfSemester: {
      description:
        "moment.js compatible date/time indicating the start of the current semester / schedule rotation.",
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      defaultsTo: "2002-01-01T00:00:00Z",
    },

    /*
      URLs
    */

    website: {
      description: "URL to the WWSU website",
      type: "string",
      allowNull: true,
      isURL: true,
    },

    // TODO: name changed from stream
    shoutcastStream: {
      description:
        "The URL to the Shoutcast v2 internet stream server for WWSU",
      type: "string",
      isURL: true,
      allowNull: true,
    },

    owncastStream: {
      description: "The URL to the Owncast video stream server for WWSU",
      type: "string",
      isURL: true,
      allowNull: true,
    },

    /*
      TOMORROW.IO
    */

    // TODO: Change all config-basic stuff to their new name of tomorrowio. Also disable tomorrowio if no API provided

    tomorrowioAPI: {
      description:
        "API key for tomorrow.io weather platform to get current weather and forecasts.",
      type: "string",
      allowNull: true,
      encrypt: true,
    },

    tomorrowioLocation: {
      description:
        "Location parameter for tomorrow.io weather (such as latitude,longitude)",
      type: "string",
      allowNull: true,
    },

    /*
      SKYWAYJS
    */

    // TODO: Make sure to transmit this to DJ Controls when they host-authorize, and that DJ Controls uses these
    skywayAPI: {
      description:
        "API key for skyway.js (service used for DJ Controls to establish audio calls for remote broadcasts).",
      type: "string",
      allowNull: true,
    },

    skywaySecret: {
      description: "API secret for skyway.js service",
      type: "string",
      allowNull: true,
      encrypt: true,
    },

    /*
      SPORTS
    */

    sports: {
      description:
        "Array of sports that can be broadcast on WWSU. Each sport MUST exist as a subcategory in RadioDJ under the categories Sports Openers, Sports Liners, and Sports Closers.",
      type: "json",
      custom: function (value) {
        // Must be an array
        if (value.constructor !== Array) return false;

        // All entries in array must be a string. But allow empty array.
        if (value.length > 0 && value.find((val) => typeof val !== "string"))
          return false;

        return true;
      },
      defaultsTo: [
        `Men's Basketball`,
        `Women's Basketball`,
        `Men's Baseball`,
        `Women's Softball`,
        `Men's Soccer`,
        `Women's Soccer`,
        `Men's Tennis`,
        `Women's Tennis`,
        `Men's Volleyball`,
        `Women's Volleyball`,
        `Men's Football`,
        `Women's Football`,
      ],
    },

    /*
      BASIC BREAK CONFIGURATION
    */

    breakCheck: {
      type: "number",
      defaultsTo: 10,
      min: 3,
      max: 50,
      description:
        "Error deviation for breaks in minutes; prevents airing breaks (except top of hour ID) less than this many minutes apart from each other. Ideally, this number should not be greater than the smallest duration between two clockwheel breaks. Also, any music tracks longer than this many minutes will be considered too long by the system.",
    },

    linerTime: {
      type: "number",
      defaultsTo: 10,
      min: 1,
      max: 59,
      description:
        "Do not play a track from the liners category in automation any more often than once every defined number of minutes.",
    },

    /*
      MAX ALLOWED QUEUE TIMES
    */

    maxQueueLive: {
      description:
        "When starting a live show, if total queue is greater than this many seconds, all music will be skipped/removed immediately to speed up going live.",
      type: "number",
      min: 0,
      defaultsTo: 60 * 3,
    },

    maxQueuePrerecord: {
      description:
        "When starting a prerecord, if total queue is greater than this many seconds, all music will be skipped/removed immediately to speed up starting the prerecord.",
      type: "number",
      min: 0,
      defaultsTo: 60 * 3,
    },

    maxQueueSports: {
      description:
        "When starting a sports broadcast, if total queue is greater than this many seconds, all music will be skipped/removed immediately to speed up starting.",
      type: "number",
      min: 0,
      defaultsTo: 60,
    },

    maxQueueSportsReturn: {
      description:
        "When returning from break during a sports broadcast, if total queue is greater than this many seconds, all non-commercials and IDs will be skipped/removed immediately to speed up going back on air.",
      type: "number",
      min: 0,
      defaultsTo: 30,
    },

    maxQueueRemote: {
      description:
        "When starting a remote broadcast, if total queue is greater than this many seconds, all music will be skipped/removed immediately to speed up starting.",
      type: "number",
      min: 0,
      defaultsTo: 60 * 3,
    },

    /*
      LIKED TRACKS
    */

    songsLikedCooldown: {
      description:
        "When someone likes a track on the website, the same track cannot be liked again by the same IP address for this many days. 0 means a track can only ever be liked one time per IP address.",
      type: "number",
      min: 0,
      defaultsTo: 7,
    },

    songsLikedPriorityChange: {
      description:
        "When someone likes a track, its priority in RadioDJ will be adjusted by this amount (positive = increase, negative = decrease)",
      type: "number",
      min: -100,
      max: 100,
      defaultsTo: 2,
    },

    /*
      REQUEST SYSTEM
    */

    requestsDailyLimit: {
      description:
        "Each IP address is allowed to make this many track requests per day (reset at midnight).",
      type: "number",
      min: 0,
      defaultsTo: 10,
    },

    requestsPriorityChange: {
      description:
        "When someone requests a track, its priority in RadioDJ will be adjusted by this amount (positive = increase, negative = decrease). Keep in mind if priority adjustment is set in RadioDJ for a played track, its priority will also be reduced/adjusted by that amount when played.",
      type: "number",
      min: -100,
      max: 100,
      defaultsTo: 1,
    },

    /*
      PROFANITY FILTER
    */

    filterProfanity: {
      description:
        "The specified words will be censored (including within other words) in metadata and messages.",
      type: "json",
      custom: function (value) {
        // Must be an array
        if (value.constructor !== Array) return false;

        // All entries in array must be a string. But allow empty array.
        if (value.length > 0 && value.find((val) => typeof val !== "string"))
          return false;

        return true;
      },
      defaultsTo: [
        "5h1t",
        "5hit",
        "assfukka",
        "asshole",
        "asswhole",
        "b!tch",
        "b17ch",
        "b1tch",
        "ballbag",
        "ballsack",
        "beastiality",
        "bestiality",
        "bi+ch",
        "biatch",
        "bitch",
        "blow job",
        "blowjob",
        "boiolas",
        "boner",
        "bunny fucker",
        "butthole",
        "buttmuch",
        "buttplug",
        "carpet muncher",
        "chink",
        "cl1t",
        "clit",
        "cnut",
        "cock-sucker",
        "cockface",
        "cockhead",
        "cockmunch",
        "cocksuck",
        "cocksuka",
        "cocksukka",
        "cokmuncher",
        "coksucka",
        "cummer",
        "cumming",
        "cumshot",
        "cunilingus",
        "cunillingus",
        "cunnilingus",
        "cunt",
        "cyberfuc",
        "dickhead",
        "dildo",
        "dog-fucker",
        "doggin",
        "dogging",
        "donkeyribber",
        "doosh",
        "duche",
        "dyke",
        "ejaculate",
        "ejaculating",
        "ejaculation",
        "ejakulate",
        "f u c k",
        "fag",
        "fannyflaps",
        "fannyfucker",
        "fatass",
        "fcuk",
        "felching",
        "fellate",
        "fellatio",
        "flange",
        "fuck",
        "fudge packer",
        "fudgepacker",
        "fuk",
        "fux",
        "fux0r",
        "f_u_c_k",
        "gangbang",
        "gaylord",
        "gaysex",
        "goatse",
        "god-dam",
        "goddamn",
        "hardcoresex",
        "heshe",
        "homosex",
        "horniest",
        "horny",
        "hotsex",
        "jack-off",
        "jackoff",
        "jerk-off",
        "jism",
        "jiz",
        "jizm",
        "kawk",
        "kunilingus",
        "l3i+ch",
        "l3itch",
        "labia",
        "m0f0",
        "m0fo",
        "m45terbate",
        "ma5terb8",
        "ma5terbate",
        "master-bate",
        "masterb8",
        "masterbat*",
        "masterbat3",
        "masterbate",
        "masterbation",
        "masturbate",
        "mo-fo",
        "mof0",
        "mofo",
        "muthafecker",
        "muthafuckker",
        "mutherfucker",
        "n1gga",
        "n1gger",
        "nigg3r",
        "nigg4h",
        "nigga",
        "nigger",
        "nob jokey",
        "nobhead",
        "nobjocky",
        "nobjokey",
        "numbnuts",
        "nutsack",
        "pecker",
        "penis",
        "phonesex",
        "phuck",
        "phuk",
        "phuq",
        "pimpis",
        "piss",
        "prick",
        "pusse",
        "pussi",
        "pussies",
        "pussy",
        "rectum",
        "retard",
        "rimjaw",
        "rimming",
        "s hit",
        "schlong",
        "scroat",
        "scrote",
        "scrotum",
        "semen",
        "sh!+",
        "sh!t",
        "sh1t",
        "shag",
        "shagger",
        "shaggin",
        "shagging",
        "shemale",
        "shi+",
        "shit",
        "skank",
        "slut",
        "sluts",
        "smegma",
        "spunk",
        "s_h_i_t",
        "t1tt1e5",
        "t1tties",
        "teets",
        "testical",
        "testicle",
        "titfuck",
        "tits",
        "tittie5",
        "tittiefucker",
        "titties",
        "tw4t",
        "twat",
        "twunt",
        "vagina",
        "vulva",
        "w00se",
        "wang",
        "wanker",
        "wanky",
        "whore",
      ],
    },

    /*
      HTML SANITIZE
    */

    sanitize: {
      description:
        "Options for sanitize-html to prevent annoying or unsafe HTML from being used in messages and public locations.",
      type: "json",
      defaultsTo: {
        allowedTags: [
          "h2",
          "h3",
          "h4",
          "h5",
          "h6",
          "blockquote",
          "p",
          "a",
          "ul",
          "ol",
          "nl",
          "li",
          "b",
          "i",
          "strong",
          "em",
          "strike",
          "code",
          "hr",
          "br",
          "u",
          "s",
          "span",
        ],
        allowedAttributes: {
          a: ["href", "name", "target"],
          span: ["style"],
        },
        allowedStyles: {
          span: {
            // Match HEX and RGB
            color: [
              /^#(0x)?[0-9a-f]+$/i,
              /^rgb\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\)$/,
            ],
          },
        },
        // Lots of these won't come up by default because we don't allow them
        selfClosing: [
          "br",
          "hr",
          "area",
          "base",
          "basefont",
          "input",
          "link",
          "meta",
        ],
        // URL schemes we permit
        allowedSchemes: ["http", "https"],
        allowedSchemesByTag: {},
        allowedSchemesAppliedToAttributes: ["href", "src", "cite"],
        allowProtocolRelative: true,
      },
    },
  },

  initialize: async function () {
    return; // TODO: remove when ready
    sails.config.custom.basic = await sails.models.configbasic
      .findOrCreate({ ID: 1 }, { ID: 1 })
      .decrypt();

    await sails.helpers.config.basic.sports.reload();
  },

  // Websockets standards
  // TODO: Do any necessary maintenance of specific setting changes
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`config-basic socket: ${data}`);
    sails.sockets.broadcast("config-basic", "config-basic", data);

    (async () => {
      // Update config
      sails.config.custom.basic = await sails.models.configbasic
        .findOrCreate({ ID: 1 }, { ID: 1 })
        .decrypt();

      // Reload sports in calendar
      await sails.helpers.config.basic.sports.reload();
    })();

    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`config-basic socket: ${data}`);
    sails.sockets.broadcast("config-basic", "config-basic", data);

    (async () => {
      // Update config
      sails.config.custom.basic = await sails.models.configbasic
        .findOrCreate({ ID: 1 }, { ID: 1 })
        .decrypt();

      // Reload sports in calendar
      await sails.helpers.config.basic.sports.reload();
    })();

    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`config-basic socket: ${data}`);
    sails.sockets.broadcast("config-basic", "config-basic", data);

    (async () => {
      // Update config
      sails.config.custom.basic = await sails.models.configbasic
        .findOrCreate({ ID: 1 }, { ID: 1 })
        .decrypt();

      // Reload sports in calendar
      await sails.helpers.config.basic.sports.reload();
    })();

    return proceed();
  },
};
