/**
 * Subcategory.js
 *
 * @description :: A container of subcategories from RadioDJ.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "radiodj",
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    parentid: {
      type: "number",
    },

    name: {
      type: "string",
    },
  },

  initialize: async function () {
    // Determine RadioDJ subcategory IDs for configured categories
    await sails.helpers.songs.reloadSubcategories();
  },
};
