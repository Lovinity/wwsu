/**
 * Cache.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "ram",
  ignoreEmptyTable: true,
  attributes: {
    ID: {
      type: "string",
      unique: true,
      required: true,
    },

    model: {
      type: "string",
      required: true,
    },

    data: {
      type: "json",
    },
  },

  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord.data };
    sails.sockets.broadcast(
      `cache-${newlyCreatedRecord.model}`,
      `cache-${newlyCreatedRecord.model}`,
      data
    );

    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord.data };
    sails.sockets.broadcast(
      `cache-${updatedRecord.model}`,
      `cache-${updatedRecord.model}`,
      data
    );

    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = {
      remove: destroyedRecord.data
        ? destroyedRecord.data.ID || destroyedRecord.ID
        : destroyedRecord.ID,
    };
    sails.sockets.broadcast(
      `cache-${destroyedRecord.model}`,
      `cache-${destroyedRecord.model}`,
      data
    );

    return proceed();
  },
};
