# WWSU 8 ALPHA
The WWSU Radio Sails.js API application enables external / remote control of core WWSU functionality. Applications can be developed utilizing this API. 

**The master branch should NOT be used for your own deployment and should be considered extremely unstable**

API documentation not currently available as version 8 is in alpha stage.
